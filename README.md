## General Information

This code consist of a multi-level reactive power market that enables reactive
power provision from distributed energy resources to higher voltage levels. It
provides an algorithm for the determination of the reactive power flexibility
range and cost function of a complete grid. An evaluation environment, consisting
of multiple medium voltage grids and a high voltage grid, for time series
simulations is also provided.

The code was used for the publication
Bozionek et al. - Design and Evaluation of a Multi-level Reactive Power Market.
https://www.researchsquare.com/article/rs-1389372/v1. (preprint)

If you re-use part of the code for research, please cite the publication.

The time series/market evaluation data that was created for the paper can be found here:
https://gitlab.com/digitalized-energy-systems/data/data_multi_level_reactive_power_market_design

For questions, suggestions, etc. contact Thomas Wolgast per email
(thomas.wolgast@uni-oldenburg.de) or create an issue here.


## Installation

* Create some kind of virtual environment with python 3.9 in anaconda, pipenv etc. 
* Run `pip install -r requirements.txt`


## Usage

### Development of the multi-level market algorithm
If you wish to test the multi level market algorithm and the EPF approximations
methods for a discrete grid state:
* run `python examples_and_testing.py`
For a quick visualisation of the calculated EPF and the approximated EPF set
`plot_epf = True` in `parameter.py`.

### Market evaluation based on time series simulation
To carry out a time series calculation, run one of the following:
* Multi-level market: `python Multi_level_market.py`
* Reference Case OptMarket: `python RC_OptMarket.py`
* Reference Case NoMarket: `python RC_NoMarket.py`

If you want to choose a specific number of time steps for the 
time series simulations, change the variable `nbr_of_samples` in the main function of each file.


## How to recreate plots from paper

For all plots results of time series calculations are required. Pull the data from the URL above into the Results folder in this directory. If you name the folder differently, change the variable “directory_result_folder” in the main of the script result_visualisation.py to your chosen path.

* Run `result_visualisation.py`

If you want to visualise results from your own time series calculations:

Run the calculations for the market and the two reference cases. Name the generated individual result folder accordingly to the paper results from the URL above. Afterwards, repeat the steps described in this section.

