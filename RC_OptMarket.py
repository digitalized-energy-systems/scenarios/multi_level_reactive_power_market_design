import simbench as sb
import pandas as pd
import random
import os

from evaluation_functions import store_results as store, RC_OptMarket_specific

import logging
logger = logging.getLogger(__name__)


def main():
    RC_OptMarket(nbr_of_samples=5000)


def RC_OptMarket(nbr_of_samples):
    net = sb.get_simbench_net('1-HVMV-urban-all-0-no_sw')

    initial_tap_settings, profiles, index_dict = RC_OptMarket_specific.set_up_complete_grid(net)

    random.seed(1)
    sequence = profiles[('load', 'p_mw')].index.tolist()
    time_steps = random.sample(sequence, nbr_of_samples)

    print(time_steps)

    res_dir, res_files, res_files_voltage, res_files_loading, slack_values_mw = store.create_results_csv_files("ideal_optimum")

    RC_OptMarket_specific.ts_calculation(net, profiles, index_dict, initial_tap_settings, res_dir, res_files,
                                         res_files_voltage, res_files_loading, time_steps)

    res_ideal_case_df = pd.read_csv(os.path.join(res_dir, res_files["NT"]))
    print(res_ideal_case_df.to_string())


if __name__ == '__main__':
    main()
