""" Functions to set different grid constraints depending
    on the operation mode of the multilevel grid reactive power market.
"""

import numpy as np
import pandapower as pp

import parameter


def set_voltage_constraints(net):
    """ Sets the voltage constraints for the buses depending on the type of
        network and voltage level (for simbench grids).

        INPUT:
            **net** - The pandapower format network
    """
    if net.bus.columns.str.contains('voltLvl').any():

        conditions = [(net.bus['voltLvl'] == 1),
                      (net.bus['voltLvl'] == 3),
                      (net.bus['voltLvl'] == 5),
                      (net.bus['voltLvl'] == 7)]

        choices_min = [parameter.vm_pu["EHV"][0],
                       parameter.vm_pu["HV"][0],
                       parameter.vm_pu["MV"][0],
                       parameter.vm_pu["LV"][0]]

        choices_max = [parameter.vm_pu["EHV"][1],
                       parameter.vm_pu["HV"][1],
                       parameter.vm_pu["MV"][1],
                       parameter.vm_pu["LV"][1]]

        net.bus["min_vm_pu"] = np.select(conditions, choices_min, default=1)
        net.bus["max_vm_pu"] = np.select(conditions, choices_max, default=1)

    else:
        net.bus["min_vm_pu"] = parameter.min_vm_pu
        net.bus["max_vm_pu"] = parameter.max_vm_pu


def set_line_and_trafo_constraints(net):
    """ Sets the maximum loadings of lines and trafos. If a simbench net is
        used the loadings will be set depended on the voltage level.
        If no simbench grid is used,the loadings can be defined separately.

        INPUT:
            **net** - The pandapower format network
    """
    if net.line.columns.str.contains('voltLvl').any() \
            and net.trafo.columns.str.contains('voltLvl').any():

        conditions_line = [(net.line['voltLvl'] == 1),
                           (net.line['voltLvl'] == 3),
                           (net.line['voltLvl'] == 5),
                           (net.line['voltLvl'] == 7)]

        choices_line = [parameter.simbench_loadings["EHV"],
                        parameter.simbench_loadings["HV"],
                        parameter.simbench_loadings["MV"],
                        parameter.simbench_loadings["LV"]]

        net.line["max_loading_percent"] = np.select(conditions_line,
                                                    choices_line, default=100)

        conditions_trafo = [(net.trafo['voltLvl'] == 2),
                            (net.trafo['voltLvl'] == 4),
                            (net.trafo['voltLvl'] == 6)]

        choices_trafo = [parameter.simbench_loadings["EHV_HV"],
                         parameter.simbench_loadings["HV_MV"],
                         parameter.simbench_loadings["MV_LV"]]

        net.trafo["max_loading_percent"] = np.select(conditions_trafo,
                                                     choices_trafo, default=100)

    else:
        net.line["max_loading_percent"] = parameter.max_line_loading
        net.trafo["max_loading_percent"] = parameter.max_trafo_loading


def set_ext_grid_constraints(net, slack_voltage_vm_pu):
    """ Sets the constraints for the external grid. Active/Reactive power limits
        are set by default quite high to ensure reactive/active power balancing

        INPUT:
            **net** - The pandapower format network

            **slack_voltage_vm_pu** - Voltage in pu
    """
    net.ext_grid.loc[0, "vm_pu"] = slack_voltage_vm_pu
    net.ext_grid["min_p_mw"] = -1000
    net.ext_grid["max_p_mw"] = 1000
    net.ext_grid["min_q_mvar"] = -1000
    net.ext_grid["max_q_mvar"] = 1000
    net.ext_grid["controllable"] = True


def set_opf_constraints(net, slack_voltage_vm_pu, specific_units: dict = None):
    """ Sets the constraints for the lines, voltages, external grid and
        flexibility units.

        INPUT:
            **net** - The pandapower format network

            **specific_units** (dict) - Optional: Dictionary that contains the
            unit type and a list of indexes of the flexible
            units: {'sgen': [indexes], 'gen': [indexes],..}, if no dictionary
            is given, every unit of the specified flexibility unit types is
            considered as flexible unit.
    """

    set_voltage_constraints(net)

    set_line_and_trafo_constraints(net)

    set_ext_grid_constraints(net, slack_voltage_vm_pu)

    if specific_units is None:
        for unit_type in parameter.flexibility_units:
            net[unit_type]["controllable"] = True
            flex_units = net[unit_type]["controllable"]
            q_flex = (net[unit_type].loc[flex_units, "sn_mva"] ** 2
                      - net[unit_type].loc[flex_units, "p_mw"] ** 2) ** 0.5

            net[unit_type]["min_p_mw"] = net[unit_type].loc[flex_units, "p_mw"]
            net[unit_type]["max_p_mw"] = net[unit_type].loc[flex_units, "p_mw"]
            net[unit_type]["min_q_mvar"] = -q_flex
            net[unit_type]["max_q_mvar"] = q_flex

    else:
        for unit_type in specific_units.keys():
            net[unit_type].loc[specific_units[unit_type], "controllable"] = True
            flex_units = net[unit_type]["controllable"]
            q_flex = (net[unit_type].loc[flex_units, "sn_mva"] ** 2
                      - net[unit_type].loc[flex_units, "p_mw"] ** 2) ** 0.5

            net[unit_type]["min_p_mw"] = net[unit_type].loc[flex_units, "p_mw"]
            net[unit_type]["max_p_mw"] = net[unit_type].loc[flex_units, "p_mw"]
            net[unit_type]["min_q_mvar"] = -q_flex
            net[unit_type]["max_q_mvar"] = q_flex


def time_series_constraints(net, slack_voltage_vm_pu, specific_units: dict = None):
    """ Sets the constraints for flexibility units and specifies the slack
        voltage at the coupling point.

        INPUT:
            **net** - The pandapower format network

            **specific_units** (dict) - Optional: Dictionary that contains the
            unit type and a list of indexes of the flexible
            units: {'sgen': [indexes], 'gen': [indexes],..}, if no dictionary
            is given, every unit of the specified flexibility unit types is
            considered as flexible unit.
    """
    net.ext_grid.loc[0, "vm_pu"] = slack_voltage_vm_pu

    if specific_units is None:
        for unit_type in parameter.flexibility_units:
            net[unit_type]["controllable"] = True
            flex_units = net[unit_type]["controllable"]
            q_flex = (net[unit_type].loc[flex_units, "sn_mva"] ** 2
                      - net[unit_type].loc[flex_units, "p_mw"] ** 2) ** 0.5

            net[unit_type]["min_p_mw"] = net[unit_type].loc[flex_units, "p_mw"]
            net[unit_type]["max_p_mw"] = net[unit_type].loc[flex_units, "p_mw"]
            net[unit_type]["min_q_mvar"] = -q_flex
            net[unit_type]["max_q_mvar"] = q_flex

    else:
        for unit_type in specific_units.keys():
            net[unit_type].loc[specific_units[unit_type], "controllable"] = True
            flex_units = net[unit_type]["controllable"]
            q_flex = (net[unit_type].loc[flex_units, "sn_mva"] ** 2
                      - net[unit_type].loc[flex_units, "p_mw"] ** 2) ** 0.5

            net[unit_type]["min_p_mw"] = net[unit_type].loc[flex_units, "p_mw"]
            net[unit_type]["max_p_mw"] = net[unit_type].loc[flex_units, "p_mw"]
            net[unit_type]["min_q_mvar"] = -q_flex
            net[unit_type]["max_q_mvar"] = q_flex


def set_up_eq(net, object1, idx_eq: tuple, epf: str):
    """ Every simbench net contains sgen and load that are equivalent elements
        for the downstream nets. This function sets up
        downstream grids with the calculated reactive power flexibility range
        as an equivalent sgen. Also, the EPF will be set as costs for this
        equivalent sgen. The (constant) active power of the
        downstream network will be modeled either by a sgen or a load depending
        on the sign of the active power/active power flow direction.


        INPUT:
            **net** - The pandapower format network

            **object1** (object) - Object of the class epf_one_grid, which
            contains the results of the EPF-net calculation

            **idx_eq** (tuple) - Tuple that contains tuples with the unit_type,
            the indexes, the indexes of the connected bus and the subnet name
            of the equivalent/elements of a specific grid (for load and sgen
            elements)
            (('sgen', 69, 63, 'HV2_MV1_eq'),(),...)

            **epf** (str) - String that specifies the used EPF
    """

    for unit_type, idx, bus, name in idx_eq:
        # Cost creation
        grid_epf_coefficients = []
        if unit_type == "sgen":
            if epf == "EPF1":
                grid_epf_coefficients = object1.coefficients_EPF1
            elif epf == "EPF2":
                grid_epf_coefficients = object1.coefficients_EPF2
            elif epf == "EPF3":
                grid_epf_coefficients = object1.coefficients_EPF3

            pp.create_poly_cost(net, idx, unit_type,
                                cp1_eur_per_mw=0,
                                cq0_eur=grid_epf_coefficients[2],
                                cq1_eur_per_mvar=grid_epf_coefficients[1],
                                cq2_eur_per_mvar2=grid_epf_coefficients[0])

            # Constraint creation
            net[unit_type].loc[idx, "min_q_mvar"] = object1.Q_min_eq_sgen
            net[unit_type].loc[idx, "max_q_mvar"] = object1.Q_max_eq_sgen
            net[unit_type].loc[idx, "controllable"] = True

            if object1.P_net <= 0:
                net[unit_type].loc[idx, "min_p_mw"] = -object1.P_net
                net[unit_type].loc[idx, "max_p_mw"] = -object1.P_net

            else:
                net[unit_type].loc[idx, "min_p_mw"] = 0
                net[unit_type].loc[idx, "max_p_mw"] = 0
                net[unit_type].loc[idx, "p_mw"] = 0

        elif unit_type == "load":

            net[unit_type].loc[idx, "controllable"] = False
            net[unit_type].loc[idx, "q_mvar"] = 0
            if object1.P_net > 0:
                net[unit_type].loc[idx, "p_mw"] = object1.P_net
            else:
                net[unit_type].loc[idx, "p_mw"] = 0


def set_up_non_eq(net, idx_non_eq: tuple, units_in_grid: bool):
    """ Sets constraints and costs for all non-equivalent elements of a
        simbench net.

        INPUT:
            **net** - The pandapower format network

            **idx_non_eq** (tuple) - Tuple that contains tuples of the unit
            type, the indexes, the indexes of the connected bus and the subnet
            name of the non equivalent elements

            **units_in_grid** (bool) - States whether directly to the grid
            coupled flexibility units can offer their reactive power
            flexibility in the market.
            True: Directly to the grid coupled units can participate in market
            False: Only downstream networks are able to participate in market
    """

    flex_ele_current_v_level = []
    for unit_type, idx, bus, name in idx_non_eq:

        if unit_type in parameter.flexibility_units:
            if net[unit_type].loc[idx, "sn_mva"] == 0 or not units_in_grid:
                net[unit_type].loc[idx, "controllable"] = False
            else:
                flex_ele_current_v_level.append((unit_type, idx, bus, name))
                q_flexibility = (net[unit_type].loc[idx, "sn_mva"] ** 2
                                 - net[unit_type].loc[idx, "p_mw"] ** 2) ** 0.5
                net[unit_type].loc[idx, "min_p_mw"] = net[unit_type].loc[idx, "p_mw"]
                net[unit_type].loc[idx, "max_p_mw"] = net[unit_type].loc[idx, "p_mw"]
                net[unit_type].loc[idx, "min_q_mvar"] = -q_flexibility
                net[unit_type].loc[idx, "max_q_mvar"] = q_flexibility
                net[unit_type].loc[idx, "controllable"] = True

        else:
            net[unit_type].loc[idx, "controllable"] = False

    return tuple(flex_ele_current_v_level)


def set_upstream_net_constraints(net, slack_voltage_vm_pu: float = 1):
    """ Sets voltage, line and trafo constraints as well as constraints for
        the external grid.

        INPUT:
            **net** - The pandapower format network

            **slack_voltage_vm_pu** - Voltage of the external grid, default
            value is 1 pu
    """
    # Constraints and cost_function for the external grid
    set_ext_grid_constraints(net, slack_voltage_vm_pu)

    # Constraints for bus voltages, line and trafo loadings
    set_voltage_constraints(net)
    set_line_and_trafo_constraints(net)


def set_constrains_flexibility_units(net, index_dict):
    """
        Sets the constraints for flexibilities for a calculation with
        different grids and voltage levels.

        INPUT:
            **net** - The pandapower format network

            **index_dict** Dictionary that contains list of indexes for
                specific network elements
    """
    for units in parameter.flexibility_units:
        for voltage_levels in parameter.voltage_levels:
            for index in index_dict[(units, voltage_levels, 'non_eq')]:
                if net[units].loc[index, "sn_mva"] != 0:
                    q_flexibility = (net[units].loc[index, "sn_mva"] ** 2 -
                                     net[units].loc[index, "p_mw"] ** 2) ** 0.5

                    net[units].loc[index, "min_p_mw"] = net[units].loc[
                        index, "p_mw"]
                    net[units].loc[index, "max_p_mw"] = net[units].loc[
                        index, "p_mw"]
                    net[units].loc[index, "min_q_mvar"] = -q_flexibility
                    net[units].loc[index, "max_q_mvar"] = q_flexibility
                    net[units].loc[index, "controllable"] = True

