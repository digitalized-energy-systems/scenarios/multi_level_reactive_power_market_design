"""
Auxiliary functions for the creation of a pandapower OPF
based reactive power market

"""
import logging
import pandapower as pp

import parameter

logger = logging.getLogger(__name__)


def loss_calculation(net):
    """ Calculation of active and reactive losses of a network.

        INPUT:
            **net** - The pandapower format network

        OUTPUT:
            **p_losses_total** (float) - Active power losses of the grid  in MW

            **q_losses_total** (float) - Reactive power losses of the grid in
            MVar
    """

    p_losses_lines = 0.
    q_losses_lines = 0.
    for index, row in net.res_line.iterrows():
        p_losses_lines += row["pl_mw"]
        q_losses_lines += row["ql_mvar"]

    p_losses_trafos = 0.
    q_losses_trafos = 0.
    for index, row in net.res_trafo.iterrows():
        p_losses_trafos += row["pl_mw"]
        q_losses_trafos += row["ql_mvar"]

    p_losses_total = p_losses_lines + p_losses_trafos
    q_losses_total = q_losses_lines + q_losses_trafos

    return [p_losses_total, q_losses_total]


def flex_units_q_production(net):
    """ Calculation of the reactive power output of the flexibility units.

        INPUT:
            **net** - The pandapower format network

        OUTPUT:
            **total_q_production** (float) - Reactive Power Production in MVar
    """

    total_q_production = 0.
    for unit_type in parameter.flexibility_units:
        for index, row in net["res_" + unit_type].iterrows():
            if net[unit_type].loc[index, "controllable"] == True:
                total_q_production += row["q_mvar"]

    return total_q_production


def max_reactive_power_flex_units(net):
    """ Calculation of the total providable reactive power sum of the flexibility units.

        INPUT:
            **net** - The pandapower format network

        Output:
            **search_interval** (list) - List the contains Q_min/Q_max of the flexibility units.
    """
    q_min_flex_units = 0.
    q_max_flex_units = 0.

    for unit_type in parameter.flexibility_units:

        if not net[unit_type].empty:
            for index, row in net.poly_cost.iterrows():
                if row["et"] == unit_type:
                    q_min_flex_units += net[unit_type].loc[row["element"], "min_q_mvar"]
                    q_max_flex_units += net[unit_type].loc[row["element"], "max_q_mvar"]

    search_interval = [q_min_flex_units, q_max_flex_units]
    return search_interval


def rated_app_power(net, profiles):
    """
        Calculation of the rated apparent power of a unit. This is done on the
        basis of a units maximum active power injection in a year.
        Adjusts the predefined value "sn_mva" in dataframe.

        INPUT:
            **net** - The pandapower format network

            **profiles** (dict) - Dictionary that contains the predefined
            profiles of p and q values of a year
    """
    sn_mva = []
    installed_power_mw = []
    for index, row in net.sgen.iterrows():
        installed_power_mw.append(profiles[('sgen', 'p_mw')][index].max())
        if row["voltLvl"] == 3:
            sn_mva.append(profiles[('sgen', 'p_mw')][index].max()/0.95)
        elif row["voltLvl"] == 5:
            sn_mva.append(profiles[('sgen', 'p_mw')][index].max()/0.95)
    net.sgen["sn_mva"] = sn_mva
    net.sgen["installed_power_mw"] = installed_power_mw


def apply_absolute_values(net, absolute_values: dict, case_or_time_step):
    """ Applies absolute values to the units.
        Source: https://github.com/e2nIEE/simbench/blob/master/tutorials/simbench_grids_basics_and_usage.ipynb

        INPUT:
            **net** - The pandapower format network

            **absolute_values** (dict) - Dictionary that contains the absolute
            profile values

            **case_or_time_step** - Time step if a time series calculation is
            carried out or the case ('hL', 'n1', ..) for a case study
    """
    for element_param in absolute_values.keys():
        if absolute_values[element_param].shape[1]:
            element = element_param[0]
            param = element_param[1]
            net[element].loc[:, param] = absolute_values[element_param].loc[case_or_time_step]


def units_in_wind_park(net, rough_apparent_power_unit):
    """
        Function for the calculation of the number of n equal wind power plants
        in a wind park.

        INPUT:
            **net** - The pandapower format network

            **rough_apparent_power_unit** - Wished apparent power in MVA for one
            unit in the wind park. Due to the requirement of n equal units the
            apparent power will deviate from the chosen one.
    """
    units_higher_wished_peak = net.sgen["sn_mva"] > rough_apparent_power_unit
    wind_type_units = net.sgen["type"] == "Wind"

    net.sgen["unit_number"] = (net.sgen.loc[units_higher_wished_peak & wind_type_units, "sn_mva"]
                               / rough_apparent_power_unit).round()
    net.sgen["sn_mva_max_unit"] = net.sgen["sn_mva"] / net.sgen["unit_number"]


def create_1_bus_test_grid():
    """ Creation of a simple 1 bus test grid. Contains one slack, one bus and
    generating units. """

    net = pp.create_empty_network()

    # Min/max voltage has no influence in this network, everything is connected
    # to the slack bus
    min_vm_pu = .99
    max_vm_pu = 1.001
    bus1 = pp.create_bus(net, vn_kv=20, min_vm_pu=min_vm_pu, max_vm_pu=max_vm_pu)

    # Creation of the external grid and its constraints
    pp.create_ext_grid(net, bus1, vm_pu=1.)
    net.ext_grid["min_p_mw"] = -2000
    net.ext_grid["max_p_mw"] = 2000
    net.ext_grid["min_q_mvar"] = -2000
    net.ext_grid["max_q_mvar"] = 2000

    pp.create_sgen(net, bus1, p_mw=0, min_p_mw=0, max_p_mw=0,
                   min_q_mvar=-1, max_q_mvar=1, sn_mva=1,
                   controllable=True)
    pp.create_sgen(net, bus1, p_mw=0, min_p_mw=0, max_p_mw=0,
                   min_q_mvar=-1, max_q_mvar=1, sn_mva=1,
                   controllable=True)

    # Create cost functions
    pp.create_poly_cost(net, 0, 'ext_grid', cp1_eur_per_mw=0, cq1_eur_per_mvar=-500)
    pp.create_poly_cost(net, 0, 'sgen', cp1_eur_per_mw=0, cq2_eur_per_mvar2=1)
    pp.create_poly_cost(net, 1, 'sgen', cp1_eur_per_mw=0, cq2_eur_per_mvar2=5)

    return net


def create_simple_2_bus_test_grid():
    """ Creation of a simple 2 bus test grid. Contains one slack, one bus,
    one load and generators. """

    net = pp.create_empty_network()

    min_vm_pu = .9
    max_vm_pu = 1.1
    bus1 = pp.create_bus(net, vn_kv=20, min_vm_pu=min_vm_pu, max_vm_pu=max_vm_pu)
    bus2 = pp.create_bus(net, vn_kv=20, min_vm_pu=min_vm_pu, max_vm_pu=max_vm_pu)

    pp.create_line(net, from_bus=bus1, to_bus=bus2, length_km=20,
                   std_type="NA2XS2Y 1x240 RM/25 12/20 kV")
    pp.create_load(net, bus2, p_mw=0, q_mvar=0, controllable=False)

    # Create external grid and its constraints
    pp.create_ext_grid(net, bus1, vm_pu=1.)
    net.ext_grid["min_p_mw"] = -1000
    net.ext_grid["max_p_mw"] = 1000
    net.ext_grid["min_q_mvar"] = -1000
    net.ext_grid["max_q_mvar"] = 1000

    pp.create_sgen(net, bus2, p_mw=0, min_p_mw=0, max_p_mw=0,
                   min_q_mvar=-1, max_q_mvar=1, sn_mva=1,
                   controllable=True)
    pp.create_sgen(net, bus2, p_mw=0, min_p_mw=0, max_p_mw=0,
                   min_q_mvar=-1, max_q_mvar=1, sn_mva=1,
                   controllable=True)

    # Create cost functions
    pp.create_poly_cost(net, 0, 'ext_grid', cp1_eur_per_mw=200, cq1_eur_per_mvar=-500)
    pp.create_poly_cost(net, 0, 'sgen', cp1_eur_per_mw=0, cq2_eur_per_mvar2=1)
    pp.create_poly_cost(net, 1, 'sgen', cp1_eur_per_mw=0, cq2_eur_per_mvar2=5)

    return net


def create_simple_3_bus_test_grid():
    """ Creation of a simple 3 bus test grid. Contains one slack, one bus,
    one load and generators. """

    net = pp.create_empty_network()

    min_vm_pu = .1
    max_vm_pu = 1.9
    bus1 = pp.create_bus(net, vn_kv=20, min_vm_pu=min_vm_pu, max_vm_pu=max_vm_pu)
    bus2 = pp.create_bus(net, vn_kv=20, min_vm_pu=min_vm_pu, max_vm_pu=max_vm_pu)
    bus3 = pp.create_bus(net, vn_kv=20, min_vm_pu=min_vm_pu, max_vm_pu=max_vm_pu)

    # Create lines
    pp.create_line(net, from_bus=bus1, to_bus=bus2, length_km=20, std_type="NA2XS2Y 1x240 RM/25 12/20 kV")
    pp.create_line(net, from_bus=bus2, to_bus=bus3, length_km=20, std_type="NA2XS2Y 1x240 RM/25 12/20 kV")
    pp.create_line(net, from_bus=bus3, to_bus=bus1, length_km=20, std_type="NA2XS2Y 1x240 RM/25 12/20 kV")

    pp.create_load(net, bus3, p_mw=0.02, q_mvar=0, controllable=False)

    # Create external grid and its constraints
    pp.create_ext_grid(net, bus1, vm_pu=1.)
    net.ext_grid["min_p_mw"] = -1000
    net.ext_grid["max_p_mw"] = 1000
    net.ext_grid["min_q_mvar"] = -1000
    net.ext_grid["max_q_mvar"] = 1000

    pp.create_sgen(net, bus2, p_mw=0.02, min_p_mw=0.02,
                   max_p_mw=0.02,
                   min_q_mvar=-(0.04**2 - 0.02**2)**0.5,
                   max_q_mvar=(0.04**2 - 0.02**2)**0.5,
                   sn_mva=0.04, controllable=True)
    pp.create_sgen(net, bus3, p_mw=0.01, min_p_mw=0.01,
                   max_p_mw=0.01,
                   min_q_mvar=-(0.04**2 - 0.01**2)**0.5,
                   max_q_mvar=(0.04**2 - 0.01**2)**0.5,
                   sn_mva=0.04, controllable=True)

    return net
