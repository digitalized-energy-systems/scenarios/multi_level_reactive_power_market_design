"""
    Different functions that either calculate specific costs
    or set cost related constraints.
"""

import numpy as np
import math as m

import pandapower as pp

from functions.pp_aux import loss_calculation
import parameter


def create_cost_functions(net, price_active_power: float):
    """ Creates a polynomial reactive power cost function for every unit of
        a specific unit type. Additionally, a linear active power cost function
        for the external grid is created.

        INPUT:
            **net** - The pandapower format network

            **price_active_power** (float) - Price for active power in €/MW at
            the ext_grid.
    """

    pp.create_poly_cost(net, 0, 'ext_grid',
                        cp1_eur_per_mw=price_active_power)

    for elements in parameter.flexibility_units:
        for index, row in net[elements].iterrows():

            if row['controllable'] is True:
                pp.create_poly_cost(net, index, elements,
                                    cp1_eur_per_mw=0,
                                    cq0_eur=0,
                                    cq1_eur_per_mvar=0,
                                    cq2_eur_per_mvar2=parameter.DER_cq2_eur_per_mvar2)


def costs_q_flex_units(net, only_one_vlt_lvl=False, volt_LvL=5, market=False):
    """ Calculation of the total costs for the provision of the reactive power
        by the variable units.

        INPUT:
            **net** - The pandapower format network

            **only_one_vlt_lvl** (boolean)- Boolean that specifies whether only
            the cost of one voltage level should be calculated

            **volt_LvL** (integer)- If an integer is given only the costs of
            flexibilities for the specific voltage level are calculated

            **market** (boolena)- Specifies whether the cost are calculated
            for the market environment

        OUTPUT:
            **cost_q_flex** (float) - Sum of costs for the reactive power
            provision by all flexible units
    """

    cost_q_flex = 0.
    if only_one_vlt_lvl is False:
        for index, row in net.poly_cost.iterrows():
            if row["et"] in parameter.flexibility_units \
                    and net[row["et"]].loc[row["element"], "controllable"] == True:

                q_output_element = net["res_" + row["et"]].loc[row["element"], "q_mvar"]
                cost_q_flex += (row["cq0_eur"]
                                + row["cq1_eur_per_mvar"] * q_output_element
                                + row["cq2_eur_per_mvar2"] * q_output_element ** 2)
    elif only_one_vlt_lvl is True and market is False:
        flexibility_df = net.sgen[net.sgen.controllable == True]
        indexes_flex_units_vlt_lvl = flexibility_df[flexibility_df.voltLvl == volt_LvL].index
        for index, row in net.poly_cost.iterrows():
            if row["et"] in parameter.flexibility_units and net[row["et"]].loc[row["element"], "controllable"] == True:
                if row["element"] in indexes_flex_units_vlt_lvl:
                    q_output_element = net["res_" + row["et"]].loc[
                        row["element"], "q_mvar"]
                    cost_q_flex += (row["cq0_eur"]
                                    + row["cq1_eur_per_mvar"] * q_output_element
                                    + row[
                                        "cq2_eur_per_mvar2"] * q_output_element ** 2)
    elif only_one_vlt_lvl is True and market is True:
        flexibility_df = net.sgen[net.sgen.controllable == True]
        non_eq_flexibility_df = flexibility_df[flexibility_df.subnet == "HV2"]
        indexes_flex_units_vlt_lvl = non_eq_flexibility_df[non_eq_flexibility_df.voltLvl == volt_LvL].index
        for index, row in net.poly_cost.iterrows():
            if row["et"] in parameter.flexibility_units and net[row["et"]].loc[row["element"], "controllable"] == True:
                if row["element"] in indexes_flex_units_vlt_lvl:
                    q_output_element = net["res_" + row["et"]].loc[
                        row["element"], "q_mvar"]
                    cost_q_flex += (row["cq0_eur"]
                                    + row[
                                        "cq1_eur_per_mvar"] * q_output_element
                                    + row[
                                        "cq2_eur_per_mvar2"] * q_output_element ** 2)

    return cost_q_flex


def loss_costs(net):
    """Calculation of costs for active power losses."""
    [p_loss, q_loss] = loss_calculation(net)
    active_power_losses = p_loss * net.poly_cost.loc[0, "cp1_eur_per_mw"]

    return active_power_losses


def poly_function(x_values: list, y_values: list, degree: int):
    """ Fits a polynomial function of a variable degree through given
        data-points. Returns a vector with coefficients in the
        order deg, deg-1, … 0 that minimises the squared error.

        INPUT:
            **x_values** (list) - List that contains x-values

            **y_values** (list)- List that contains y-values

            **degree** (int)- Degree of the polynomial fit

        OUTPUT:
            **coefficients** (tuple) -  Tuple with coefficients in the
            order deg, deg-1, … 0
    """

    coefficients = np.polyfit(x_values, y_values, degree)

    return coefficients


def poly_function_values(coefficients, start_point: float,
                         end_point: float, point_number: int):
    """ Creates lists with the x and y values of a polynomial function.

        INPUT:
            **coefficients** (numpy.array) - List with coefficients of the
            polynomial function in the order deg, deg-1, … 0.

            **start_point** (float)- Start point of the interval

            **end_point** (float)- End point of the interval

            **point_number** (int)- Number of points in the interval

        Output:
            **function_values** (list) - List with dimension (2, point_number)
            First row contains all the x, second all corresponding y values
    """

    x = np.linspace(start_point, end_point, point_number, endpoint=True)
    fct_values = []
    for data in range(len(x)):
        fct_value = 0.
        for coefficient in range(coefficients.size):
            fct_value += coefficients[-(coefficient + 1)] \
                         * m.pow(x[data], coefficient)
        fct_values.append(fct_value)

    function_values = [x.tolist(), fct_values]
    return function_values


def set_costs_non_eq(net, idx_non_eq: tuple, units_in_grid: bool):
    """ Sets constraints and costs for all non equivalent elements of a
        simbench net.

        INPUT:
            **net** - The pandapower format network

            **idx_non_eq** (tuple) - Tuple that contains tuples of the unit
            type and the index of the non equivalent elements

            **units_in_grid** (bool) - States whether directly to the grid
            coupled flexibility units can offer their reactive power
            flexibility in the market.
            True: Directly to the grid coupled units can participate in market
            False: Only downstream networks are able to participate in market
    """
    for unit_type, idx, bus, name in idx_non_eq:
        if unit_type in parameter.flexibility_units:
            if net[unit_type].loc[idx, "sn_mva"] != 0 and units_in_grid:
                quad_cost_co = parameter.DER_cq2_eur_per_mvar2
                if m.isnan(net[unit_type].loc[idx, "unit_number"]):
                    pp.create_poly_cost(net, idx, unit_type,
                                        cp1_eur_per_mw=0,
                                        cq0_eur=0,
                                        cq1_eur_per_mvar=0,
                                        cq2_eur_per_mvar2=quad_cost_co)
                else:
                    pp.create_poly_cost(net, idx, unit_type,
                                        cp1_eur_per_mw=0,
                                        cq0_eur=0,
                                        cq1_eur_per_mvar=0,
                                        cq2_eur_per_mvar2=quad_cost_co
                                                          / net[unit_type].loc[idx, "unit_number"])


def set_costs_flexi_units(net, index_dict):
    """
        Sets the costs for the flexibility for a calculation with
        different grids and voltage levels. Wind parks will get an adjusted
        cost coefficient.

        INPUT:
            **net** - The pandapower format network

            **index_dict** Dictionary that contains list of indexes for
            specific network elements
    """
    for units in parameter.flexibility_units:
        for voltage_levels in parameter.voltage_levels:
            for index in index_dict[(units, voltage_levels, 'non_eq')]:
                if net[units].loc[index, "sn_mva"] != 0:
                    quad_cost_co = parameter.DER_cq2_eur_per_mvar2
                    if "unit_number" not in net[units] or m.isnan(
                            net[units].loc[index, "unit_number"]):
                        pp.create_poly_cost(net, index, units,
                                            cp1_eur_per_mw=0,
                                            cq0_eur=0,
                                            cq1_eur_per_mvar=0,
                                            cq2_eur_per_mvar2=quad_cost_co)
                    else:
                        pp.create_poly_cost(net, index, units,
                                            cp1_eur_per_mw=0,
                                            cq0_eur=0,
                                            cq1_eur_per_mvar=0,
                                            cq2_eur_per_mvar2=quad_cost_co /
                                                              net[units].loc[
                                                                  index, "unit_number"])


def set_costs_flexi_units_to_zero(net, index_dict):
    """
        Sets the costs for the flexibility for a calculation with
        different grids and voltage levels to zero.

        INPUT:
            **net** - The pandapower format network

            **index_dict** Dictionary that contains list of indexes for
            specific network elements
    """
    for units in parameter.flexibility_units:
        for voltage_levels in parameter.voltage_levels:
            for index in index_dict[(units, voltage_levels, 'non_eq')]:
                if net[units].loc[index, "sn_mva"] != 0:
                    pp.create_poly_cost(net, index, units,
                                        cp1_eur_per_mw=0,
                                        cq0_eur=0,
                                        cq1_eur_per_mvar=0,
                                        cq2_eur_per_mvar2=0)
