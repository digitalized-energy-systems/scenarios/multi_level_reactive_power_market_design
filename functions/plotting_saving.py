"""
    Plotting and saving of the results of multi level market results for a single grid.
"""
import matplotlib.pyplot as plt
import os
import csv

from functions.cost_fct import poly_function_values


def plot_epf_results(algorithm_res, show_plot):
    """ Different plots of the results for one complete grid.

        INPUT:
            **algorithm_res** - Object that contains the results of the
            epf_algorithm for a specific grid

            **show_plot** (bool) - Boolean that states whether the EPFs and
            other graphs should be plot after the epf_algorithm
    """

    if show_plot:

        approx_EPF1 = poly_function_values(algorithm_res.coefficients_EPF1, algorithm_res.Q_min_eq_sgen, algorithm_res.Q_max_eq_sgen, 50)
        # approx_EPF2 = poly_function_values(algorithm_res.coefficients_EPF2, algorithm_res.Q_min_eq_sgen, algorithm_res.Q_max_eq_sgen, 50)
        approx_EPF3 = poly_function_values(algorithm_res.coefficients_EPF3, algorithm_res.Q_min_eq_sgen, algorithm_res.Q_max_eq_sgen, 50)

        # Plot of approximated and calculated values of EPF1
        plt.title('Grid ' + algorithm_res.name)
        plt.plot(algorithm_res.res_df["q_eq_sgen"], algorithm_res.res_df.loc[:, "EPF1"], "o", label="Calculated EPF")
        plt.plot(approx_EPF1[0], approx_EPF1[1], label="$w = 1$")
        # plt.plot(approx_EPF2[0], approx_EPF2[1], label="$w = 100000$")
        plt.plot(approx_EPF3[0], approx_EPF3[1], label="$w = 1000$")
        plt.xlabel('Reactive power $Q_{\mathrm{PCC}}$ at PCC in MVAr', fontsize=15)
        plt.ylabel('Costs $C$ in €/h', fontsize=15)
        plt.legend(fontsize=14)
        plt.yticks(fontsize=12)
        plt.xticks(fontsize=12)
        plt.grid(False)
        plt.show()

        # Plot of the costs that grid operator has depending on the used EPF
        plt.title('Cost of Grid Operator - with different EPFs')
        plt.plot(algorithm_res.res_df["q_eq_sgen"], algorithm_res.res_df["EPF1_operator_costs"], "o--", label="EPF1")
        plt.plot(algorithm_res.res_df["q_eq_sgen"], algorithm_res.res_df["EPF2_operator_costs"], "o--", label="EPF2")
        plt.plot(algorithm_res.res_df["q_eq_sgen"], algorithm_res.res_df["EPF3_operator_costs"], "o--", label="EPF3")
        plt.xlabel('Reactive power $Q_{\mathrm{PCC}}$ at PCC in MVAr', fontsize=13)
        plt.ylabel('Cost of Grid Operator in €/h', fontsize=13)
        plt.legend(fontsize=13)
        plt.grid(True)
        plt.show()


def saving(algorithm_res, save_result, time_step=0):
    """ Saves the result dataframe of the epf_algorithm as a csv file.

        INPUT:
            **algorithm_res** - Object that contains the results of the
            epf_algorithm for a specific grid

            **save_result** (bool) - Boolean that states whether the result
            dataframe should be saved

            **time_step** (int) - Time step of the time series calculation. If
            no time series calculation is done, the time step is the
            default value "0".
    """

    if save_result:
        res_dir = os.path.join(os.getcwd(), 'Results_epf_algorithm',
                               algorithm_res.name)

        os.makedirs(res_dir, exist_ok=True)

        algorithm_res.res_df.to_csv(os.path.join(res_dir, 'Dataframe_Timestep_' + str(time_step) + '.csv'))

        results_dict = {"name": algorithm_res.name,
                        "price_active_power": algorithm_res.price_active_power,
                        "SearchInterval": algorithm_res.SearchInterval,
                        "Q_min_ext": algorithm_res.Q_min_ext,
                        "Q_max_ext": algorithm_res.Q_max_ext,
                        "Q_max_eq_sgen": algorithm_res.Q_max_eq_sgen,
                        "Q_min_eq_sgen": algorithm_res.Q_min_eq_sgen,
                        "P_net": algorithm_res.P_net,
                        "base_case_cost": algorithm_res.base_case_cost,
                        "base_case_q_eq_sgen": algorithm_res.base_case_q_eq_sgen,
                        "lin_spline_min_vm_pu": algorithm_res.lin_spline_min_vm_pu,
                        "lin_spline_max_vm_pu": algorithm_res.lin_spline_max_vm_pu,
                        "lin_spline_l_loading": algorithm_res.lin_spline_l_loading,
                        "lin_spline_t_loading": algorithm_res.lin_spline_t_loading,
                        "coefficients_EPF1": algorithm_res.coefficients_EPF1,
                        "coefficients_EPF2": algorithm_res.coefficients_EPF2,
                        "coefficients_EPF3": algorithm_res.coefficients_EPF3}

        file_name = 'Object_res_' + str(time_step) + '.csv'

        with open(os.path.join(res_dir, file_name), 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=results_dict.keys(), lineterminator='\n')
            writer.writeheader()
            writer.writerow(results_dict)
