import os
import parameter
from evaluation_functions import visualisation_functions as graph
import matplotlib.pyplot as plt

plt.rcParams["font.family"] = "serif"


def main():
    directory_result_folder = os.path.join(os.getcwd(), 'Results')
    print(directory_result_folder)
    epf_loading_list = ["EPF3"]
    plot_epf = "EPF3"
    tap_position = "ct"

    results_dict, results_legend_dict = graph.load_main_res_files(directory_result_folder, epf_loading_list)

    plot_results(results_dict, directory_result_folder, plot_epf, tap_position, volt_analysis=True, loading_analysis=True, costs_analysis=True, q_analysis=True)


def plot_results(results, directory_result_folder, plot_epf, tap_position, volt_analysis, loading_analysis, costs_analysis, q_analysis):
    """
        Starts the plotting of the evaluation results. The current visualisation
        allows comparing 3 different cases. Currently, the two reference cases
        and one market result are compared. Specify the wished EPF of the market in the main
        (EPF3: quadratic EPF with weighted base case/w=1000).
        It is possible to specify the tap optimization methode
        (nt - no tap optimization, dt- discrete tap optimization, ct - continuous
        tap optimization) which is to be analysed.


    INPUT:
        **results** - Dictionary that contains dataframes of the loaded CSV file

        **directory_result_folder** - Directory of the result results folder

        **plot_epf** - String that specifies the EPF that should be plotted (e.g. "EPF3")

        **tap_position** - String that specifies the tap optimization that should
        be compared (nt - no tap optimization, dt- discrete tap optimization,
        ct - continuous tap optimization)

        **volt_analysis** (boolean) - Boolean that specifies whether a voltage
        comparison plot should be given out

        **loading_analysis** (boolean) - Boolean that specifies whether a loadig
        comparison plot should be given out

        **costs_analysis** (boolean) - Boolean that specifies whether a cost
        comparison plot should be given out

        **q_analysis** (boolean) - Boolean that specifies whether a reactive power
        comparison plot should be given out
    """

    results_of_interest = ["market_" + plot_epf + "_" + tap_position, "ideal_optimum_" + tap_position, "status_quo_" + tap_position]

    legend_dict = {"status_quo_" + tap_position: "RC NoMarket",
                   "ideal_optimum_" + tap_position: "RC OptMarket",
                   "market_EPF1_" + tap_position: "ML Market",
                   "market_EPF2_" + tap_position: "ML Market",
                   "market_EPF3_" + tap_position: "ML Market"}

    if volt_analysis:
        detailed_voltage = graph.load_detailed_res_files(directory_result_folder, tap_position, "voltage_min_max",
                                                         epf=plot_epf)

        HV_min_vm_pu = {key: detailed_voltage[key][detailed_voltage[key].vlt_level == 3].min_vm_pu for key in results_of_interest}
        HV_max_vm_pu = {key: detailed_voltage[key][detailed_voltage[key].vlt_level == 3].max_vm_pu for key in results_of_interest}
        MV_min_vm_pu = {key: detailed_voltage[key][detailed_voltage[key].vlt_level == 5].min_vm_pu for key in results_of_interest}
        MV_max_vm_pu = {key: detailed_voltage[key][detailed_voltage[key].vlt_level == 5].max_vm_pu for key in results_of_interest}

        hv_volt_comparison = graph.set_up_grouped_boxplot([HV_min_vm_pu, HV_max_vm_pu, MV_min_vm_pu, MV_max_vm_pu],
                                                          ["$V_\mathrm{min}^\mathrm{HV}$", "$V_\mathrm{max}^\mathrm{HV}$",
                                                          "$V_\mathrm{min}^\mathrm{MV}$", "$V_\mathrm{max}^\mathrm{MV}$"],
                                                          legend_dict)

        graph.voltage_analysis_grouped(hv_volt_comparison, voltage_level="HV_V", nbr_col_legend=4, graphic_title="Minimum and maximum node voltages")

    if loading_analysis:
        detailed_loading_t = graph.load_detailed_res_files(directory_result_folder, tap_position, "loading_t_max",
                                                           epf=plot_epf)

        EHVHV_max_trafo_load = {key: detailed_loading_t[key][detailed_loading_t[key].vlt_level == 2].loading_percent for key in results_of_interest}
        HVMV_max_trafo_loading = {key: detailed_loading_t[key][detailed_loading_t[key].vlt_level == 4].loading_percent for key in results_of_interest}

        detailed_loading_l = graph.load_detailed_res_files(directory_result_folder, tap_position, "loading_l_max",
                                                           epf=plot_epf)
        HV_max_line_loading = {key: detailed_loading_l[key][detailed_loading_l[key].vlt_level == 3].loading_percent for key in results_of_interest}
        MV_max_line_loading = {key: detailed_loading_l[key][detailed_loading_l[key].vlt_level == 5].loading_percent for key in results_of_interest}

        t_loading_comparison = graph.set_up_grouped_boxplot([EHVHV_max_trafo_load, HVMV_max_trafo_loading], ["EHV/HV", "HV/MV"], legend_dict)
        graph.loading_analysis_grouped(t_loading_comparison, graphic_title="Maximum transformer loadings", plot_h_line=False)

        l_loading_comparison = graph.set_up_grouped_boxplot([HV_max_line_loading, MV_max_line_loading], ["HV grid", "MV grids"], legend_dict)
        graph.loading_analysis_grouped(l_loading_comparison, graphic_title="Maximum line loadings", nbr_col_legend=2, plot_h_line=True)

    if costs_analysis:
        total_cost = {key: results[key].total_cost for key in results_of_interest}

        graph.cost_analysis(total_cost, legend_dict)

        HV_total_cost = {key: results[key].Q_flexibility_cost_HV + results[key].p_losses_MV * parameter.price_active_power for key in results_of_interest}
        MV_total_cost = {key: results[key].Q_flexibility_cost_MV + results[key].p_losses_HV * parameter.price_active_power for key in results_of_interest}
        cost_by_v_lvl = graph.set_up_grouped_boxplot([HV_total_cost, MV_total_cost], ["HV grid", "MV grids"], legend_dict)
        graph.cost_analysis_grouped(cost_by_v_lvl, graphic_title="Total costs separated by voltage level")

    if q_analysis:
        Q_provided = {key: results[key].Q_abs_flexibility_sum for key in results_of_interest}
        HV_Q_provided = {key: results[key].Q_abs_flexibility_HV for key in results_of_interest}
        MV_Q_provided = {key: results[key].Q_abs_flexibility_MV for key in results_of_interest}

        graph.reactive_power_analysis(Q_provided, legend_dict, graphic_title="Reactive power provided by flexibilities")

        Q_comparison = graph.set_up_grouped_boxplot([HV_Q_provided, MV_Q_provided],
                                                    ["$Q_\mathrm{HV}$", "$Q_\mathrm{MV}$"], legend_dict)

        graph.reactive_power_analysis_grouped(Q_comparison, graphic_title="Reactive power provided by flexibilities")


if __name__ == '__main__':
    main()
