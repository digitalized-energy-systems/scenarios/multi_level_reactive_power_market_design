import simbench as sb
import pandas as pd

import os
import random

import parameter

from evaluation_functions import general_evaluation, RC_NoMarket_specific, store_results as store, mapping
import logging
logger = logging.getLogger(__name__)


def main():
    RC_NoMarket(nbr_of_samples=5000)


def RC_NoMarket(nbr_of_samples):

    complete_net = sb.get_simbench_net('1-HVMV-urban-all-0-no_sw')
    initial_tap_settings, profiles_complete_grid, idx_controllables_complete_grid = RC_NoMarket_specific.set_up_complete_grid(complete_net)

    hv_net = sb.get_simbench_net(parameter.SimBenchCodes["HV2"] + parameter.switch_position)

    mv_grid_names = mapping.downstream_grid_names(hv_net)
    mv_nets, downstream_profiles, downstream_flexibility_units = general_evaluation.prepare_downstream_grids(mv_grid_names)

    idx_eq_sorted, idx_eq, idx_non_eq, sgen_eq = mapping.idx_dataframes(hv_net, mv_grid_names)

    profiles_hv = RC_NoMarket_specific.set_up_hv_and_mv_nets(hv_net, mv_nets)

    random.seed(1)
    sequence = profiles_hv[('load', 'p_mw')].index.tolist()
    time_steps = random.sample(sequence, nbr_of_samples)

    print(time_steps)

    res_dir, res_files, res_files_voltage, res_files_loading, slack_values_mv_grids = \
        store.create_results_csv_files("status_quo", save_slack_values_mw=True)

    RC_NoMarket_specific.ts_calculation(complete_net, profiles_complete_grid,
                                        idx_controllables_complete_grid, initial_tap_settings,
                                        hv_net, profiles_hv, mv_nets, downstream_profiles, slack_values_mv_grids,
                                        mv_grid_names, idx_non_eq, idx_eq_sorted, downstream_flexibility_units, res_dir,
                                        res_files, res_files_voltage, res_files_loading, time_steps)

    res_status_quo_df = pd.read_csv(os.path.join(res_dir, res_files["NT"]))
    print(res_status_quo_df.to_string())


if __name__ == '__main__':
    main()
