"""
Creation of different dictionaries and lists for cross-network calculations.
Equivalent and non-equivalent network elements of simbench grids are identified
and stored.
"""

import parameter


def idx_dataframes(net, ds_grids: tuple):
    """ Creation of tuples, which contain the indexes of the specific elements
        of the grid.

        INPUT:
            **net** - The pandapower format network

            **ds_grids** (tuple) - Tuple which contains the voltage levels of
            the downstream grids and their simbench number.
            Example: ('MV1', 'MV2', 'MV3', 'MV4')

        OUTPUT:
            **idx_eq_sorted** (dict) - Contains all indexes of equivalent
            elements in a simbench grid. Example:
            {'MV1': (('sgen', idx, bus_idx, 'subnet_name')),
            'MV2': (('sgen', idx, bus_idx, 'subnet_name'))...,}

            **idx_eq_all** (tuple) - Contains all indexes of equivalent
            elements in a simbench grid.

            **idx_non_eq_elements** (tuple) - Contains all indexes of
            non equivalent elements in a simbench grid. Example:
            (('sgen', idx1, bus_idx1, 'subnet_name'),
            ('sgen', idx2, bus_idx2, 'subnet_name'),...))

            **sgen_eq** (dict) - Dictionary that contains the indexes of
            _eq sgens and their downstream grid name: {66: 'MV1', 67: 'MV2'..}
    """

    # Searching for _eq and _non_eq elements in the simbench grid and write
    # lists with their index and additional infos
    idx_eq_all = []
    idx_non_eq_elements = []
    for unit_type in parameter.possible_flexibility_units:
        for index in net[unit_type].index:
            if "_eq" in net[unit_type].subnet.at[index]:

                idx_eq_all.append((unit_type, index, net[unit_type].loc[index, "bus"],
                                   net[unit_type].loc[index, "subnet"]))
            else:
                idx_non_eq_elements.append((unit_type, index, net[unit_type].loc[index, "bus"],
                                            net[unit_type].loc[index, "subnet"]))

    # Sorting the elements into a dictionary corresponding to their simbench
    # grid name
    # First: Create dictionary with a mapping code (to be able to write matrix)
    idx_eq_sorted_matrix = []
    idx_eq_sorted = {}
    for count, grid in enumerate(ds_grids):
        idx_eq_sorted[grid] = count
        idx_eq_sorted_matrix.append([])

    sgen_eq = {}
    for data in idx_eq_all:
        for grid in ds_grids:
            if grid in data[3]:
                idx_eq_sorted_matrix[idx_eq_sorted[grid]].append(data)

                if data[0] == "sgen":
                    sgen_eq[data[1]] = grid

    # Create the dictionary
    for key in idx_eq_sorted.keys():
        idx_eq_sorted[key] = tuple(idx_eq_sorted_matrix[idx_eq_sorted[key]])

    return idx_eq_sorted, tuple(idx_eq_all), tuple(idx_non_eq_elements), sgen_eq


def downstream_grid_names(net):
    """ Creation of a tuple, that contains the voltage levels of the
        downstream grids and their simbench number.

        INPUT:
            **net** - The pandapower format network

        OUTPUT:
            **ds_grids** (tuple) - Tuple which contains the voltage levels of
            the downstream grids and their simbench number.

        Example: ('MV1', 'MV2', 'MV3', 'MV4')
    """
    ds_grids = []
    for idx in net.sgen.index:
        if "_eq" in net.sgen.subnet.at[idx]:

            transition_seperation = net.sgen.loc[idx, 'subnet'].partition("_")
            transition_ele_1 = transition_seperation[2].partition("_")
            equivalent_name = transition_ele_1[0]

            if "." in equivalent_name:
                transition_ele_2 = equivalent_name.partition(".")
                equivalent_name = transition_ele_2[0]

            if equivalent_name not in ds_grids:
                ds_grids.append(equivalent_name)

    ds_grids.sort()
    return tuple(ds_grids)


def eq_from_non_eq_elements(net):
    """ Creates a dictionary which contains lists of the indexes of the "_eq"
        and "non_eq" flexibility elements of a grid

        INPUT:
            **net** - The pandapower format network

        OUTPUT:
            **eq_dict** (dict) - Dictionary which contains the unit_types
            and a list of booleans. "True" if a unit of that type is an
            equivalent unit, "False" otherwise

            **non_eq_dict** (dict) - Dictionary which contains the unit_types
            and a list of booleans. "True" if a unit of that type is not an
            equivalent unit, "False" otherwise
        EXAMPLE:
            eq_dict = {'sgen', [True,False..], 'gen': [False, True..], ...}
            non_eq_dict = {'sgen', [False, True..], 'gen': [True,False..], ...}
    """
    eq_dict = {}
    non_eq_dict = {}

    for unit_type in parameter.flexibility_units:
        if not net[unit_type].empty:
            eq_unit = []
            non_eq_unit = []

            for index in net[unit_type].index:
                if "_eq" in net[unit_type].loc[index, "subnet"]:
                    eq_unit.append(True)
                    non_eq_unit.append(False)
                else:
                    eq_unit.append(False)
                    non_eq_unit.append(True)

            eq_dict[unit_type] = eq_unit
            non_eq_dict[unit_type] = non_eq_unit

    return eq_dict, non_eq_dict
