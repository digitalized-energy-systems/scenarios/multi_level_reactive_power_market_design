import numpy as np
import csv
import os
from datetime import datetime
import pandas as pd

from functions.pp_aux import loss_calculation
from functions.cost_fct import costs_q_flex_units
from evaluation_functions.general_evaluation import dictionary_different_grids_in_complete_grid

import parameter


def results_dict(net, time_step, non_eq_dict, calc_mode: str, opf_converged: bool = True):
    """
        Creation of a dictionary that contains the results of the market or a reference case for one time step.

        INPUT:
            **net** - The pandapower format network

            **time_step** (int) - Current timestep of the calculation

            **non_eq_dict** (dict) - Dictionary that contains the directly to the upper grid connected flexibility units

            **calc_mode** (str) - Specifies the calculation mode ("market", "ideal_case" or "status_quo")

            **hv_opf_converged** (bool) - Boolean that states whether the OPF of the upstream grid converged

        OUTPUT:
            **res_q_market** (dict) - Dictionary which contains the results for a specific timestep
    """

    if calc_mode == "market_bottom_up":

        res_q_market = results_multi_level_market_bottom_up(net, time_step, non_eq_dict, opf_converged)

        return res_q_market

    elif calc_mode == "complete_grid":

        res_calculation = results_reference_cases(net, time_step, opf_converged)

        return res_calculation


def results_multi_level_market_bottom_up(net, time_step, non_eq_dict, opf_converged: bool = True):
    """
        Functions that returns a result dictionary for the market for a single time step.

        INPUT:
            **net** - The pandapower format network

            **time_step** (int) - Current timestep of the calculation

            **non_eq_dict** (dict) - Dictionary that contains directly to the upper grid connected flexibility units

            **hv_opf_converged** (bool) - Boolean that states whether the OPF of the upstream grid converged

        OUTPUT:
            **res_q_market** (dict) - Dictionary which contains the results for a specific timestep
        """

    if not opf_converged:
        key_list = ["timestep", "P_slack_EHV_HV", "Q_slack_EHV_HV", "Q_flexibility_sum",
                    "Q_abs_flexibility_sum", "Q_load_sum", "Q_flexibility_HV", "Q_flexibility_cost_HV"]

        res_q_market = {key: np.nan for key in key_list}

    else:
        res_q_market = \
            {"timestep": time_step,
             "P_slack_EHV_HV": net.res_ext_grid.p_mw.sum(),
             "Q_slack_EHV_HV": net.res_ext_grid["q_mvar"].sum(),

             "Q_flexibility_sum": net.res_sgen["q_mvar"].sum(),
             "Q_abs_flexibility_sum": net.res_sgen["q_mvar"].abs().sum(),
             "Q_load_sum": net.res_load["q_mvar"].sum(),

             "Q_flexibility_HV": net.res_sgen.loc[non_eq_dict["sgen"]].q_mvar.sum(),
             "Q_flexibility_cost_HV": costs_q_flex_units(net, True, volt_LvL=3, market=True)}

    return res_q_market


def results_reference_cases(net, time_step, opf_converged: bool = True):
    """
        Creation of a dictionary that contains the results for the ideal optimum or the status quo for one time step.

        INPUT:
            **net** - The pandapower format network

            **time_step** (int) - Current timestep of the calculation

            **hv_opf_converged** (bool) - Boolean that states whether the OPF of the upstream grid converged
        OUTPUT:
            **res_q_market** (dict) - Dictionary which contains the results for a specific timestep
    """

    if not opf_converged:
        key_list = ["timestep", "P_load_sum", "P_sgen_sum", "P_slack", "p_losses_total",
                    "p_losses_HV", "p_losses_MV", "Q_slack", "Q_load_sum", "q_losses_total", "q_losses_HV",
                    "q_losses_MV", "Q_flexibility_sum", "Q_abs_flexibility_sum", "Q_flexibility_HV",
                    "Q_abs_flexibility_HV", "Q_flexibility_MV", "Q_abs_flexibility_MV", "total_cost",
                    "Q_flexibility_cost", "loss_cost", "Q_flexibility_cost_HV", "Q_flexibility_cost_MV",
                    "min_vm_pu_HV", "max_vm_pu_HV", "min_vm_pu_MV", "max_vm_pu_MV", "average_vm_pu", "average_vm_pu_HV",
                    "average_vm_pu_MV", "max_line_loading_HV", "max_line_loading_MV", "average_line_loading",
                    "average_line_loading_HV", "average_line_loading_MV", "max_trafo_loading_EHVHV",
                    "max_trafo_loading_HVMV", "average_trafo_loading", "average_trafo_loading_EHVHV",
                    "average_trafo_loading_HVMV"]

        res_optimization = {key: np.nan for key in key_list}
        res_optimization["timestep"] = time_step

    else:
        p_loss, q_loss = loss_calculation(net)

        res_optimization = \
            {"timestep": time_step,
             "P_load_sum": net.res_load.p_mw.sum(),
             "P_sgen_sum": net.res_sgen.p_mw.sum(),
             "P_slack": net.res_ext_grid.p_mw.sum(),
             "p_losses_total": p_loss,
             "p_losses_HV": net.res_line.pl_mw.loc[net.line.voltLvl == 3].sum() + net.res_trafo.pl_mw.loc[net.trafo.voltLvl == 2].sum(),
             "p_losses_MV": net.res_line.pl_mw.loc[net.line.voltLvl == 5].sum() + net.res_trafo.pl_mw.loc[net.trafo.voltLvl == 4].sum(),

             "Q_slack": net.res_ext_grid["q_mvar"].sum(),
             "Q_load_sum": net.res_load["q_mvar"].sum(),
             "q_losses_total": q_loss,
             "q_losses_HV": net.res_line.ql_mvar.loc[net.line.voltLvl == 3].sum() + net.res_trafo.ql_mvar.loc[net.trafo.voltLvl == 2].sum(),
             "q_losses_MV": net.res_line.ql_mvar.loc[net.line.voltLvl == 5].sum() + net.res_trafo.ql_mvar.loc[net.trafo.voltLvl == 4].sum(),
             "Q_flexibility_sum": net.res_sgen["q_mvar"].sum(),
             "Q_abs_flexibility_sum": net.res_sgen["q_mvar"].abs().sum(),
             "Q_flexibility_HV": net.res_sgen.q_mvar.loc[net.sgen.voltLvl == 3].sum(),
             "Q_abs_flexibility_HV": net.res_sgen.q_mvar.loc[net.sgen.voltLvl == 3].abs().sum(),
             "Q_flexibility_MV": net.res_sgen.q_mvar.loc[net.sgen.voltLvl == 5].sum(),
             "Q_abs_flexibility_MV": net.res_sgen.q_mvar.loc[net.sgen.voltLvl == 5].abs().sum(),

             "total_cost": costs_q_flex_units(net) + p_loss * parameter.price_active_power,
             "Q_flexibility_cost": costs_q_flex_units(net),
             "loss_cost": p_loss * parameter.price_active_power,
             "Q_flexibility_cost_HV": costs_q_flex_units(net, True, volt_LvL=3),
             "Q_flexibility_cost_MV": costs_q_flex_units(net, True, volt_LvL=5),

             "min_vm_pu_HV": net.res_bus.vm_pu.loc[net.bus.voltLvl == 3].min(),
             "max_vm_pu_HV": net.res_bus.vm_pu.loc[net.bus.voltLvl == 3].max(),
             "min_vm_pu_MV": net.res_bus.vm_pu.loc[net.bus.voltLvl == 5].min(),
             "max_vm_pu_MV": net.res_bus.vm_pu.loc[net.bus.voltLvl == 5].max(),
             "average_vm_pu": net.res_bus.vm_pu.mean(),
             "average_vm_pu_HV": net.res_bus.vm_pu.loc[net.bus.voltLvl == 3].mean(),
             "average_vm_pu_MV": net.res_bus.vm_pu.loc[net.bus.voltLvl == 5].mean(),

             "max_line_loading_HV": net.res_line.loading_percent.loc[net.line.voltLvl == 3].max(),
             "max_line_loading_MV": net.res_line.loading_percent.loc[net.line.voltLvl == 5].max(),
             "average_line_loading": net.res_line.loading_percent.mean(),
             "average_line_loading_HV": net.res_line.loading_percent.loc[net.line.voltLvl == 3].mean(),
             "average_line_loading_MV": net.res_line.loading_percent.loc[net.line.voltLvl == 5].mean(),

             "max_trafo_loading_EHVHV": net.res_trafo.loading_percent.loc[net.trafo.voltLvl == 2].max(),
             "max_trafo_loading_HVMV": net.res_trafo.loading_percent.loc[net.trafo.voltLvl == 4].max(),
             "average_trafo_loading": net.res_trafo.loading_percent.mean(),
             "average_trafo_loading_EHVHV": net.res_trafo.loading_percent.loc[net.trafo.voltLvl == 2].mean(),
             "average_trafo_loading_HVMV": net.res_trafo.loading_percent.loc[net.trafo.voltLvl == 4].mean()}

    return res_optimization


def store_detailed_hv_data(net, res_dir, epf, time_step):
    """
        Writes a csv file with detailed information about the high
        voltage grid for the reoptimization.

        INPUT:
            **net** - The pandapower format network

            **res_dir** (str) - Result directory of the time series calculation

            **epf** (str) - String of the used EPF ("EPF1", "EPF2" or "EPF3")

            **time_step** (int) - Timestep of the time series calculation
    """
    saving_dir = os.path.join(res_dir, 'detailed_info_hv_grid')
    os.makedirs(saving_dir, exist_ok=True)

    information_df_hv = pd.DataFrame(columns=["name", "q_provided_mvar"])
    information_df_hv.name = net.sgen.name
    information_df_hv.q_provided_mvar = net.res_sgen.q_mvar
    information_df_hv.to_csv(os.path.join(saving_dir, epf + '_' + str(time_step) + '.csv'))


def append_to_csv(res_dir: str, file_name: str, calculation_results: dict):
    """ Appends a specific row to a csv file with the specified name at the
        specified direction. Method used is the DictWriter.

        INPUT:
            **res_dir** (string)- Directory of the csv-file

            **file_name** (string)- File name of the the csv-file

            **calculation_results** (dict)- Dictionary that contains the
            calculation results
    """

    with open(os.path.join(res_dir, file_name), "a") as csvfile:
        fieldnames = list(calculation_results.keys())
        writer = csv.DictWriter(csvfile, fieldnames, lineterminator='\n')
        writer.writerow(calculation_results)


def write_to_csv(res_dir, file_name, row):
    """ Writes a specific row to a csv file with the specified name at the
        specified direction.

        INPUT:
            **res_dir** (string)- Directory of the csv-file

            **file_name** (string)- File name of the csv-file

            **row** (list)- List that contains the values of a row that should
            be written to the csv-file
    """

    with open(os.path.join(res_dir, file_name), 'w') as csvfile:
        writer = csv.writer(csvfile, lineterminator='\n')
        writer.writerow(row)


def store_all_voltages(net, timestep, res_dir: str, file_name: str):
    """
        Function that stores all node voltages of the grid. The calculation
        results are appended to the existing CSV results file.

        INPUT:
            **net** - The pandapower format network

            **timestep** (int) - Current timestep of the calculation

            **res_dir** (string)- Directory where the results should be saved

            **file_name** (string)- File name of the csv-file
    """

    list_of_timestep = [timestep]*net.res_bus.shape[0]

    voltage_res_df = pd.DataFrame(list_of_timestep, columns=['timestep'])
    voltage_res_df["node"] = net.res_bus.index
    voltage_res_df["vlt_level"] = net.bus.voltLvl.tolist()
    voltage_res_df["voltage_vm_pu"] = net.res_bus.vm_pu.tolist()

    voltage_res_df.to_csv(os.path.join(res_dir, file_name), mode='a', header=False, index=False)


def store_all_loadings(net, timestep, res_dir: str, tap_information: str, res_files_loading):
    """
        Function that stores all loadings of the grid. The calculation
        results are appended to the existing CSV results file.
        Both line and trafo loadings are saved.

        INPUT:
            **net** - The pandapower format network

            **timestep** (int) - Current timestep of the calculation

            **res_dir** (string) - Directory of the result CSV file

            **tap_information** (string) - Information of the tap optimization
            that is used for the current calculation (nt - no tap optimization;
            dt - discrete tap optimization; ct - continuous tap optimization)

            **res_files_loading** (dict) - Dictionary that contains the
            directories of the results files
    """

    loading_df = {"_trafo": pd.DataFrame(columns=['timestep', 'index', 'vlt_level', 'loading_percent']),
                  "_lines": pd.DataFrame(columns=['timestep', 'index', 'vlt_level', 'loading_percent'])}

    for case, df in loading_df.items():
        if case == "_trafo":
            df["timestep"] = [timestep]*net.res_trafo.shape[0]
            df["index"] = net.res_trafo.index
            df["vlt_level"] = net.trafo.voltLvl.tolist()
            df["loading_percent"] = net.res_trafo.loading_percent.tolist()
        elif case == "_lines":
            df["timestep"] = [timestep]*net.res_line.shape[0]
            df["index"] = net.res_line.index
            df["vlt_level"] = net.line.voltLvl.tolist()
            df["loading_percent"] = net.res_line.loading_percent.tolist()

    for case, df in loading_df.items():
        df.to_csv(os.path.join(res_dir, res_files_loading[tap_information+case]), mode='a', header=False, index=False)


def store_all_maximum_loadings(net, timestep, res_dir: str, tap_information: str, res_files_loading):
    """
        Function that stores the maximum loadings of the grid. The calculation
        results are appended to the existing CSV results file.
        Both line and trafo loadings are saved.

        INPUT:
            **net** - The pandapower format network

            **timestep** (int) - Current timestep of the calculation

            **res_dir** (string) - Directory of the result CSV files

            **tap_information** (string) - Information of the tap optimization
            that is used for the current calculation

            **res_files_loading** (dict) - Dictionary that contains the
            directories of the results files
    """
    loading_df = {"_trafo_max": pd.DataFrame(columns=['timestep', 'grid_name', 'loading_percent', 'vlt_level']),
                  "_lines_max": pd.DataFrame(columns=['timestep', 'grid_name', 'loading_percent', 'vlt_level'])}

    for case, df in loading_df.items():
        grid_dictionary = {}
        if "lines" in case:
            grid_dictionary = dictionary_different_grids_in_complete_grid(net, "line_loading")
        elif "trafo" in case:
            grid_dictionary = dictionary_different_grids_in_complete_grid(net, "trafo_loading")

        grid_name = []
        loading_percent = []
        vlt_level = []

        for key, value in grid_dictionary.items():
            grid_name.append(key)

            if "lines" in case:
                loading_percent.append(net.res_line.loc[value].loading_percent.max())
                if "MV" in key:
                    vlt_level.append(5)
                else:
                    vlt_level.append(3)

            elif "trafo" in case:
                loading_percent.append(net.res_trafo.loc[value].loading_percent.max())
                if "MV" in key:
                    vlt_level.append(4)
                else:
                    vlt_level.append(2)

        df["grid_name"] = grid_name
        df["loading_percent"] = loading_percent
        df["vlt_level"] = vlt_level
        df["timestep"] = timestep

        df.to_csv(os.path.join(res_dir, res_files_loading[tap_information + case]), mode='a', header=False, index=False)


def store_all_max_min_voltages(net, timestep, res_dir: str, file_name: str):
    """
       Function that stores the maximum and minimum node voltages of the grid.
       The calculation results are appended to the existing CSV result files.

       INPUT:
           **net** - The pandapower format network

           **timestep** (int) - Current timestep of the calculation

           **res_dir** (string)- Directory of the result CSV file

           **file_name** (string)- File name of the result CSV file
       """
    voltage_res_df = pd.DataFrame(columns=['timestep', 'grid_name', 'min_vm_pu', 'max_vm_pu', 'vlt_level'])

    grid_dictionary = dictionary_different_grids_in_complete_grid(net, "voltage")

    grid_name = []
    min_vm_pu = []
    max_vm_pu = []
    vlt_level = []

    for key, value in grid_dictionary.items():
        grid_name.append(key)
        min_vm_pu.append(net.res_bus.loc[value].vm_pu.min())
        max_vm_pu.append(net.res_bus.loc[value].vm_pu.max())
        if "MV" in key:
            vlt_level.append(5)
        else:
            vlt_level.append(3)

    voltage_res_df["grid_name"] = grid_name
    voltage_res_df["min_vm_pu"] = min_vm_pu
    voltage_res_df["max_vm_pu"] = max_vm_pu
    voltage_res_df["vlt_level"] = vlt_level
    voltage_res_df["timestep"] = timestep

    voltage_res_df.to_csv(os.path.join(res_dir, file_name), mode='a', header=False, index=False)


def create_results_csv_files(folder_name, save_slack_values_mw=False):
    """
        Creation of the result CSV files and the result folders
        INPUT:
            **folder_name** - Name of the results folder

            **save_slack_values_mw** - Boolean that states whether the slack values
            of the MV grid values should be saved

        OUTPUT:
            **res_dir** (dict) - General result directory

            **res_files** (dict) - Dictionary that contains the result paths for the correct calculation method
            (No tap, discrete or continuous optimization). EXAMPLE: {NT: res path, DT: res path, CT: res path}

            **res_files_voltage** (dict) - Dictionary that contains the voltage (all node voltages, min and max values)
            result paths for the correct calculation method (No tap, discrete or continuous optimization)

            **res_files_loading** (dict) - Dictionary that contains the line and trafo loading (all and maximum
            loadings) result paths for the correct calculation method (No tap, discrete or continuous optimization)

            **slack_values_mw** (dict) - Dictionary that contains the result path of the HV/MV slack values.
    """

    res_dir = os.path.join(os.getcwd(), 'Results', datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + '_' + folder_name)

    os.makedirs(res_dir, exist_ok=True)

    calculation_type = ["nt", "dt", "ct"]
    for case in calculation_type:
        os.makedirs(os.path.join(res_dir, case), exist_ok=True)

    res_files = {"NT": os.path.join("nt", folder_name + "_nt.csv"),
                 "DT": os.path.join("dt", folder_name + "_dt.csv"),
                 "CT": os.path.join("ct", folder_name + "_ct.csv")}

    res_files_voltage = {"NT": os.path.join("nt", folder_name + "_nt_voltage.csv"),
                         "DT": os.path.join("dt", folder_name + "_dt_voltage.csv"),
                         "CT": os.path.join("ct", folder_name + "_ct_voltage.csv"),
                         "NT_min_max": os.path.join("nt", folder_name + "_nt_voltage_min_max.csv"),
                         "DT_min_max": os.path.join("dt", folder_name + "_dt_voltage_min_max.csv"),
                         "CT_min_max": os.path.join("ct", folder_name + "_ct_voltage_min_max.csv")}

    res_files_loading = {"NT_trafo": os.path.join("nt", folder_name + "_nt_loading_t.csv"),
                         "DT_trafo": os.path.join("dt", folder_name + "_dt_loading_t.csv"),
                         "CT_trafo": os.path.join("ct", folder_name + "_ct_loading_t.csv"),
                         "NT_trafo_max": os.path.join("nt", folder_name + "_nt_loading_t_max.csv"),
                         "DT_trafo_max": os.path.join("dt", folder_name + "_dt_loading_t_max.csv"),
                         "CT_trafo_max": os.path.join("ct", folder_name + "_ct_loading_t_max.csv"),

                         "NT_lines": os.path.join("nt", folder_name + "_nt_loading_l.csv"),
                         "DT_lines": os.path.join("dt", folder_name + "_dt_loading_l.csv"),
                         "CT_lines": os.path.join("ct", folder_name + "_ct_loading_l.csv"),
                         "NT_lines_max": os.path.join("nt", folder_name + "_nt_loading_l_max.csv"),
                         "DT_lines_max": os.path.join("dt", folder_name + "_dt_loading_l_max.csv"),
                         "CT_lines_max": os.path.join("ct", folder_name + "_ct_loading_l_max.csv")}

    # Write CSV file with corresponding header
    header_main_file = results_dict(0, 0, 0, calc_mode="complete_grid", opf_converged=False).keys()
    for file_name in res_files.values():
        write_to_csv(res_dir, file_name, header_main_file)

    for file_name in res_files_voltage.values():
        if "max" in file_name:
            write_to_csv(res_dir, file_name, ['timestep', 'grid_name', 'min_vm_pu', 'max_vm_pu', 'vlt_level'])
        else:
            write_to_csv(res_dir, file_name, ["timestep", "node", "vlt_level", "voltage_vm_pu"])

    for file_name in res_files_loading.values():
        if "max" in file_name:
            write_to_csv(res_dir, file_name, ['timestep', 'grid_name', 'loading_percent', 'vlt_level'])
        else:
            write_to_csv(res_dir, file_name, ["timestep", "index", "vlt_level", "loading_percent"])

    slack_values_mv = {}
    if save_slack_values_mw:
        slack_values_mv["Q"] = folder_name + "_nt_q_mw.csv"
        slack_values_mv["P"] = folder_name + "_nt_p_mw.csv"
        slack_values_mv["cos_phi"] = folder_name + "_nt_cos_phi_mw.csv"

        for file_name in slack_values_mv.values():
            write_to_csv(res_dir, file_name, ["timestep", "MV1", "MV2", "MV3", "MV4"])

    return res_dir, res_files, res_files_voltage, res_files_loading, slack_values_mv


def store_all_results(net, res_dir, time_step, res_files, res_files_voltage, res_files_loading, tap_mode, opf_converged=True):
    """
        Stores all relevant results of the time series calculation.

        INPUT:
           **net** - The pandapower format network

           **res_dir** (string)- Directory where the results should be saved

           **time_step** (int) - Current timestep of the calculation

           **res_files** (string) - Dictionary that holds the directories of the
           general result CSV files

           **res_files_voltage** - Dictionary that holds the directories of the
           voltage result CSV files

           **res_files_loading** - Dictionary that holds the directories of the
           loading result CSV files

           **tap_mode** (string) - Information of the tap optimization
            that is used for the current calculation (nt - no tap optimization;
            dt - discrete tap optimization; ct - continuous tap optimization)

           **opf_converged** (boolean) - Boolean that states whether the OPF converged
    """

    if opf_converged is True:
        res_dictionary = results_dict(net, time_step, 0, calc_mode="complete_grid")
        append_to_csv(res_dir, res_files[tap_mode], res_dictionary)

        store_all_voltages(net, time_step, res_dir, res_files_voltage[tap_mode])
        store_all_loadings(net, time_step, res_dir, tap_mode, res_files_loading)
        store_all_max_min_voltages(net, time_step, res_dir, res_files_voltage[tap_mode+"_min_max"])
        store_all_maximum_loadings(net, time_step, res_dir, tap_mode, res_files_loading)

    else:
        res_dictionary = results_dict(net, time_step, 0, "complete_grid", opf_converged=False)
        append_to_csv(res_dir, res_files["NT"], res_dictionary)
        append_to_csv(res_dir, res_files["DT"], res_dictionary)
        append_to_csv(res_dir, res_files["CT"], res_dictionary)
