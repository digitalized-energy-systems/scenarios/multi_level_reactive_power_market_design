import pandas as pd
import simbench as sb
import pandapower as pp
import time

import parameter

from functions import pp_aux
from evaluation_functions import top_down_specific, general_evaluation, store_results as store
from functions import grid_constraints as constraints
from functions import cost_fct

import logging
logger = logging.getLogger(__name__)


def set_up_complete_grid(complete_net):
    """
        Sets constraints for voltages, lines, trafos. Further the rated apparent
        power and the EPFs for the flexibility units are set. SimBench profiles
        are created saved.

        INPUT:
            **complete_net** - The pandapower format network of the complete
            grid (HV+MV)

        OUTPUT:
             **initial_tap_settings** - (list) Initial tap settings of all
             transformers in the complete grid

             **profiles** (dict) - Dictionary that contains the profiles for
             the elements in the complete grid

             **idx_flexibility_units** - Indexes of the flexibility units in
             the complete grid
    """

    initial_tap_settings = []
    for tap_pos in complete_net.trafo.tap_pos:
        initial_tap_settings.append(tap_pos)

    # need to set the wrongly included HV_MV_eq elements to zero
    index_dict = general_evaluation.indexes_relevant_units(complete_net)
    complete_net.sgen.loc[index_dict[('sgen', 3, '_eq')], "in_service"] = False
    complete_net.load.loc[index_dict[('load', 3, '_eq')], "in_service"] = False

    constraints.set_voltage_constraints(complete_net)
    constraints.set_line_and_trafo_constraints(complete_net)
    constraints.set_ext_grid_constraints(complete_net, slack_voltage_vm_pu=1.0)

    assert not sb.profiles_are_missing(complete_net)
    profiles = sb.get_absolute_values(complete_net, profiles_instead_of_study_cases=True)
    profiles[('sgen', 'q_mvar')] = pd.DataFrame(0, columns=profiles[('sgen', 'p_mw')].columns,
                                                index=profiles[('sgen', 'p_mw')].index)

    pp_aux.rated_app_power(complete_net, profiles)
    pp_aux.units_in_wind_park(complete_net, 3)
    cost_fct.set_costs_flexi_units(complete_net, index_dict)
    complete_net.sgen.loc[complete_net.poly_cost.element, "controllable"] = True

    idx_flexibility_units = complete_net.sgen[complete_net.sgen.controllable == True].index

    return initial_tap_settings, profiles, idx_flexibility_units


def set_up_hv_and_mv_nets(hv_net, mv_nets):
    """
        Sets the cost for the MV grid operator for reactive power provision of
        MV units to zero. Sets constraints for the HV grid (Line, nodes,
        Slack voltage, Q_PCC_EHV/,..). SimBench profiles for the HV grid
        are created.

        INPUT:
            **hv_net** - The pandapower format network of the complete
            HV grid

            **mv_nets** (dict) - Dictionary that holds the pandapower format
            networks of the MV grids

        OUTPUT:
             **profiles_hv** (dict) - Dictionary that contains the profiles for
             the elements in the HV grid
    """

    for mv_grid in mv_nets.values():
        mv_grid.poly_cost.cq2_eur_per_mvar2 = 0.0
        mv_grid.poly_cost.loc[0, "cq1_eur_per_mvar"] = 0.0

    constraints.set_upstream_net_constraints(hv_net)
    hv_net.ext_grid.loc[0, "min_q_mvar"] = 0
    hv_net.ext_grid.loc[0, "max_q_mvar"] = 0
    hv_net.ext_grid.loc[0, "vm_pu"] = 1

    assert not sb.profiles_are_missing(hv_net)
    profiles_hv = sb.get_absolute_values(hv_net, profiles_instead_of_study_cases=True)

    pp_aux.rated_app_power(hv_net, profiles_hv)
    pp_aux.units_in_wind_park(hv_net, 3)

    return profiles_hv


def ts_calculation(complete_net, profiles_complete_grid, idx_controllables_complete_grid, initial_tap_settings,
                   hv_net, profiles_hv, mv_nets, downstream_profiles, slack_values_mv_grids,
                   mv_grid_names, idx_non_eq, idx_eq_sorted, downstream_flexibility_units, res_dir, res_files,
                   res_files_voltage, res_files_loading, time_steps):
    """
        Executes the timeseries calculation for the Reference Case No market. Firstly, each (MV) grid operator minimizes
        its active power loss costs without taking into account the reactive power costs/EPFs of flexibility providers,
        while complying with the restricted reactive power at the coupling point (cos_Q_PCC within +/- 0.95).
        The resulting reactive power set points are then applied in the complete grid model (HV+MV) and power
        flow calculations with and without tap optimizations are performed.

        INPUT:

            **complete_net** - The pandapower format network of the complete grid

            **profiles_complete_grid** (dict) - Dictionary that contains the profiles of the complete grid

            **idx_controllables_complete_grid** - Indexes of the flexibility units in the complete grid

            **initial_tap_settings** (list) - List with the initial tap settings of the transformers in the complete grid

            **hv_net** - The pandapower format network of the HV grid

            **profiles_hv** (dict) - Dictionary that contains the profiles of the HV grid

            **mv_nets** (dict) - Dictionary that holds the pandapower format networks of the MV grids

            **downstream_profiles** (dict) - Dictionary that contains the dictionaries of the MV grid profiles

            **slack_values_mv_grids** (dict)- Dictionary that contains the result path of the HV/MV slack values.

            **mv_grid_names** (tuple) - Tuple that contains the MV grid names

            **idx_non_eq** (tuple) - Contains all indexes of non-equivalent elements in a simbench grid.
            Example: (('sgen', idx1, bus_idx1, 'subnet_name'), ('sgen', idx2, bus_idx2, 'subnet_name'),...))
            idx_non_eq_elements

            **idx_eq_sorted** (dict) - Contains all indexes of equivalent elements in a simbench grid.
            Example: {'MV1': (('sgen', idx, bus_idx, 'subnet_name')), 'MV2': (('sgen', idx, bus_idx, 'subnet_name'))...,}


            **downstream_flexibility_units** (dict) - Dictionary that contains dictionaries with a list of booleans
            for the flexibility unit_types units of a grid. Boolean is True if the unit is a flexibility
            unit {"MV1": {"sgen": [True, False..]}, "MV2":...}

            **res_dir** (dict)- General result directory

            **res_files** (dict)- Dictionary that contains the result paths for the correct calculation method
            (No tap, discrete or continuous optimization). EXAMPLE: {NT: res path, DT: res path, CT: res path}

            **res_files_voltage** (dict)- Dictionary that contains the voltage (all node voltages, min and max values) result
            paths for the correct calculation method (No tap, discrete or continuous optimization)

            **res_files_loading** (dict)- Dictionary that contains the line and trafo loading (all and maximum
            loadings) result paths for the correct calculation method (No tap, discrete or continuous optimization)

            **time_steps** (list)- List that contains all time steps of the time series calculation
    """

    start_time_ts_calc = time.time()
    for it_counter, time_step in enumerate(time_steps):
        pp.create_poly_cost(hv_net, 0, 'ext_grid', cp1_eur_per_mw=parameter.price_active_power, cq1_eur_per_mvar=0)

        start_time_one_t_step = time.time()

        pp_aux.apply_absolute_values(hv_net, profiles_hv, time_step)

        detailed_q_info_units, slack_values_mv, mv_opfs_converged = determine_mv_grid_set_points(mv_nets,
                                                                                                 downstream_profiles,
                                                                                                 downstream_flexibility_units,
                                                                                                 time_step)

        q_slack_mv_res_dict = {"timestep": time_step}
        p_slack_mv_res_dict = {"timestep": time_step}
        cos_phi_slack_mv_res_dict = {"timestep": time_step}

        for key in slack_values_mv.keys():

            p_slack = slack_values_mv[key].loc[0, "p_mw"]
            q_slack = slack_values_mv[key].loc[0, "q_mvar"]

            q_slack_mv_res_dict[key] = q_slack
            p_slack_mv_res_dict[key] = p_slack
            cos_phi_slack_mv_res_dict[key] = p_slack/(p_slack**2 + q_slack**2)**0.5

        store.append_to_csv(res_dir, slack_values_mv_grids["Q"], q_slack_mv_res_dict)
        store.append_to_csv(res_dir, slack_values_mv_grids["P"], p_slack_mv_res_dict)
        store.append_to_csv(res_dir, slack_values_mv_grids["cos_phi"], cos_phi_slack_mv_res_dict)

        if all(value == True for value in mv_opfs_converged.values()):
            set_up_hv_non_eq_units_with_tech_max_q(hv_net, idx_non_eq)
            for grid_name in mv_grid_names:
                set_up_hv_eq_units(hv_net, slack_values_mv[grid_name], idx_eq_sorted[grid_name])

            try:
                pp.runopp(hv_net, delta=1e-16)
            except pp.optimal_powerflow.OPFNotConverged:
                logger.warning("Optimal Power Flow did not converge in timestep %d " % time_step)

                store.store_all_results(complete_net, res_dir, time_step, res_files, res_files_voltage,
                                        res_files_loading, tap_mode="all", opf_converged=False)

                hv_net.poly_cost.drop(hv_net.poly_cost.index, inplace=True)

            else:
                detailed_q_info_units["HV_grid"] = detailed_hv_q_data(hv_net)

                # Setting up the complete grid with the correct profiles and reactive power values
                pp_aux.apply_absolute_values(complete_net, profiles_complete_grid, time_step)

                for idx in idx_controllables_complete_grid:
                    mapping_complete_grid_to_single_grids(complete_net, idx, detailed_q_info_units)

                complete_net.trafo.tap_pos = initial_tap_settings
                pp.runpp(complete_net)
                store.store_all_results(complete_net, res_dir, time_step, res_files,
                                        res_files_voltage, res_files_loading, tap_mode="NT")

                general_evaluation.power_flow_with_tap_optimization(complete_net, "DT")
                store.store_all_results(complete_net, res_dir, time_step, res_files,
                                        res_files_voltage, res_files_loading, tap_mode="DT")

                general_evaluation.power_flow_with_tap_optimization(complete_net, "CT")
                store.store_all_results(complete_net, res_dir, time_step, res_files,
                                        res_files_voltage, res_files_loading, tap_mode="CT")

                hv_net.poly_cost.drop(hv_net.poly_cost.index, inplace=True)

        else:
            store.store_all_results(complete_net, res_dir, time_step, res_files, res_files_voltage,
                                    res_files_loading, tap_mode="all", opf_converged=False)

            hv_net.poly_cost.drop(hv_net.poly_cost.index, inplace=True)
            print(cos_phi_slack_mv_res_dict)

        print("Time step %s :--- %s seconds ---"
              % (it_counter, time.time() - start_time_one_t_step))

    print("Complete TS: --- %s seconds ---" % (time.time() - start_time_ts_calc))


def slack_info_mv_nets(net):
    """
        Function that returns a dataframe of the results values of the external grid.

        INPUT:
            **net** - The pandapower format network

        OUTPUT:
            **detailed_info_ds_df** - Dataframe with the active and reactive power values of the grid coupling point
    """

    detailed_info_ds_df = pd.DataFrame(columns=["p_mw", "q_mvar"])
    detailed_info_ds_df.p_mw = net.res_ext_grid["p_mw"]
    detailed_info_ds_df.q_mvar = net.res_ext_grid["q_mvar"]

    return detailed_info_ds_df


def determine_mv_grid_set_points(downstream_nets, downstream_profiles, flexible_units, time_step):

    """
        Determination of the reactive power setpoints for MV flexibility units. First the reactive power boundary
        restriction at the coupling point between the HV grid and each MV grid is determined. This is done with a power
        flow calculation without the consideration of a flexible reactive power support from DERs.
        Afterwards a loss minimization optimal power flow with the correct restrictions (flexibility units
        and grid constraints) is performed.

        INPUT:
            **downstream_nets** (dict) - Dictionary that contains the name of
            the network and the pandapower format networks of the downstream
            grids the name of the network: {'MV1': net1, 'MV2': net2, ..}

            **downstream_profiles** (dict) - Dictionary that contains the
            profiles of the downstream grids
            {'MV1': profiles1, 'MV2': profiles2, ..}

            **flexible_units** (dict) - Dictionary with the unit types and list
            of booleans. True if unit is a flexibility unit, False otherwise

            **time_step** (int) - Specific timestep (index of the specific
            timestep in the profiles dataframe) that should be calculated

        OUTPUT:
            **detailed_mv_q_info** (dict) - Dictionary that contains the reactive power set point for each
            flexibility unit for each MV grid. Calculated in the corresponding local OPF.

            **slack_values_mv** (dict) - Dictionary that contains the active and reactive power value at the
            coupling point between the HV and the MV grids.

            **mv_opfs_converged** (dict) - Dictionary that contains booleans whether the OPF for a specific MV grids
            converged.

    """

    detailed_mv_q_info = {}
    slack_values_mv = {}
    mv_opfs_converged = {}
    for net_name, downstream_net in downstream_nets.items():

        pp_aux.apply_absolute_values(downstream_net, downstream_profiles[net_name], time_step)

        pp.runpp(downstream_net)
        Q_min_ext = -abs(downstream_net.res_ext_grid.loc[0, "p_mw"]) * ((1/0.95**2)-1) ** 0.5
        Q_max_ext = abs(downstream_net.res_ext_grid.loc[0, "p_mw"]) * ((1/0.95**2)-1) ** 0.5

        downstream_net.ext_grid.loc[0, "min_q_mvar"] = Q_min_ext
        downstream_net.ext_grid.loc[0, "max_q_mvar"] = Q_max_ext

        downstream_net.sgen["min_p_mw"] = downstream_net.sgen.loc[flexible_units[net_name]["sgen"], "p_mw"]
        downstream_net.sgen["max_p_mw"] = downstream_net.sgen.loc[flexible_units[net_name]["sgen"], "p_mw"]

        for index in downstream_net.sgen[flexible_units[net_name]["sgen"]].index.to_list():
            q_flexibility = (downstream_net.sgen.loc[index, "sn_mva"] ** 2 - downstream_net.sgen.loc[index, "p_mw"] ** 2) ** 0.5
            downstream_net.sgen.loc[index, "min_q_mvar"] = -q_flexibility
            downstream_net.sgen.loc[index, "max_q_mvar"] = q_flexibility

        try:
            pp.runopp(downstream_net)
        except pp.optimal_powerflow.OPFNotConverged:
            mv_opfs_converged[net_name] = False
        else:
            mv_opfs_converged[net_name] = True
            detailed_mv_q_info[net_name] = top_down_specific.detailed_information_of_mv_units(downstream_net)
            slack_values_mv[net_name] = slack_info_mv_nets(downstream_net)

    return detailed_mv_q_info, slack_values_mv, mv_opfs_converged


def set_up_hv_non_eq_units_with_tech_max_q(net, idx_non_eq: tuple):
    """ Sets constraints and costs for all non equivalent elements of a
        simbench net. Reactive power flexibility is based on the mandatory region.

        INPUT:
            **net** - The pandapower format network

            **idx_non_eq** (tuple) - Tuple that contains tuples of the unit
            type, the indexes, the indexes of the connected bus and the subnet
            name of the non equivalent elements
    """
    for unit_type, idx, bus, name in idx_non_eq:

        if unit_type in parameter.flexibility_units:
            if net[unit_type].loc[idx, "sn_mva"] == 0:
                net[unit_type].loc[idx, "controllable"] = False
            else:
                q_flexibility = (net[unit_type].loc[idx, "sn_mva"] ** 2 - net[unit_type].loc[idx, "p_mw"] ** 2) ** 0.5
                q_flexibility_min = -q_flexibility
                q_flexibility_max = q_flexibility

                net[unit_type].loc[idx, "min_p_mw"] = net[unit_type].loc[idx, "p_mw"]
                net[unit_type].loc[idx, "max_p_mw"] = net[unit_type].loc[idx, "p_mw"]
                net[unit_type].loc[idx, "min_q_mvar"] = q_flexibility_min
                net[unit_type].loc[idx, "max_q_mvar"] = q_flexibility_max
                net[unit_type].loc[idx, "controllable"] = True

                pp.create_poly_cost(net, idx, unit_type, cp1_eur_per_mw=0,
                                    cq0_eur=0, cq1_eur_per_mvar=0,
                                    cq2_eur_per_mvar2=0)
        else:
            net[unit_type].loc[idx, "controllable"] = False


def set_up_hv_eq_units(net, slack_info_df, idx_eq: tuple):
    """ Sets constraints and costs for all non equivalent elements of a
            simbench net. Reactive power flexibility is based on the mandatory region.

            INPUT:
                **net** - The pandapower format network

                **slack_info_df** (dataframe) - Dataframe that contains the active and reactive power value
                of the external grid (node HV to MV)

                **idx_eq** (tuple) - Tuple that contains tuples of the unit
                type, the indexes, the indexes of the connected bus and the subnet
                name of the equivalent elements
        """
    for unit_type, idx, bus, name in idx_eq:
        if unit_type == "sgen":
            net[unit_type].loc[idx, "min_q_mvar"] = -slack_info_df.loc[0, "q_mvar"]
            net[unit_type].loc[idx, "max_q_mvar"] = -slack_info_df.loc[0, "q_mvar"]
            net[unit_type].loc[idx, "controllable"] = True

            if slack_info_df.loc[0, "p_mw"] <= 0:
                net[unit_type].loc[idx, "min_p_mw"] = -slack_info_df.loc[0, "p_mw"]
                net[unit_type].loc[idx, "max_p_mw"] = -slack_info_df.loc[0, "p_mw"]

            else:
                net[unit_type].loc[idx, "min_p_mw"] = 0
                net[unit_type].loc[idx, "max_p_mw"] = 0
                net[unit_type].loc[idx, "p_mw"] = 0

        elif unit_type == "load":

            net[unit_type].loc[idx, "controllable"] = False
            net[unit_type].loc[idx, "q_mvar"] = 0
            if slack_info_df.loc[0, "p_mw"] > 0:
                net[unit_type].loc[idx, "p_mw"] = slack_info_df.loc[0, "p_mw"]
            else:
                net[unit_type].loc[idx, "p_mw"] = 0


def detailed_hv_q_data(net):
    """
        Dataframe with detailed information about the reactive power output
        of the high voltage grid units.

        INPUT:
            **net** - The pandapower format network
    """
    information_df_hv = pd.DataFrame(columns=["name", "q_provided_mvar"])
    information_df_hv.name = net.sgen.name
    information_df_hv.q_provided_mvar = net.res_sgen.q_mvar
    information_df_hv.set_index('name', inplace=True)

    return information_df_hv


def mapping_complete_grid_to_single_grids(net, index, reoptimized_q_of_hv_mv_unit):
    """
        Sets the reactive power value of a flexibility unit in the complete grid (HV+MV) to the previously
        in the optimizations of the single HV net and single MV nets determined set point .

        INPUT:
         **net** - The pandapower format network

         **index** - Index of the unit in the complete grid

         **reoptimized_q_of_hv_mv_unit** - Dictionary that holds 14 dataframes with the detailed reactive power
         set point for each units of the HV grid and the MV grids.

         **complete_grid_to_mv_grid** - Dictionary that connects the name of the units in the complete grid with
         corresponding unit of the correct single grid.

    """
    mapping_dict = {"MV4.201": "MV4",
                    "MV4.202": "MV4",
                    "MV4.203": "MV4",
                    "MV1.201": "MV1",
                    "MV1.202": "MV1",
                    "MV1.203": "MV1",
                    "MV1.204": "MV1",
                    "MV1.205": "MV1",
                    "MV2.201": "MV2",
                    "MV2.202": "MV2",
                    "MV2.203": "MV2",
                    "MV3.201": "MV3",
                    "MV3.202": "MV3",
                    "HV2": "HV_grid"}

    unit_name_complete_grid = net.sgen.loc[index, "name"]
    partitioned_name = unit_name_complete_grid.partition(" ")
    specific_grid_name = partitioned_name[0]
    rest_of_name = partitioned_name[2]
    grid_name = specific_grid_name.partition(".")[0]

    if "MV" in grid_name:
        unit_name = grid_name + ".101 " + rest_of_name
    else:
        unit_name = unit_name_complete_grid

    corresponding_df = reoptimized_q_of_hv_mv_unit[mapping_dict[specific_grid_name]]
    net.sgen.loc[index, "q_mvar"] = corresponding_df.loc[unit_name, "q_provided_mvar"]