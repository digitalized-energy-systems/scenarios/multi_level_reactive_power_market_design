import pandas as pd
import simbench as sb
import pandapower as pp
import time

import parameter

from functions import pp_aux
from evaluation_functions import top_down_specific, general_evaluation, store_results as store
from functions import grid_constraints as constraints
from functions import cost_fct
from functions.grid_constraints import set_voltage_constraints
from functions.grid_constraints import set_line_and_trafo_constraints
from functions.grid_constraints import set_ext_grid_constraints
from functions.pp_aux import rated_app_power
from functions.pp_aux import apply_absolute_values

import logging
logger = logging.getLogger(__name__)


def set_up_complete_grid(complete_net):
    """
        Sets constraints for voltages, lines, trafos. Further the rated apparent
        power and the EPFs for the flexibility units are set. SimBench profiles
        are created saved.

        INPUT:
            **complete_net** - The pandapower format network of the complete
            grid (HV+MV)

        OUTPUT:
             **initial_tap_settings** - (list) Initial tap settings of all
             transformers in the complete grid

             **profiles** (dict) - Dictionary that contains the profiles for
             the elements in the complete grid

             **index_dict** (dict) - Dictionary with the indexes of the element types (_eq, non_eq - elements)
             separated by the calculated voltage levels
    """

    initial_tap_settings = []
    for tap_pos in complete_net.trafo.tap_pos:
        initial_tap_settings.append(tap_pos)

    index_dict = general_evaluation.indexes_relevant_units(complete_net)

    # need to set the wrongly included HV_MV_eg elements to zero
    complete_net.sgen.loc[index_dict[('sgen', 3, '_eq')], "in_service"] = False
    complete_net.load.loc[index_dict[('load', 3, '_eq')], "in_service"] = False

    assert not sb.profiles_are_missing(complete_net)
    profiles = sb.get_absolute_values(complete_net, profiles_instead_of_study_cases=True)
    profiles[('sgen', 'q_mvar')] = pd.DataFrame(0, columns=profiles[('sgen', 'p_mw')].columns,
                                                index=profiles[('sgen', 'p_mw')].index)

    rated_app_power(complete_net, profiles)

    set_voltage_constraints(complete_net)
    set_line_and_trafo_constraints(complete_net)

    set_ext_grid_constraints(complete_net, slack_voltage_vm_pu=1.0)
    complete_net.ext_grid.loc[0, "min_q_mvar"] = 0
    complete_net.ext_grid.loc[0, "max_q_mvar"] = 0
    pp.create_poly_cost(complete_net, 0, 'ext_grid', cp1_eur_per_mw=parameter.price_active_power, cq1_eur_per_mvar=0)

    pp_aux.units_in_wind_park(complete_net, 3)
    cost_fct.set_costs_flexi_units(complete_net, index_dict)

    return initial_tap_settings, profiles, index_dict


def ts_calculation(net, profiles, index_dict, initial_tap_settings, res_dir, res_files, res_files_voltage, res_files_loading, time_steps):
    """
        Executes the timeseries calculation for the Reference Case Optimal market. In this Reference case an optimal
        power flow for the complete grid (HV+MVs) is performed for each time step.

        INPUT:

            **net** - The pandapower format network of the complete grid

            **profiles** (dict) - Dictionary that contains the profiles of the complete grid

            **index_dict** (dict) - Dictionary with the indexes of the element types (_eq, non_eq - elements)
             separated by the calculated voltage levels

            **initial_tap_settings** (list) - List with the initial tap settings of the transformers in the complete grid

            **res_dir** (dict)- General result directory

            **res_files** (dict)- Dictionary that contains the result paths for the correct calculation method
            (No tap, discrete or continuous optimization). EXAMPLE: {NT: res path, DT: res path, CT: res path}

            **res_files_voltage** (dict)- Dictionary that contains the voltage (all node voltages, min and max values) result
            paths for the correct calculation method (No tap, discrete or continuous optimization)

            **res_files_loading** (dict)- Dictionary that contains the line and trafo loading (all and maximum
            loadings) result paths for the correct calculation method (No tap, discrete or continuous optimization)

            **time_steps** (list)- List that contains all time steps of the time series calculation
    """
    start_time_ts_calc = time.time()
    for it_counter, time_step in enumerate(time_steps):
        apply_absolute_values(net, profiles, time_step)

        constraints.set_constrains_flexibility_units(net, index_dict)

        start_time_opf = time.time()

        net.trafo.tap_pos = initial_tap_settings

        pp.runopp(net, delta=1e-16)
        store.store_all_results(net, res_dir, time_step, res_files,
                                res_files_voltage, res_files_loading, tap_mode="NT")

        # Tap Optimization #
        net.sgen.q_mvar = net.res_sgen.q_mvar

        general_evaluation.power_flow_with_tap_optimization(net, "DT")
        store.store_all_results(net, res_dir, time_step, res_files,
                                res_files_voltage, res_files_loading, tap_mode="DT")

        general_evaluation.power_flow_with_tap_optimization(net, "CT")
        store.store_all_results(net, res_dir, time_step, res_files,
                                res_files_voltage, res_files_loading, tap_mode="CT")

        print("OPF %s: --- %s seconds ---" % (it_counter, time.time() - start_time_opf))
    print("Complete TS: --- %s seconds ---" % (time.time() - start_time_ts_calc))

