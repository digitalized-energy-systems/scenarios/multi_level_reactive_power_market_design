import numpy as np
import simbench as sb
import pandas as pd
import os
import pandapower as pp

import time
from datetime import datetime

from functions import pp_aux
from functions import cost_fct
import parameter
from evaluation_functions import general_evaluation, store_results as store
from functions import grid_constraints as constraints
from evaluation_functions.general_evaluation import power_flow_with_tap_optimization

import logging
logger = logging.getLogger(__name__)


def store_reoptimization_mv(opf_converged, current_net, timestep,
                            current_MV_Q_provided: float, Q_base_case: float,
                            base_case_costs: float, epf_value: float, epfs: str,
                            current_grid_name: str, res_dir: str, res_file: str):
    """
        Saving of the re-optimization results. A row with the results is
        appended to the result CSV file.
        INPUT:
            **opf_converged** (bool) - Boolean that states whether the OPF for
            the current time step and grid converged

            **current_net** - The pandapower format network

            **timestep** (int)- Current timestep of the time series calculation

            **current_MV_Q_provided** (float)- Reactive power of the grid that
            is provided to the upstream grid operator

            **Q_base_case** (float)- Base case reactive power at the coupling point

            **base_case_costs** (float)- Grid operator base case costs

            **epf_value** (float)- Corresponding value of the EPF (Remuneration) for
            the provided reactive power

            **epfs** (str)- EPF calculation method that is used in the calculation

            **current_grid_name** (str)- Name of the grid (i.e. MV_idx_66 = Medium
            voltage grid with index 66 in the upstream SGEN dataframe)

            **res_dir** (str)- Results directory

            **res_file** (str)- Name of the result CSV-file
    """

    if opf_converged is True:
        p_loss, q_loss = pp_aux.loss_calculation(current_net)
        res_recalculation = {"timestep": timestep,
                             "P_Slack": current_net.res_ext_grid.loc[0, "p_mw"],
                             "Q_Slack": current_net.res_ext_grid.loc[0, "q_mvar"],
                             "Q_provided": current_MV_Q_provided,
                             "Q_base_case": Q_base_case,
                             "base_case_costs": base_case_costs,
                             "epf_value": epf_value,
                             "min_vm_pu_MV": current_net.res_bus.vm_pu.min(),
                             "max_vm_pu_MV": current_net.res_bus.vm_pu.max(),
                             "max_line_loading_MV": current_net.res_line.loading_percent.max(),
                             "max_trafo_loading_HVMV": current_net.res_trafo.loading_percent.max(),
                             "EPF": epfs,
                             "Grid_name": current_grid_name,
                             "Q_losses_MV": q_loss,
                             "P_losses_MV": p_loss,
                             "Costs_Q_DEAs": cost_fct.costs_q_flex_units(current_net),
                             "C_OPF": current_net.res_cost,
                             "loss_cost_MV": p_loss * parameter.price_active_power,
                             "Q_DEAs_MV": current_net.res_sgen.q_mvar.sum(),
                             "Q_ext_grid_net": current_net.res_ext_grid.loc[0, "q_mvar"]}

        store.append_to_csv(res_dir, res_file, res_recalculation)
    else:
        err_recalculation = {"timestep": timestep,
                             "P_Slack": current_net.res_ext_grid.loc[0, "p_mw"],
                             "Q_Slack": current_net.res_ext_grid.loc[0, "q_mvar"],
                             "Q_provided": current_MV_Q_provided,
                             "Q_base_case": Q_base_case,
                             "base_case_costs": base_case_costs,
                             "epf_value": epf_value,
                             "min_vm_pu_MV": np.nan,
                             "max_vm_pu_MV": np.nan,
                             "max_line_loading_MV": np.nan,
                             "max_trafo_loading_HVMV": np.nan,
                             "EPF": epfs,
                             "Grid_name": current_grid_name,
                             "Q_losses_MV": np.nan,
                             "P_losses_MV": np.nan,
                             "Costs_Q_DEAs": np.nan,
                             "C_OPF": np.nan,
                             "loss_cost_MV": np.nan,
                             "Q_DEAs_MV": np.nan,
                             "Q_ext_grid_net": np.nan}

        store.append_to_csv(res_dir, res_file, err_recalculation)


def set_up_mv_grid_for_reoptimization(current_net, current_MV_Q_provided):
    """
        Sets the (MV) grid up for the re-optimization --e.g. previously (bottom up process)
        defined reactive power limits at the coupling and local constraints--

        INPUT:
            **current_net** - The pandapower format network

            **current_MV_Q_provided** (float)- Reactive power of the grid that
            is provided to the upstream grid operator
    """

    current_net.ext_grid.loc[0, "min_q_mvar"] = -current_MV_Q_provided
    current_net.ext_grid.loc[0, "max_q_mvar"] = -current_MV_Q_provided

    flex_units = current_net.sgen.controllable == True

    q_flex = (current_net.sgen.loc[flex_units, "sn_mva"] ** 2
              - current_net.sgen.loc[flex_units, "p_mw"] ** 2) ** 0.5

    current_net.sgen["min_p_mw"] = current_net.sgen.loc[flex_units, "p_mw"]
    current_net.sgen["max_p_mw"] = current_net.sgen.loc[flex_units, "p_mw"]
    current_net.sgen["min_q_mvar"] = -q_flex
    current_net.sgen["max_q_mvar"] = q_flex


def initial_set_up_complete_grid(net):
    """
        Sets initial values for the multi level grid --e.g. define flexibility
        units in this grid for correct determination of costs etc.--.

        INPUT:
            **net** - The pandapower format network

            **current_MV_Q_provided** (float)- Reactive power of the grid that
            is provided to the upstream grid operator

        OUTPUT:
            **profiles** - Returns the profiles of the complete grid (HV+MVs)
    """

    index_dict = general_evaluation.indexes_relevant_units(net)

    # need to set the wrongly included HV_MV_eg elements to zero
    net.sgen.loc[index_dict[('sgen', 3, '_eq')], "in_service"] = False
    net.load.loc[index_dict[('load', 3, '_eq')], "in_service"] = False

    # constraints for voltages, lines and trafos
    constraints.set_voltage_constraints(net)
    constraints.set_line_and_trafo_constraints(net)
    constraints.set_ext_grid_constraints(net, slack_voltage_vm_pu=1.0)

    assert not sb.profiles_are_missing(net)

    profiles = sb.get_absolute_values(net, profiles_instead_of_study_cases=True)
    profiles[('sgen', 'q_mvar')] = pd.DataFrame(0, columns=profiles[('sgen', 'p_mw')].columns,
                                                index=profiles[('sgen', 'p_mw')].index)

    pp_aux.rated_app_power(net, profiles)
    pp_aux.units_in_wind_park(net, 3)

    cost_fct.set_costs_flexi_units(net, index_dict)
    net.sgen.loc[net.poly_cost.element, "controllable"] = True

    return profiles


def mapping_dictionaries(downstream_nets):
    """
        Creation of dictionaries which allow to link the various downstream
        (MV) grids with their individual SGEN elements in the upstream (HV) grid

        INPUT:
            **downstream_nets** - Dictionary that contains the downstream (MV)
            grids

        OUTPUT:

            **mapping_dict** (dict) - Dictionary that contains the pandapower
            format nets for all MV grids connected to the HV grid

            **name_mapping_dict** (dict) - Dictionary that links the SimBench
            grid names and the indexes of the MV grids in the HV grid

            **name_mapping_dict2** (dict) - Dictionary that links the grid names
            in the complete grid with the grid names in the HV grid
    """
    mapping_dict = {"MV_idx_66": downstream_nets["MV4"],
                    "MV_idx_67": downstream_nets["MV4"],
                    "MV_idx_68": downstream_nets["MV4"],
                    "MV_idx_69": downstream_nets["MV1"],
                    "MV_idx_70": downstream_nets["MV1"],
                    "MV_idx_71": downstream_nets["MV1"],
                    "MV_idx_72": downstream_nets["MV1"],
                    "MV_idx_73": downstream_nets["MV1"],
                    "MV_idx_74": downstream_nets["MV2"],
                    "MV_idx_75": downstream_nets["MV2"],
                    "MV_idx_76": downstream_nets["MV2"],
                    "MV_idx_77": downstream_nets["MV3"],
                    "MV_idx_78": downstream_nets["MV3"]}

    name_mapping_dict = {"MV_idx_66": "MV4",
                         "MV_idx_67": "MV4",
                         "MV_idx_68": "MV4",
                         "MV_idx_69": "MV1",
                         "MV_idx_70": "MV1",
                         "MV_idx_71": "MV1",
                         "MV_idx_72": "MV1",
                         "MV_idx_73": "MV1",
                         "MV_idx_74": "MV2",
                         "MV_idx_75": "MV2",
                         "MV_idx_76": "MV2",
                         "MV_idx_77": "MV3",
                         "MV_idx_78": "MV3"}

    name_mapping_dict2 = {"MV4.201": "MV_idx_66",
                          "MV4.202": "MV_idx_67",
                          "MV4.203": "MV_idx_68",
                          "MV1.201": "MV_idx_69",
                          "MV1.202": "MV_idx_70",
                          "MV1.203": "MV_idx_71",
                          "MV1.204": "MV_idx_72",
                          "MV1.205": "MV_idx_73",
                          "MV2.201": "MV_idx_74",
                          "MV2.202": "MV_idx_75",
                          "MV2.203": "MV_idx_76",
                          "MV3.201": "MV_idx_77",
                          "MV3.202": "MV_idx_78",
                          "HV2": "HV_grid"}

    return mapping_dict, name_mapping_dict, name_mapping_dict2


def detailed_information_of_mv_units(net):
    """
        Function that saves the detailed reactive power values for each
        flexibility unit of the downstream (MV) grid in a dataframe.

        INPUT:
            **net** - The pandapower format network

        OUTPUT:
            **detailed_info_ds_df** - Dataframe with the reactive power provided
            (Q set points) by each MV connected flexibility unit
    """

    detailed_info_ds_df = pd.DataFrame(columns=["name", "q_provided_mvar"])
    flexibility_units = net.sgen.controllable
    detailed_info_ds_df.name = net.sgen[flexibility_units].name
    detailed_info_ds_df.q_provided_mvar = net.res_sgen[flexibility_units].q_mvar
    detailed_info_ds_df.set_index('name', inplace=True)

    return detailed_info_ds_df


def load_mv_units_reactive_power_values(base_directory_loading, epf, time_step):
    """
        Loads the reactive power set points for the MV grid operators that were
        calculated in the previous bottom up process of the  multi level market.

        INPUT:
            **base_directory_loading** (str) - Base directory for the loading

            **epf** (str) - String that specifies the used EPF

            **time_step** (int) - Current timestep

        OUTPUT:
            **downstream_grid_info** - Dataframe with the detailed information
             about the results of the bottom up process for each MV grid.
             Including the reactive power setpoint at the coupling point,
             base case costs, etc.
    """
    current_filename = base_directory_loading + "/detailed_info_ds_grid/" + epf + "_" + str(time_step) + ".CSV"
    downstream_grid_info = pd.read_csv(current_filename)
    downstream_grid_info.set_index('Unnamed: 0', inplace=True)

    return downstream_grid_info


def load_hv_unit_q_information(base_directory_loading, epf, time_step):
    """
        Loads the reactive power information for the HV units that were
        calculated in the previous bottom up process of the  multi level market.

        INPUT:
            **base_directory_loading** (str) - Base directory for the loading

            **epf** (str) - String that specifies the used EPF

            **time_step** (int) - Current timestep

        OUTPUT:
            **hv_grid_info** - Dataframe with the reactive power provided
            (Q set points) by each HV connected flexibility unit


    """
    current_filename = base_directory_loading + "/detailed_info_hv_grid/" + epf + "_" + str(time_step) + ".CSV"
    hv_grid_info = pd.read_csv(current_filename)
    hv_grid_info.set_index('name', inplace=True)

    return hv_grid_info


def set_up_complete_grid_with_q_setpoints(net, index, reoptimized_q_of_hv_mv_unit, complete_grid_to_mv_grid):
    """
        Sets the reactive power value of a flexibility unit in the
        complete/multi level grid/(HV+MV) to the Q value previously
        determined in the market bottom up process.

        INPUT:
         **net** - The pandapower format network

         **index** - Index of the unit in the complete grid

         **reoptimized_q_of_hv_mv_unit** - Dictionary that holds 14 dataframes with the detailed reactive power
         set point for each units of the HV grid and the MV grids.

         **complete_grid_to_mv_grid** - Dictionary that connects the name of the units in the complete grid with
         corresponding unit of the correct single grid.

    """
    unit_name_complete_grid = net.sgen.loc[index, "name"]
    partitioned_name = unit_name_complete_grid.partition(" ")
    specific_grid_name = partitioned_name[0]
    rest_of_name = partitioned_name[2]
    grid_name = specific_grid_name.partition(".")[0]

    if "MV" in grid_name:
        unit_name = grid_name + ".101 " + rest_of_name
    else:
        unit_name = unit_name_complete_grid

    corresponding_df = reoptimized_q_of_hv_mv_unit[complete_grid_to_mv_grid[specific_grid_name]]
    net.sgen.loc[index, "q_mvar"] = corresponding_df.loc[unit_name, "q_provided_mvar"]


def create_results_csv_files(used_epfs, folder_name):
    """
        Creation of the result CSV files and the results folder. Return dictionaries that contain directories that
        lead to the corresponding result files.

        INPUT:
            **folder_name** - Name of the results folder

            **save_slack_values_mw** - Boolean that states whether the slack values
            of the MV grid values should be saved

        OUTPUT:
            **res_dir** (dict) - General result directoriy

            **res_files** (dict) - Dictionary that contains the result paths for the correct calculation method
            (No tap, discrete or continuous optimization). EXAMPLE: {NT: res path, DT: res path, CT: res path}

            **res_files_detailed_mv_grid_info** - Dictionary that contains the result paths for detailed information
            of the medium voltage grids for the correct calculation method. #
            EXAMPLE: {EPF1: res path1, EPF2: res path2,..}

            **res_files_voltage** (dict) - Dictionary that contains the voltage (all node voltages, min and max values)
            result paths for the correct calculation method (No tap, discrete or continuous optimization)

            **res_files_loading** (dict) - Dictionary that contains the line and trafo loading (all and maximum
            loadings) result paths for the correct calculation method (No tap, discrete or continuous optimization)
    """

    res_dir = os.path.join(os.getcwd(), 'Results',
                           datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + '_market_top_down_results')

    os.makedirs(res_dir, exist_ok=True)

    calculation_type = ["nt", "dt", "ct"]
    for case in calculation_type:
        os.makedirs(os.path.join(res_dir, case), exist_ok=True)

    res_file_dict = {}
    res_files_detailed_mv_grid_info = {}
    res_files_voltage = {}
    res_files_loading = {}
    for epf in used_epfs:
        res_file_dict["NT_" + epf] = os.path.join("nt", folder_name + "_" + epf + "_nt.csv")
        res_file_dict["DT_" + epf] = os.path.join("dt",  folder_name + "_" + epf + "_dt.csv")
        res_file_dict["CT_" + epf] = os.path.join("ct",  folder_name + "_" + epf + "_ct.csv")
        res_files_detailed_mv_grid_info[epf] = "market_detailed_MV_" + epf + ".csv"

        res_files_voltage["NT_" + epf] = os.path.join("nt", folder_name + "_" + epf + "_nt_voltage.csv")
        res_files_voltage["DT_" + epf] = os.path.join("dt", folder_name + "_" + epf + "_dt_voltage.csv")
        res_files_voltage["CT_" + epf] = os.path.join("ct", folder_name + "_" + epf + "_ct_voltage.csv")
        res_files_voltage["NT_" + epf + "_min_max"] = os.path.join("nt", folder_name + "_" + epf + "_nt_voltage_min_max.csv")
        res_files_voltage["DT_" + epf + "_min_max"] = os.path.join("dt", folder_name + "_" + epf + "_dt_voltage_min_max.csv")
        res_files_voltage["CT_" + epf + "_min_max"] = os.path.join("ct", folder_name + "_" + epf + "_ct_voltage_min_max.csv")

        res_files_loading["NT_" + epf + "_trafo"] = os.path.join("nt", folder_name + "_" + epf + "_nt_loading_t.csv")
        res_files_loading["DT_" + epf + "_trafo"] = os.path.join("dt", folder_name + "_" + epf + "_dt_loading_t.csv")
        res_files_loading["CT_" + epf + "_trafo"] = os.path.join("ct", folder_name + "_" + epf + "_ct_loading_t.csv")
        res_files_loading["NT_" + epf + "_trafo_max"] = os.path.join("nt", folder_name + "_" + epf + "_nt_loading_t_max.csv")
        res_files_loading["DT_" + epf + "_trafo_max"] = os.path.join("dt", folder_name + "_" + epf + "_dt_loading_t_max.csv")
        res_files_loading["CT_" + epf + "_trafo_max"] = os.path.join("ct", folder_name + "_" + epf + "_ct_loading_t_max.csv")

        res_files_loading["NT_" + epf + "_lines"] = os.path.join("nt", folder_name + "_" + epf + "_nt_loading_l.csv")
        res_files_loading["DT_" + epf + "_lines"] = os.path.join("dt", folder_name + "_" + epf + "_dt_loading_l.csv")
        res_files_loading["CT_" + epf + "_lines"] = os.path.join("ct", folder_name + "_" + epf + "_ct_loading_l.csv")
        res_files_loading["NT_" + epf + "_lines_max"] = os.path.join("nt", folder_name + "_" + epf + "_nt_loading_l_max.csv")
        res_files_loading["DT_" + epf + "_lines_max"] = os.path.join("dt", folder_name + "_" + epf + "_dt_loading_l_max.csv")
        res_files_loading["CT_" + epf + "_lines_max"] = os.path.join("ct", folder_name + "_" + epf + "_ct_loading_l_max.csv")

    # Write CSV file with corresponding header
    header_main_file = store.results_dict(0, 0, 0, calc_mode="complete_grid", opf_converged=False).keys()
    for file_name in res_file_dict.values():
        store.write_to_csv(res_dir, file_name, header_main_file)

    header_detailed_mw = ["timestep", "P_Slack", "Q_Slack", "Q_provided", "Q_base_case", "base_case_costs", "epf_value",
                          "min_vm_pu_MV", "max_vm_pu_MV", "max_line_loading_MV", "max_trafo_loading_HVMV",
                          "EPF", "Grid_name", "Q_losses_MV", "P_losses_MV", "Costs_Q_DEAs", "C_OPF", "loss_cost_MV",
                          "Q_DEAs_MV", "Q_ext_grid_net"]
    for file_name in res_files_detailed_mv_grid_info.values():
        store.write_to_csv(res_dir, file_name, header_detailed_mw)

    for file_name in res_files_voltage.values():
        if "max" in file_name:
            store.write_to_csv(res_dir, file_name, ['timestep', 'grid_name', 'min_vm_pu', 'max_vm_pu', 'vlt_level'])
        else:
            store.write_to_csv(res_dir, file_name, ["timestep", "node", "vlt_level", "voltage_vm_pu"])

    for file_name in res_files_loading.values():
        if "max" in file_name:
            store.write_to_csv(res_dir, file_name, ['timestep', 'grid_name', 'loading_percent', 'vlt_level'])
        else:
            store.write_to_csv(res_dir, file_name, ["timestep", "index", "vlt_level", "loading_percent"])

    return res_dir, res_file_dict, res_files_detailed_mv_grid_info, res_files_voltage, res_files_loading


def store_market_results(net, res_dir, time_step, res_files, res_files_voltage, res_files_loading, tap_mode, epf, opf_converged=True):
    """
        Stores all relevant results of the time series calculation.

        INPUT:
           **net** - The pandapower format network

           **res_dir** (string)- Directory where the results should be saved

           **time_step** (int) - Current timestep of the calculation

           **res_files** (string) - Dictionary that holds the directories of the
           general result CSV files

           **res_files_voltage** - Dictionary that holds the directories of the
           voltage result CSV files

           **res_files_loading** - Dictionary that holds the directories of the
           loading result CSV files

           **tap_mode** (string) - Information of the tap optimization
            that is used for the current calculation (nt - no tap optimization;
            dt - discrete tap optimization; ct - continuous tap optimization)

            **epf** (str)- String that specifies the used EPF

           **opf_converged** (boolean) - Boolean that states whether the OPF converged
    """

    if opf_converged is True:
        res_dictionary = store.results_dict(net, time_step, 0, calc_mode="complete_grid")
        store.append_to_csv(res_dir, res_files[tap_mode+"_"+epf], res_dictionary)

        store.store_all_voltages(net, time_step, res_dir, res_files_voltage[tap_mode + "_" + epf])
        store.store_all_loadings(net, time_step, res_dir, tap_mode + "_" + epf, res_files_loading)
        store.store_all_max_min_voltages(net, time_step, res_dir, res_files_voltage[tap_mode + "_" + epf + "_min_max"])
        store.store_all_maximum_loadings(net, time_step, res_dir, tap_mode + "_" + epf, res_files_loading)

    else:
        res_dictionary = store.results_dict(net, time_step, 0, "complete_grid", opf_converged=False)
        store.append_to_csv(res_dir, res_files["NT_" + epf], res_dictionary)
        store.append_to_csv(res_dir, res_files["DT_" + epf], res_dictionary)
        store.append_to_csv(res_dir, res_files["CT_" + epf], res_dictionary)


def csv_file_to_sorted_dataframe(directory):
    """
        Function returns a sorted dataframe of the result CSV file

        INPUT:
            **csv_directory** - Directory of the CSV file

        OUTPUT:
            **result_dataframe** - Sorted Dataframe
    """
    result_dataframe = pd.read_csv(directory)
    result_dataframe.sort_values(by=['timestep'], inplace=True)
    result_dataframe.reset_index(drop=True, inplace=True)

    return result_dataframe


def top_down_ts_calculation(complete_grid, profiles_complete_grid, initial_tap_settings, complete_grid_to_mv_grid,
                            idx_controllables_complete_grid, mapping_dict, downstream_profiles, unit_idx_to_grid,
                            iteration_list, base_directory_loading, res_dir, res_files, res_files_voltage,
                            res_files_loading, res_files_detailed_mv_grid_info, used_epfs):
    """
        Executes the timeseries calculation for the top-down process of the reactive power market.
        For each timestep the MV grids are re-optimized. The previously determined reactive power
        setpoints (in the HV market clearing) are set as constraints in the final MV grid clearings.

        Finally, all reactive power values/set points of the flexibility provider (both in the HV grid and MV grids)
        are determined by local market clearings. These set points are then used in the following evaluation of the
        reactive power market in complete grid (HV+MVs). Power flow calculations are performed (with and without tap
        optimizations) and the results are saved in CSV files.

        INPUT:

            **complete_grid** - The pandapower format network of the complete grid

            **profiles_complete_grid** (dict) - Dictionary that contains the profiles of the complete grid

            **initial_tap_settings** (list) - List with the initial tap settings of the transformers in the complete grid

            **complete_grid_to_mv_grid** -

            **idx_controllables_complete_grid** - Indexes of the flexibility units in the complete grid

            **mapping_dict** (dict) - Dictionary that contains the pandapower format nets for all MV grids connected
            to the HV grid

            **downstream_profiles** (dict) - Dictionary that contains the dictionaries of the MV grid profiles

            **unit_idx_to_grid** (dict) - Dictionary that links the SimBench grid names and the indexes of the MV grids
            in the HV grid

            **iteration_list** (list)- List that contains all time steps of the time series calculation

            **base_directory_loading** - Loading directory of the market bottom up results

            **res_dir** (dict)- General result directory

            **res_files** (dict)- Dictionary that contains the result paths for the correct calculation method
            (No tap, discrete or continuous optimization). EXAMPLE: {NT: res path, DT: res path, CT: res path}

            **res_files_voltage** (dict)- Dictionary that contains the voltage (all node voltages, min and max values) result
            paths for the correct calculation method (No tap, discrete or continuous optimization)

            **res_files_loading** (dict)- Dictionary that contains the line and trafo loading (all and maximum
            loadings) result paths for the correct calculation method (No tap, discrete or continuous optimization)


            **res_files_detailed_mv_grid_info** - Dictionary that contains the result paths for detailed information of
             the medium voltage grids for the correct calculation method. # EXAMPLE: {EPF1: res path1, EPF2: res path2,..}

            **used_epfs** (list) - List that contains strings that specify the used EPFs
    """

    start_time_ts_calc = time.time()
    for it_counter, time_steps in enumerate(iteration_list):
        start_time_one_t_step = time.time()

        for epfs in used_epfs:

            q_set_points_mv_hv_flex_units = reoptimize_mv_grids(mapping_dict, downstream_profiles,
                                                                res_files_detailed_mv_grid_info, unit_idx_to_grid,
                                                                base_directory_loading, res_dir, epfs, time_steps)

            q_set_points_mv_hv_flex_units["HV_grid"] = load_hv_unit_q_information(base_directory_loading, epfs, time_steps)

            # Set up complete grid and run power flow calculations #######################
            pp_aux.apply_absolute_values(complete_grid, profiles_complete_grid, time_steps)
            for idx in idx_controllables_complete_grid:
                set_up_complete_grid_with_q_setpoints(complete_grid, idx, q_set_points_mv_hv_flex_units,
                                                      complete_grid_to_mv_grid)

            complete_grid.trafo.tap_pos = initial_tap_settings

            pp.runpp(complete_grid)
            store_market_results(complete_grid, res_dir, time_steps, res_files, res_files_voltage,
                                 res_files_loading, "NT", epfs)

            power_flow_with_tap_optimization(complete_grid, "DT")
            store_market_results(complete_grid, res_dir, time_steps, res_files, res_files_voltage,
                                 res_files_loading, "DT", epfs)

            power_flow_with_tap_optimization(complete_grid, "CT")
            store_market_results(complete_grid, res_dir, time_steps, res_files, res_files_voltage,
                                 res_files_loading, "CT", epfs)

        print("Counter %s, Timestep %s,--- %s seconds ---" % (it_counter, time_steps, time.time()-start_time_one_t_step))
    print("Complete Recalculation Time: --- %s seconds ---" % (time.time() - start_time_ts_calc))


def reoptimize_mv_grids(mapping_dict, downstream_profiles, res_files_detailed_mv_grid_info, unit_idx_to_grid,
                        loading_directory, res_dir, epfs, time_step):

    """"
        Re-Optimization of the MV grids: The reactive power set points for the downstream grids, which were previously
        determined during the market bottom up process are set as constraint (Q_PCC = Q_setpoint). Afterwards, each
        local (MV) market is cleared again and the detailed reactive power set points for each flexibility unit of the
        MV grids are saved and returned.

        INPUT:
            **mapping_dict** (dict) - Dictionary that contains the pandapower
            format nets for all MV grids connected to the HV grid

            **downstream_profiles** (dict) - Dictionary that contains the dictionaries of the MV grid profiles

            **res_files_detailed_mv_grid_info** - Dictionary that contains the result paths for detailed information of
            the medium voltage grids for the correct calculation method. # EXAMPLE: {EPF1: res path1, EPF2: res path2,.}

            **unit_idx_to_grid** - Dictionary that links the SimBench grid names and the indexes of the MV grids in
            the HV grid

            **loading_directory** - Directory to the results of the market bottom up process

            **res_dir** - General result directory

            **epfs** (str) - String that specifies the EPF

            **time_step** (int)- Current time step

        OUTPUT:
            **q_set_points_mv_units** (dict) - Dictionary that contains detailed information about the
            provided reactive power of each MV connected flexibility unit

    """
    q_setpoints_ds_grids = load_mv_units_reactive_power_values(loading_directory, epfs, time_step)

    q_set_points_mv_units = {}
    for grid_idx in q_setpoints_ds_grids.index:
        current_grid_name = "MV_idx_" + str(grid_idx)
        current_net = mapping_dict[current_grid_name]

        pp_aux.apply_absolute_values(current_net, downstream_profiles[unit_idx_to_grid[current_grid_name]], time_step)

        current_MV_Q_provided = q_setpoints_ds_grids.loc[grid_idx, "Q_provided"]
        set_up_mv_grid_for_reoptimization(current_net, current_MV_Q_provided)

        Q_base_case = q_setpoints_ds_grids.loc[grid_idx, "Q_base_case"]
        base_case_costs = q_setpoints_ds_grids.loc[grid_idx, "base_case_costs"]
        epf_value = q_setpoints_ds_grids.loc[grid_idx, "epf_value"]
        try:
            pp.runopp(current_net, delta=1e-16)
        except pp.optimal_powerflow.OPFNotConverged:
            logger.warning("Optimal Power Flow did not converge in timestep %d " % time_step)

            store_reoptimization_mv(False, current_net, time_step, current_MV_Q_provided, Q_base_case, base_case_costs,
                                    epf_value, epfs, current_grid_name, res_dir, res_files_detailed_mv_grid_info[epfs])

        else:
            store_reoptimization_mv(True, current_net, time_step, current_MV_Q_provided, Q_base_case, base_case_costs,
                                    epf_value, epfs, current_grid_name, res_dir, res_files_detailed_mv_grid_info[epfs])

            q_set_points_mv_units[current_grid_name] = detailed_information_of_mv_units(current_net)

    return q_set_points_mv_units

