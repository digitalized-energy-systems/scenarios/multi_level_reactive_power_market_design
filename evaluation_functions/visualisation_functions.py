import matplotlib.pyplot as plt
import pandas as pd
import os

import seaborn as sns

color_codes = {"black": "#000000",
               "grey_dark": "#333333",
               "grey": "#999999",
               "grey_light": "#b3b3b3",
               "blue_light": "#29ABE2",
               "blue": "#0071BC",
               "orange": "#f7931e",
               "orange_darker": "#FF6633",
               "orange_light": "#FAAC58",
               "green": "#669933",
               "yellow": "#FFFF66",
               "pink": "#993366",
               "turquoise": "#81F7D8",
               "blue_new": "#1035FF",
               "yellow_new": "#F5D500",
               "red_new": "#B40404"}


def load_main_res_files(res_main_folder_dir, epf_list):
    """
        Loads the result CSV files, sorts them by the time series timesteps and
        saves them into a dictionary which contains pandas dataframes

        INPUT:
             **res_folder_dir** - String that specifies the directory of the
             folder in which the calculation results are located

        OUTPUT:
            **results_dict** - Dictionary that contains dataframes of the loaded
            CSV files

            **file_name_legend_dict** - Dictionary that contains the names for
            the different results dataframes
    """

    sub_folders = ["status_quo", "ideal_optimum", "market"]
    tap_positions = ["nt", "dt", "ct"]

    tap_position_dict = {"nt": "NT",
                         "dt": "DT",
                         "ct": "CT"}
    file_dir_dict = {}
    file_name_legend_dict = {}

    for folder in sub_folders:
        for taps in tap_positions:
            csv_file_path = os.path.join(res_main_folder_dir, folder, taps, folder+"_"+taps+".csv")

            if folder == "status_quo":
                file_name_legend_dict[folder+"_"+taps] = "RF_A_" + tap_position_dict[taps]
                file_dir_dict[folder+"_"+taps] = csv_file_path
            elif folder == "ideal_optimum":
                file_name_legend_dict[folder+"_"+taps] = "RF_B_" + tap_position_dict[taps]
                file_dir_dict[folder+"_"+taps] = csv_file_path
            elif folder == "market":
                for epf in epf_list:
                    csv_file_path = os.path.join(res_main_folder_dir, folder, taps, folder+"_"+epf+"_"+taps+".csv")
                    file_dir_dict[folder+"_"+epf+"_"+taps] = csv_file_path
                    file_name_legend_dict[folder+"_"+epf+"_"+taps] = "Market" + "_" + tap_position_dict[taps] + "_" + epf

    results_dict = {}
    for key in file_dir_dict.keys():
        dataframe = pd.read_csv(file_dir_dict[key])
        dataframe["timestep"] = dataframe["timestep"].astype(int)
        dataframe.sort_values(by=['timestep'], inplace=True)
        dataframe.reset_index(drop=True, inplace=True)
        results_dict[key] = dataframe

    check_equality_loaded_df(results_dict)

    return results_dict, file_name_legend_dict


def load_detailed_res_files(res_main_folder_dir, tap, detailed_file, epf=""):
    """
        Loads the details CSV result files (min_max node voltages, loadings etc.),
        sorts them by the timesteps and saves them into a dictionary which
        contains pandas dataframes

        INPUT:
             **res_folder_dir** - String that specifies the directory of the folder
             in which the calculation results are located

             **tap** - String that specifies the tap optimization that should
             be loaded/compared (nt - no tap optimization, dt- discrete tap
             optimization, ct - continuous tap optimization)

             **detailed_file** (str) - voltage_min_max, loading_t_max, loading_t_max

             **epf** (str) - String that specifies the market/EPF results that
             should be loaded e.g. EPF1, EPF2, EPF3

        OUTPUT:
            **results_dict** - Dictionary that contains dataframes of the loaded
            CSV files
        """
    sub_folders = ["status_quo", "ideal_optimum", "market"]

    file_dir_dict = {}

    for folder in sub_folders:

        if folder == "status_quo":
            csv_file_path = os.path.join(res_main_folder_dir, folder, tap, folder + "_" + tap + "_" + detailed_file + ".csv")
            file_dir_dict[folder+"_"+tap] = csv_file_path
        elif folder == "ideal_optimum":
            csv_file_path = os.path.join(res_main_folder_dir, folder, tap, folder + "_" + tap + "_" + detailed_file + ".csv")
            file_dir_dict[folder+"_"+tap] = csv_file_path
        elif folder == "market":
            csv_file_path = os.path.join(res_main_folder_dir, folder, tap, folder + "_" + epf + "_" + tap + "_" + detailed_file + ".csv")
            file_dir_dict[folder+"_"+epf+"_"+tap] = csv_file_path

    results_dict = {}

    for key in file_dir_dict.keys():
        dataframe = pd.read_csv(file_dir_dict[key])
        dataframe["timestep"] = dataframe["timestep"].astype(int)
        dataframe.sort_values(by=['timestep'], inplace=True)
        dataframe.reset_index(drop=True, inplace=True)
        results_dict[key] = dataframe

    check_equality_loaded_df(results_dict)

    return results_dict


def check_equality_loaded_df(results_dict):
    """
        Function that checks whether the loaded result files (dataframes)
        have the same length and contain the same time series simulation timesteps

        INPUT:
             **results_dict** - Dictionary that contains dataframes of the loaded
            CSV files
    """
    first_df_of_dict = results_dict[list(results_dict.keys())[0]]

    df_length = []
    dict_keys = []
    equality = []
    for key, dataframe in results_dict.items():
        df_length.append(dataframe.shape[0])
        dict_keys.append(key)
        equality.append(dataframe["timestep"].equals(first_df_of_dict["timestep"]))

    if not all(value is True for value in equality):
        print("Loaded dataframes contain different timesteps!!")
        print("Equality of the dataframes:")
        print(equality)
        print("Length of the dataframes:")
        print(df_length)
        print("Dict keys:")
        print(dict_keys)


def plot_boxplot(data_list, x_axis_labels_dict, y_axis_label, title="",
                 vertical=True, figure_size=(8, 6), fontsize_axis=13,
                 fontsize_scale=10, fontsize_title=15, double=False):
    """
        Plots a boxplot for a given list of data.

        INPUT:
            **data_list** - Data that should be plotted

            **x_axis_labels_dict** - Dictionary that contains the axis names/box
            names. (e.g. "RC_NoMarket, RC_OptMarket, Market...)

            **y_axis_label** (str) - Y-axis name

            **title** (str) - Title of the boxplot

            **vertical** (boolean) - Boolean that specifies whether the boxplot
            should be vertical or not

            **figure_size** (tuple) - Boxplot size

            **fontsize_axis** - Font size of the axis

            **fontsize_scale** - Font size of the axis scale

            **fontsize_title** - Font size of the boxplot title
    """

    fig = plt.figure(figsize=figure_size)

    ax = fig.add_subplot(111)

    meanpointprops = dict(marker='o', markeredgecolor=color_codes["grey_dark"],
                          markerfacecolor=color_codes["grey_dark"])

    # "patch_artist=True" to get fill color
    boxplot = ax.boxplot(data_list.values(), patch_artist=True, vert=vertical,
                         showmeans=True, meanprops=meanpointprops)


    # Change occurrence of the boxplots ######################################
    # change outline color, fill color and linewidth of the boxes
    for box in boxplot['boxes']:
        # Change outline color
        box.set(color=color_codes["blue"], linewidth=2)
        # change fill color
        box.set(facecolor=color_codes["blue"])

    # Change color and linewidth of the whiskers
    for whisker in boxplot['whiskers']:
        whisker.set(linestyle="--", color=color_codes["grey_light"], linewidth=2)

    # Change color and linewidth of the caps
    for cap in boxplot['caps']:
        cap.set(color=color_codes["grey"], linewidth=2)

    # Change color and linewidth of the medians
    for median in boxplot['medians']:
        median.set(color=color_codes["grey_dark"], linewidth=2)

    # Change the style of fliers and their fill
    for flier in boxplot['fliers']:
        flier.set(marker='o',
                  # markerfacecolor=adobe_colors["orange"],
                  markeredgecolor=color_codes["black"],
                  markersize=7)

    plt.title(title)
    ax.title.set_size(fontsize_title)

    if vertical is True:
        # Get only plotted labels out of the dictionary
        x_axis_labels = []
        for key in data_list.keys():
            x_axis_labels.append(x_axis_labels_dict[key])
        ax.set_xticklabels(x_axis_labels)

        ax.set_ylabel(y_axis_label)
        ax.yaxis.label.set_size(fontsize_axis)
        ax.tick_params(axis='both', which='major', labelsize=fontsize_scale)

        # plt.tight_layout()
        if double is True:
            plt.subplots_adjust(left=0.1, right=0.9, bottom=0.12, top=0.98)
        else:
            plt.subplots_adjust(left=0.15, right=0.9, bottom=0.08, top=0.98)
    else:
        y_axis_labels = []
        for key in data_list.keys():
            y_axis_labels.append(x_axis_labels_dict[key])
        ax.set_yticklabels(y_axis_labels)

        ax.set_xlabel(y_axis_label)
        ax.xaxis.label.set_size(fontsize_axis)
        ax.tick_params(axis='both', which='major', labelsize=fontsize_scale)

        # plt.tight_layout()
        if double is True:
            plt.subplots_adjust(left=0.1, right=0.9, bottom=0.12, top=0.98)
        else:
            plt.subplots_adjust(left=0.1, right=0.9, bottom=0.12, top=0.98)

    plt.show()


def set_up_grouped_boxplot(list_of_dicts, list_of_categorical, legend_dict):
    """
        Returns a dataframe that can be used to plot a grouped boxplot.

        INPUT:
            **list_of_dicts** - List that contains dataframes of the individual
            results (e.g. [df: minimum voltage HV, df: maximum voltage HV, ..])

            **list_of_categorical** - List of strings that are used for the
            categories that should be used for the groups of the boxplot.

            **legend_dict** - Dictionary that contains the names for
            the different results dataframes

        OUtPUT:
            **new_frame** - Dataframe that contains 3 columns: These include
            the result data from the individual cases, the case and the categorical
    """

    new_frame = pd.DataFrame(columns=["Data", "Case", "categorical"])

    dict_counter = 0
    for dicts in list_of_dicts:
        for key, value in dicts.items():
            temp_dict = {"Data": value.tolist(),
                         "Case": pd.Series([legend_dict[key]]*dicts[key].size),
                         "categorical": list_of_categorical[dict_counter]}

            temp_frame = pd.DataFrame(temp_dict)
            new_frame = new_frame.append(temp_frame, ignore_index=True)
        dict_counter += 1

    return new_frame


def grouped_boxplot(data_frame, y_axis_label, turn_around_categoricals=False,
                    title="", all_cases=False, figure_size=(8, 6),
                    fontsize_axis=13, fontsize_scale=13, fontsize_title=15,
                    fontsize_legend=13, nbr_categorical=2, ncol_legend=2,
                    plot_h_lines=False, vlt_level="", costs=False):
    """
        Creates a grouped boxplot for a given dataframe.

        INPUT:
            **data_frame** - Dataframe that contains 3 columns. (1. column with all
            the data that should be compared, 2. column with the corresponding case
            ("EPF1", "Referencecase"), 3. categorical/ boolean variable that should
            be grouped by)

            **y_axis_label** (str) - Y-axis name

            **turn_around_categoricals** - Boolean that specifies whether the cases
            should be on the X or Y-axis

            **title** (str) - Title of the boxplot

            **all_cases** - Boolean that specifies if a cases (2 RCs, an 3 EPF
            Cases) should be plotted. If True, the xticklabels are set for 5 cases.

            **figure_size** (tuple) - Boxplot size

            **fontsize_axis** - Font size of the axis

            **fontsize_scale** - Font size of the axis scale

            **fontsize_title** - Font size of the boxplot title

            **fontsize_legend** - Font size of the boxplot legend

            **nbr_categorical** - Specifies the number of categoricals that are in
            one group. default=2, (min=2, max=4)

            **ncol_legend** (int) - Number of columns that should be in the boxplot
            legend

            **plot_h_lines** - Boolean that specifies whether horizontal maximum
            lines should be plotted

            **vlt_level** - Specifies the voltage level that is plotted (HV_V, MV_V).
            If horizontal maximum lines are plotted the voltage boundaries for MV or
            HV Grids is set accordingly

            **costs** - Boolean that specifies whether costs are plotted. If True,
            a different boxplot layout is used.
    """

    fig = plt.figure(figsize=figure_size)
    ax = fig.add_subplot(111)

    used_colors = [color_codes["blue"], color_codes["orange"]]
    if nbr_categorical == 3:
        used_colors = [color_codes["orange_darker"],
                       color_codes["blue_light"], color_codes["blue"]]
    elif nbr_categorical == 4:
        # used_colors = [adobe_colors["orange_darker"],
        #                adobe_colors["blue_light"], adobe_colors["blue"], adobe_colors["orange"]]
        used_colors = [color_codes["turquoise"],
                       color_codes["blue_new"], color_codes["yellow_new"], color_codes["red_new"]]
    elif nbr_categorical == 5:
        used_colors = [color_codes["orange_darker"],
                       color_codes["blue_light"], color_codes["blue"],
                       color_codes["orange"], color_codes["orange_light"]]
    elif nbr_categorical == "EPFs":
        used_colors = [color_codes["orange_darker"], color_codes["orange"],
                       color_codes["orange_light"]]

    # color_palette_dict = {}
    color_p = sns.set_palette(sns.color_palette(used_colors))


    individual_flier = dict(marker='o',
                            markerfacecolor="none",
                            markeredgecolor="black",
                            markeredgewidth=1,
                            markersize=7,
                            linestyle='none')               # (Outliers)

    individual_mean_style = dict(marker='o',
                                 markerfacecolor=color_codes["grey_dark"],
                                 markeredgecolor=color_codes["grey_dark"],
                                 markeredgewidth=1,
                                 markersize=6)

    data_frame_columns = data_frame.columns

    x_data = []
    hue_data = []
    if turn_around_categoricals:
        x_data = data_frame[data_frame_columns[2]]
        hue_data = data_frame[data_frame_columns[1]]
    else:
        x_data = data_frame[data_frame_columns[1]]
        hue_data = data_frame[data_frame_columns[2]]

    sns.boxplot(x=x_data,
                y=data_frame[data_frame_columns[0]],
                hue=hue_data,
                color=color_p,
                flierprops=individual_flier,
                linewidth=2,
                showmeans=True,
                meanprops=individual_mean_style,
                ax=ax)

    # each box in Seaborn boxplot is artist and has 6 associated Line2D objects
    for i, current_box in enumerate(ax.artists):
        if i % 2 == 0:
            color_left_boxes = current_box.get_facecolor()
            current_box.set_edgecolor(color_left_boxes)
        else:
            color_right_boxes = current_box.get_facecolor()
            current_box.set_edgecolor(color_right_boxes)

        # Each box has 6 (or 7 with showmean=Treu) associated Line2D objects
        # (for whiskers, fliers, etc.)
        # Loop over them here, and use the same colour as above
        # 0: lower whisker
        # 1: upper whisker
        # 2: lower cap of whisker
        # 3: upper cap of whisker
        # 4: Median Bar
        # 5:
        for j in range(i * 7, i * 7 + 7):
            if (j - i*7) == 0 or (j - i*7) == 1:            # Whiskers
                line = ax.lines[j]
                line.set_color(color_codes["grey_light"])
                line.set_linestyle("--")
                # line.set_linewidth(2)
            elif (j - i*7) == 2 or (j - i*7) == 3:          # Caps of whiskers
                line = ax.lines[j]
                line.set_color(color_codes["grey_light"])
                # line.set_linestyle("--")
                # line.set_linewidth(1)
            elif (j - i*7) == 4:                            # Median
                line = ax.lines[j]
                line.set_color(color_codes["grey_dark"])
                # line.set_linestyle("--")
                # line.set_linewidth(1)

    ax.set_xlabel("")                       # removes the x axis label ("Case")

    if all_cases == True:
        ax.set_xticklabels(["RC A", "RC B", "EPF1", "EPF2", "EPF3"])

    plt.title(title)
    ax.title.set_size(fontsize_title)

    params = {'mathtext.default': 'regular'}
    plt.rcParams.update(params)

    ax.set_ylabel(y_axis_label)
    ax.yaxis.label.set_size(fontsize_axis)

    ax.tick_params(axis='both', which='major', labelsize=fontsize_scale)

    # Plot horizontal line to indicate mvoltage band
    if plot_h_lines:
        y_values_line = 0
        h_line_label = ""
        if vlt_level == "HV_V":
            h_line_label = "Voltage boundary"
            y_values_line = [0.95, 1.05]
        elif vlt_level == "MV_V":
            h_line_label = "Voltage boundary"
            y_values_line = [0.96, 1.04]
        elif vlt_level == "Loading constraint":
            h_line_label = "Maximum loading"
            y_values_line = [100]
        left, right = plt.xlim()
        plt.hlines(y_values_line, xmin=left, xmax=right,
                   color=color_codes["black"], linestyles='-',
                   label=h_line_label)


    ax.legend(fontsize=fontsize_legend, bbox_to_anchor=(0.5, 1), loc='lower center', ncol=ncol_legend)
    # Get rif of old black edgecolors/frame of the box
    for legpatch in ax.get_legend().get_patches():
        col = legpatch.get_facecolor()
        legpatch.set_edgecolor(col)

    # plt.tight_layout()
    if nbr_categorical == 2:
        plt.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.88)
    elif nbr_categorical == 3:
        plt.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.88)
    elif nbr_categorical == 4:
        plt.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.85)
    elif nbr_categorical == 5 and costs is True:
        plt.subplots_adjust(left=0.12, right=0.9, bottom=0.08, top=0.88)
    elif nbr_categorical == 5:
        plt.subplots_adjust(left=0.10, right=0.9, bottom=0.08, top=0.83)

    plt.show()


def voltage_analysis_grouped(voltage_dict, voltage_level, nbr_col_legend=3, graphic_title=""):
    """
        Plots a standard (grouped) boxplot for the voltage analysis.

        INPUT:
            **voltage_dict** - Dataframe that contains 3 columns. (1. column with all
        the data that should be compared, 2. column with the corresponding case
        ("EPF1", "Referencecase"), 3. categorical/ boolean variable that should
        be grouped by)

            **voltage_level** - Specifies the voltage level that is plotted
            (HV_V, MV_V). If horizontal maximum lines are plotted the voltage
            boundaries for MV or HV grids is set accordingly

             **nbr_col_legend** (int) - Number of columns that should be in the boxplot
             legend

            **graphic_title** (str) - Title of the boxplot
    """
    grouped_boxplot(voltage_dict,
                    turn_around_categoricals=True,
                    title=graphic_title,
                    y_axis_label="Voltage in pu",
                    figure_size=(10, 6),
                    fontsize_axis=19,
                    fontsize_scale=18,
                    fontsize_legend=18,
                    nbr_categorical=5, ncol_legend=nbr_col_legend,
                    plot_h_lines=True, vlt_level=voltage_level)


def loading_analysis_grouped(loading_dict, graphic_title="", nbr_col_legend=3, plot_h_line=False):
    """
        Plots a standard (grouped) boxplot for the loading analysis.

        INPUT:
            **loading_dict** - Dataframe that contains 3 columns. (1. column with all
            the data that should be compared, 2. column with the corresponding case
            ("EPF1", "Referencecase"), 3. categorical/ boolean variable that should
            be grouped by)

            **graphic_title** (str) - Title of the boxplot

            **nbr_col_legend** (int) - Number of columns that should be in the boxplot
            legend

            **plot_h_lines** - Boolean that specifies whether horizontal maximum
            lines should be plotted
    """
    h_line_case = ""
    if plot_h_line is True:
        h_line_case = "Loading constraint"

    grouped_boxplot(loading_dict,
                    title=graphic_title,
                    turn_around_categoricals=True,
                    y_axis_label="Loading in %",
                    figure_size=(10, 6),
                    fontsize_axis=19,
                    fontsize_scale=18,
                    fontsize_legend=18,
                    nbr_categorical=5, ncol_legend=nbr_col_legend,
                    plot_h_lines=plot_h_line, vlt_level=h_line_case)


def cost_analysis(cost_dict, legend_dict):
    """
        Plots a standard boxplot for the cost analysis.

        INPUT:
            **cost_dict** - Dataframe that contains 3 columns. (1. column with all
            the data that should be compared, 2. column with the corresponding case
            ("EPF1", "Referencecase"), 3. categorical/ boolean variable that should
            be grouped by)

            **legend_dict** (str) - Dictionary that contains the names for
            the different results dataframes
    """

    plot_boxplot(cost_dict, x_axis_labels_dict=legend_dict,
                 y_axis_label="Costs in €/h", title="", vertical=True,
                 figure_size=(10, 6), fontsize_axis=19,
                 fontsize_scale=18, fontsize_title=15)


def cost_analysis_grouped(grouped_cost_dict, graphic_title=""):
    """
        Plots a standard (grouped) boxplot for the cost analysis.

        INPUT:
            **grouped_cost_dict** - Dataframe that contains 3 columns. (1. column with all
            the data that should be compared, 2. column with the corresponding case
            ("EPF1", "Referencecase"), 3. categorical/ boolean variable that should
            be grouped by)

            **graphic_title** (str) - Title of the boxplot
    """
    grouped_boxplot(grouped_cost_dict,
                    title=graphic_title,
                    turn_around_categoricals=True,
                    y_axis_label="Costs in €/h",
                    figure_size=(10, 6),
                    fontsize_axis=17,
                    fontsize_scale=16,
                    fontsize_legend=16,
                    nbr_categorical=5, ncol_legend=5, costs=True)


def active_power_losses_analysis(active_losses_dict, legend_dict):
    """
        Plots a standard boxplot for the active losses analysis.

        INPUT:
            **active_losses_dict** - Dataframe that contains 3 columns. (1. column with all
            the data that should be compared, 2. column with the corresponding case
            ("EPF1", "Referencecase"), 3. categorical/ boolean variable that should
            be grouped by)

            **legend_dict** (str) - Dictionary that contains the names for
            the different results dataframes
    """

    plot_boxplot(active_losses_dict, x_axis_labels_dict=legend_dict,
                 y_axis_label="Active power losses in MW", title="",
                 vertical=True, figure_size=(10, 6), fontsize_axis=20,
                 fontsize_scale=20, fontsize_title=15,double=True)


def losses_costs_analysis(losses_cost_dict, legend_dict):
    """
        Plots a standard boxplot for the losses cost analysis.

        INPUT:
            **losses_cost_dict** - Dataframe that contains 3 columns. (1. column with all
            the data that should be compared, 2. column with the corresponding case
            ("EPF1", "Referencecase"), 3. categorical/ boolean variable that should
            be grouped by)

            **legend_dict** (str) - Dictionary that contains the names for
            the different results dataframes
    """

    plot_boxplot(losses_cost_dict, x_axis_labels_dict=legend_dict,
                 y_axis_label="Costs in €/h", title="", vertical=True,
                 figure_size=(10, 6), fontsize_axis=20, fontsize_scale=20,
                 fontsize_title=15, double=True)


def reactive_power_analysis(q_provided_dict, legend_dict, graphic_title=""):
    """
        Plots a standard boxplot for the reactive power analysis.

        INPUT:
            **q_provided_dict** - Dataframe that contains 3 columns. (1. column with all
            the data that should be compared, 2. column with the corresponding case
            ("EPF1", "Referencecase"), 3. categorical/ boolean variable that should
            be grouped by)

            **legend_dict** (str) - Dictionary that contains the names for
            the different results dataframes

            **graphic_title** (str) - Title of the boxplot
    """

    plot_boxplot(q_provided_dict, x_axis_labels_dict=legend_dict,
                 y_axis_label="Reactive power in MVAr",
                 title=graphic_title, figure_size=(10, 6), fontsize_axis=19,
                 fontsize_scale=19, fontsize_title=15)


def reactive_power_analysis_grouped(q_provided_dict, graphic_title=""):
    """
        Plots a grouped boxplot for the reactive power analysis.

        INPUT:
            **q_provided_dict** - Dataframe that contains 3 columns. (1. column with all
            the data that should be compared, 2. column with the corresponding case
            ("EPF1", "Referencecase"), 3. categorical/ boolean variable that should
            be grouped by)

            **graphic_title** (str) - Title of the boxplot
    """

    grouped_boxplot(q_provided_dict,
                    title=graphic_title,
                    y_axis_label="Reactive power in MVAr",
                    turn_around_categoricals=True,
                    figure_size=(10, 6),
                    fontsize_axis=19,
                    fontsize_scale=19,
                    fontsize_title=15,
                    fontsize_legend=19,
                    nbr_categorical=5,
                    ncol_legend=5,
                    costs=True)


def active_power_analysis(p_dict, legend_dict, graphic_title=""):
    """
        Plots a standard boxplot for the active power analysis.

        INPUT:
            **p_dict** - Dataframe that contains 3 columns. (1. column with all
            the data that should be compared, 2. column with the corresponding case
            ("EPF1", "Referencecase"), 3. categorical/ boolean variable that should
            be grouped by)

            **legend_dict** (str) - Dictionary that contains the names for
            the different results dataframes

            **graphic_title** (str) - Title of the boxplot
    """

    plot_boxplot(p_dict, x_axis_labels_dict=legend_dict,
                 y_axis_label="Active power in MW", title=graphic_title,
                 figure_size=(10, 6), fontsize_axis=19,
                 fontsize_scale=19, fontsize_title=15)

