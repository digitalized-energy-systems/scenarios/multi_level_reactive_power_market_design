import simbench as sb

import time

import os
from datetime import datetime
import pandapower as pp

import parameter
from functions import pp_aux
from functions import cost_fct
from functions import grid_constraints as constraints
from evaluation_functions import general_evaluation, store_results as store

import logging
logger = logging.getLogger(__name__)


def set_up_grid(net):
    """
        Sets constraints for voltages, lines, trafos of the HV grid.
        Further the rated apparent power and the EPFs for the flexibility units
        are set. SimBench profiles are created saved.

        INPUT:
            **net** - The pandapower format network of the HV grid

        OUTPUT:
             **profiles** (dict) - Dictionary that contains the profiles for
             the elements in the HV grid
    """

    constraints.set_upstream_net_constraints(net)
    net.ext_grid.loc[0, "min_q_mvar"] = 0
    net.ext_grid.loc[0, "max_q_mvar"] = 0
    net.ext_grid.loc[0, "vm_pu"] = 1

    assert not sb.profiles_are_missing(net)
    profiles = sb.get_absolute_values(net, profiles_instead_of_study_cases=True)

    pp_aux.rated_app_power(net, profiles)
    pp_aux.units_in_wind_park(net, 3)

    return profiles


def result_directories(used_epfs):
    """
        Creation of result CSV files for the corresponding EPFs. Returns the
        result directory and the result file names in a dictionary.

        INPUT:
            **used_epfs** (list) - List with strings of the used EPFs

        OUTPUT:
            **res_dir** (string) - String of the result directory

            **res_files** (dict) - Dictionary that contains the names of the
            result files for the corresponding EPFs. EXAMPLE:
            {"EPF1": "name1.csv", "EPF2": "name2.csv",..}
    """

    res_dir = os.path.join(os.getcwd(), 'Results',
                           datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + '_market_bottom_up_results')
    os.makedirs(res_dir, exist_ok=True)
    res_files = {"EPF1": "market_res_epf1.csv",
                 "EPF2": "market_res_epf2.csv",
                 "EPF3": "market_res_epf3.csv"}

    header = store.results_dict(0, 0, 0, "market_bottom_up", opf_converged=False).keys()
    for epf in used_epfs:
        store.write_to_csv(res_dir, res_files[epf], header)

    return res_dir, res_files


def ts_calculation(hv_net, hv_profiles, downstream_nets, downstream_profiles, downstream_flexibility_units,
                             downstream_g_names, idx_eq_sorted, idx_non_eq, non_eq_dict, eq_dict, sgen_eq, res_dir,
                             res_files, time_steps, used_epfs):
    """
        Executes the timeseries calculation for the bottom up process of the reactive power market.
        For each time step the developed algorithm is applied to the 4 MV grids and their reactive power
        flexibility range is determined and the chosen EPF(s) are created. Next, the determined reactive power
        flexibility ranges and EPFs of the MV grids are set in the HV grid for the corresponding equivalent
        units (HV2_MV1_eq,..). Finally, the HV grid operator is able to clear its market and determine the reactive
        power set points for the flexibility units in the HV grid (for both HV connected DERs and downstream/MV grid
        operators). The set points are saved in CSV files. Additionally, information for the MV grids like the
        reactive power flexibility ranges, base case costs etc. are saved.

        INPUT:
            **hv_net** - The pandapower format network of the HV grid

            **hv_profiles** (dict) - Dictionary that contains the profiles of the HV grid

            **downstream_nets** (dict) - Dictionary that holds the pandapower format networks of the MV grids

            **downstream_profiles** (dict) - Dictionary that contains the dictionaries of the MV grid profiles

            **downstream_flexibility_units** (dict) - Dictionary that contains dictionaries with a list of booleans
            for the flexibility unit_types units of a grid. Boolean is True if the unit is a flexibility
            unit {"MV1": {"sgen": [True, False..]}, "MV2":...}

            **downstream_g_names** (tuple) - Tuple that contains the MV grid names

            **idx_eq_sorted** (dict) - Contains all indexes of equivalent elements in a simbench grid.
            Example: {'MV1': (('sgen', idx, bus_idx, 'subnet_name')), 'MV2': (('sgen', idx, bus_idx, 'subnet_name'))...,}

            **idx_non_eq** (tuple) - Contains all indexes of non-equivalent elements in a simbench grid.
            Example: (('sgen', idx1, bus_idx1, 'subnet_name'), ('sgen', idx2, bus_idx2, 'subnet_name'),...))

            **non_eq_dict** (dict) - Dictionary which contains the unit_types and a list of booleans. "True" if a unit
            of that type is not an equivalent unit, "False" otherwise

            **eq_dict** (dict) - Dictionary which contains the unit_types and a list of booleans.
            "True" if a unit of that type is an equivalent unit, "False" otherwise

            **sgen_eq** (dict) - Dictionary that contains the indexes of _eq sgens and their downstream grid
            name: {66: 'MV1', 67: 'MV2'..}

            **res_dir** (dict)- General result directory

            **res_files** (dict)- Dictionary that contains the result paths for the correct calculation method
            (No tap, discrete or continuous optimization). EXAMPLE: {NT: res path, DT: res path, CT: res path}

            **time_steps** (list)- List that contains all time steps of the time series calculation

            **used_epfs** (list) - List that contains strings that specify the used EPFs
        """

    start_time_ts_calc = time.time()
    for it_counter, time_step in enumerate(time_steps):
        start_time_one_t_step = time.time()

        pp_aux.apply_absolute_values(hv_net, hv_profiles, time_step)

        downstream_grids_res_epf = general_evaluation.iterate_downstream_nets(downstream_nets, downstream_profiles,
                                                                              downstream_flexibility_units, time_step)

        for index, epf in enumerate(used_epfs):
            pp.create_poly_cost(hv_net, 0, 'ext_grid', cp1_eur_per_mw=parameter.price_active_power, cq1_eur_per_mvar=0)

            base_case_q_mvar, epf_coefficients = algorithm_results(hv_net, downstream_g_names, downstream_grids_res_epf,
                                                                   idx_eq_sorted, epf)

            constraints.set_up_non_eq(hv_net, idx_non_eq, True)
            cost_fct.set_costs_non_eq(hv_net, idx_non_eq, True)

            try:
                pp.runopp(hv_net, delta=1e-16)
            except pp.optimal_powerflow.OPFNotConverged:
                logger.warning("Optimal Power Flow did not converge in timestep %d " % time_step)

                res_q_market = store.results_dict(hv_net, time_step, non_eq_dict, "market_bottom_up", opf_converged=False)
                store.append_to_csv(res_dir, res_files[epf], res_q_market)
                hv_net.poly_cost.drop(hv_net.poly_cost.index, inplace=True)
            else:

                res_q_market = store.results_dict(hv_net, time_step, non_eq_dict, "market_bottom_up")
                store.append_to_csv(res_dir, res_files[epf], res_q_market)

                general_evaluation.save_detailed_info_ds_grids(hv_net, eq_dict, sgen_eq, downstream_grids_res_epf,
                                                               base_case_q_mvar, epf_coefficients, res_dir, epf,
                                                               time_step)
                store.store_detailed_hv_data(hv_net, res_dir, epf, time_step)

                hv_net.poly_cost.drop(hv_net.poly_cost.index, inplace=True)

        print("Counter %s, Time step %s :--- %s seconds ---" % (it_counter, time_step, time.time()-start_time_one_t_step))
    print("Complete TS: --- %s seconds ---" % (time.time() - start_time_ts_calc))


def algorithm_results(hv_net, downstream_g_names, downstream_grids_res_epf, idx_eq_sorted, epf):
    """
        Function that returns the base case reactive power flow values and EPF
        coefficients for each downstream MV grid in dictionaries.
        Further, the calculated EPFs and reactive power boundaries of the MV
        grids are set in the HV grid. Therefore, each equivalent element of an
        MV grid (HV2_MV1_eq,..) receives the correct EPF, Q range and P_PCC.

        INPUT:
                **hv_net** - The pandapower format network of the HV grid

                **downstream_g_names** (tuple) - Tuple that contains the MV grid names

                **downstream_grids_res_epf** - Dictionary that contains the results of the market algorithm
                for the MV grids

                **idx_eq_sorted** (dict) - Contains all indexes of equivalent elements in a simbench grid.
                Example: {'MV1': (('sgen', idx, bus_idx, 'subnet_name')), 'MV2': (('sgen', idx, bus_idx, 'subnet_name'))...,}

                **epf** (str) - String that specifies the used EPF

        OUTPUT:
            **base_case_reactive_power** (dict) - Dictionary that contains the
            base case reactive power value at the PCC for each MV grid

            **epf_coefficients_mv_grids** (dict) - Dictionary that contains the
            EPF coefficients of all MV grids
    """
    base_case_reactive_power = {}
    epf_coefficients_mv_grids = {}

    for grid_name in downstream_g_names:
        constraints.set_up_eq(hv_net, downstream_grids_res_epf[grid_name], idx_eq_sorted[grid_name], epf=epf)

        base_case_reactive_power[grid_name] = downstream_grids_res_epf[grid_name].base_case_q_eq_sgen

        if epf == "EPF1":
            epf_coefficients_mv_grids[grid_name] = downstream_grids_res_epf[grid_name].coefficients_EPF1
        elif epf == "EPF2":
            epf_coefficients_mv_grids[grid_name] = downstream_grids_res_epf[grid_name].coefficients_EPF2
        elif epf == "EPF3":
            epf_coefficients_mv_grids[grid_name] = downstream_grids_res_epf[grid_name].coefficients_EPF3

    return base_case_reactive_power, epf_coefficients_mv_grids


