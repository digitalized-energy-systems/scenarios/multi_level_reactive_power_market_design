import os
import pandas as pd
import simbench as sb
import math as m
import pandapower as pp
import pandapower.control as control

import parameter
import epf_algorithm

from functions.plotting_saving import plot_epf_results
from functions.plotting_saving import saving
from functions import pp_aux
from functions.cost_fct import create_cost_functions
from evaluation_functions.mapping import eq_from_non_eq_elements
from functions import grid_constraints as constraints

import logging
logger = logging.getLogger(__name__)


def iterate_downstream_nets(downstream_nets, downstream_profiles, flexible_units, time_step, slack_voltage_vm_pu: float = 1):
    """
        Executing the market algorithm for the downstream grids to calculate
        EPFs and feasible operating area for flexible reactive power provision.

        INPUT:
            **downstream_nets** (dict) - Dictionary that contains the name of
            the network and the pandapower format networks of the downstream
            grids the name of the network: {'MV1': net1, 'MV2': net2, ..}

            **downstream_profiles** (dict) - Dictionary that contains the
            profiles of the downstream grids
            {'MV1': profiles1, 'MV2': profiles2, ..}

            **flexible_units** (dict) - Dictionary with the unit types and list
            of booleans. True if unit is a flexibility unit, False otherwise

            **time_step** (int) - Specific timestep (index of the specific
            timestep in the profiles dataframe) that should be calculated

            **slack_voltage_vm_pu** (float) - Voltage of the external grid
    """

    downstream_grids_epf_algorithm = {}
    for net_name, downstream_net in downstream_nets.items():

        pp_aux.apply_absolute_values(downstream_net, downstream_profiles[net_name], time_step)

        downstream_grids_epf_algorithm[net_name] = epf_algorithm.EquivalentGrid(net_name,
                                                                                downstream_net,
                                                                                parameter.price_active_power)

        downstream_grids_epf_algorithm[net_name].set_up_grid(True, False,
                                                             flexible_units[net_name],
                                                             calculation_mode="time_series",
                                                             slack_voltage_vm_pu=slack_voltage_vm_pu)

        downstream_grids_epf_algorithm[net_name].execute_algorithm()

        plot_epf_results(downstream_grids_epf_algorithm[net_name],
                         parameter.plot_epf)

        saving(downstream_grids_epf_algorithm[net_name],
               parameter.save_epf_algorithm_res_df, time_step)

    return downstream_grids_epf_algorithm


def prepare_downstream_grids(ds_grid_names):
    """
        Preparation of the downstream grids for a time series calculation.
        Sets all costs and constraints for the elements in the downstream
        grids, calculates the rated apparent power and loads the profiles.
        The set up grids, profiles will be stored in dictionaries that they
        only need to be loaded once.

        INPUT:
            **ds_grid_names** (tuple) - Tuple with the downstream grid
            names ("MV1", "MV2"..)

        OUTPUT:
            **ds_nets** (dict) - Dictionary that contains the pandapower format
            downstream grids {"MV1": net_mv1, "MV2": net_mv2,..}

            **ds_profiles** (dict) - Dictionary that contains the profiles of
            the downstream grids {"MV1": profile_mv1, "MV2": profile_mv2,..}

            **ds_flex_units** (dict) - Dictionary that contains dictionaries
            with a list of booleans for the flexibility unit_types units of a
            grid. Boolean is True if the unit is a flexibility
            unit {"MV1": {"sgen": [True, False..]}, "MV2":...}

    """
    ds_nets = {}
    ds_profiles = {}
    ds_flex_units = {}

    for grid_name in ds_grid_names:
        ds_grid_name = parameter.SimBenchCodes[grid_name] + parameter.switch_position
        ds_net = sb.get_simbench_net(ds_grid_name)

        constraints.set_voltage_constraints(ds_net)
        constraints.set_line_and_trafo_constraints(ds_net)
        constraints.set_ext_grid_constraints(ds_net, 1.0)

        assert not sb.profiles_are_missing(ds_net)

        ds_profile = sb.get_absolute_values(ds_net, profiles_instead_of_study_cases=True)

        pp_aux.rated_app_power(ds_net, ds_profile)

        ds_eq_dict, ds_non_eq_dict = eq_from_non_eq_elements(ds_net)
        ds_flex_units[grid_name] = ds_non_eq_dict

        for unit_type in parameter.possible_flexibility_units:
            ds_net[unit_type]["controllable"] = False

        # Set the downstream non equivalent units as flexibility units and set
        # the cost for these units
        for unit_type, index_list in ds_non_eq_dict.items():
            ds_net[unit_type].loc[index_list, "controllable"] = True
            create_cost_functions(ds_net, parameter.price_active_power)

        ds_profiles[grid_name] = ds_profile
        ds_nets[grid_name] = ds_net

    return ds_nets, ds_profiles, ds_flex_units


def save_detailed_info_ds_grids(net, eq_dict, sgen_eq, downstream_grids_res_epf, base_case_q_mvar, epf_coefficients, res_dir, epf, time_step):
    """
        Write a csv file with detailed result information about every single
        downstream grid.

        INPUT:
            **net** - The pandapower format network

            **eq_dict** (dict) - Dictionary which contains the unit_types
            and a list of booleans. "True" if a unit of that type is an
            equivalent unit, "False" otherwise

            **sgen_eq** - Dictionary that contains the indexes of
            _eq sgens and their downstream grid name: {66: 'MV1', 67: 'MV2'..}

            **detailed_q_info_units** - Dictionary with the results of the
            epf_algorithm for the downstream grids

            **base_case_q_mvar** (float) - Base case reactive power of a grid

            **epf_coefficients** (array) - Coefficients of the used EPF for a grid

            **res_dir** (str) - Result directory of the time series calculation

            **epf** (str) - String of the used EPF ("EPF1", "EPF2" or "EPF3")

            **time_step** (int) - Timestep of the time series calculation
    """
    # Detailed information of the generation and reserve of flex units
    results_ds_df = pd.DataFrame(columns=["Q_provided", "Q_base_case", "Q_min", "Q_max", "subnet"])

    results_ds_df["Q_provided"] = net.res_sgen.loc[eq_dict["sgen"]].q_mvar
    results_ds_df["Q_min"] = net.sgen.loc[eq_dict["sgen"]].min_q_mvar
    results_ds_df["Q_max"] = net.sgen.loc[eq_dict["sgen"]].max_q_mvar

    results_ds_df["subnet"] = net.sgen.loc[eq_dict["sgen"]].subnet
    for idx, grid_name in sgen_eq.items():
        results_ds_df.loc[idx, "base_case_costs"] = downstream_grids_res_epf[grid_name].base_case_cost

        epf_value = 0.
        for coefficient in range(epf_coefficients[grid_name].size):
            epf_value += epf_coefficients[grid_name][-(coefficient + 1)] * m.pow(results_ds_df.loc[idx, "Q_provided"], coefficient)

        results_ds_df.loc[idx, "epf_value"] = epf_value

    for keys, values in base_case_q_mvar.items():
        positions = results_ds_df.subnet.str.contains(keys, regex=False)
        results_ds_df.loc[positions, "Q_base_case"] = values

    result_ds_dir = os.path.join(res_dir, 'detailed_info_ds_grid')
    os.makedirs(result_ds_dir, exist_ok=True)
    results_ds_df.to_csv(os.path.join(result_ds_dir, epf + '_' + str(time_step) + '.csv'))


def indexes_relevant_units(net):
    """ Function that creates a dictionary for a given network with the indexes
        of the element type (_eq, non_eq - elements) separated by the
        calculated voltage levels

    INPUT:
         **net** - The pandapower format network

    OUTPUT:
        **index_dict** (dict) - Dictionary that contains list of indexes for
        specific network elements

    EXAMPLE:
        - {(unit_type, volt_Lvl, '_eq'): [indexes],
           (unit_type, volt_Lvl, 'non_eq'): [indexes]}

        - {('sgen', volt_Lvl, '_eq'): [indexes],
          ('sgen', volt_Lvl, 'non_eq'): [indexes],
          ('load', volt_Lvl, '_eq'): [indexes],
          ('load', volt_Lvl, 'non_eq'): [indexes]}
    """
    index_dict = {}
    for unit_type in parameter.possible_flexibility_units:
        if not net[unit_type].empty:
            # create list of lists depending on the number of
            # simulated voltage levels
            eq_elements = [[] for v in parameter.voltage_levels]
            non_eq_elements = [[] for v in parameter.voltage_levels]

            # write idx of the element's type (_eq, non_eq elements) into the
            # corresponding list of the voltage level
            for index in net[unit_type].index:
                for counter, voltage_level in enumerate(parameter.voltage_levels):
                    if voltage_level == net[unit_type].loc[index, "voltLvl"]:
                        if "_" not in net[unit_type].loc[index, "subnet"]:
                            non_eq_elements[counter].append(index)
                        elif "_eq" in net[unit_type].loc[index, "subnet"]:
                            eq_elements[counter].append(index)

            # create a dictionary with relevant information for further usage
            for counter, voltage_level in enumerate(parameter.voltage_levels):
                index_dict[(unit_type, voltage_level, "_eq")] = eq_elements[counter]
                index_dict[(unit_type, voltage_level, "non_eq")] = non_eq_elements[counter]
    return index_dict


def power_flow_with_tap_optimization(net, controller_mode):
    """
        Conducts a power flow calculation with a tap optimization.

        INPUT:
            **net** - Pandapower format net

            **controller_mode** (str) - String that specifies the tap
            optimization method used (DT - Discrete tap optimization;
            CT - Continuous tap optimization)
    """
    net.controller = net.controller[0:0]

    if controller_mode == "DT":
        for idx in net.trafo.index:
            if net.trafo.loc[idx, "voltLvl"] == 4:
                control.DiscreteTapControl(net=net, tid=idx, vm_lower_pu=0.985, vm_upper_pu=1.015)

    elif controller_mode == "CT":
        for idx in net.trafo.index:
            if net.trafo.loc[idx, "voltLvl"] == 4:
                control.ContinuousTapControl(net=net, tid=idx, vm_set_pu=1.0, tol=1e-6)

    pp.runpp(net, run_control=True)


def dictionary_different_grids_in_complete_grid(net, grid_variable):
    """
        Returns a dictionary with booleans for the specific grids in the complete grid.

        INPUT:
            **net** - Pandapower format net

            **grid_variable** - String that specifies the variable of interest (voltage, line_loading or trafo_loading)
    """

    grid_boolean_dict = {}

    if grid_variable == "voltage" or grid_variable == "line_loading":
        grid_codes = ["MV1.201", "MV1.202", "MV1.203", "MV1.204", "MV1.205",
                      "MV2.201", "MV2.202", "MV2.203", "MV3.201", "MV3.202",
                      "MV4.201", "MV4.202", "MV4.203", "HV2"]

        for code in grid_codes:
            if grid_variable == "voltage":
                grid_boolean_dict[code] = net.bus.name.str.contains(code, regex=False)
            else:
                grid_boolean_dict[code] = net.line.name.str.contains(code, regex=False)

    elif grid_variable == "trafo_loading":
        grid_codes = ["HV2-MV1.201", "HV2-MV1.202", "HV2-MV1.203", "HV2-MV1.204", "HV2-MV1.205",
                      "HV2-MV2.201", "HV2-MV2.202", "HV2-MV2.203", "HV2-MV3.201", "HV2-MV3.202",
                      "HV2-MV4.201", "HV2-MV4.202", "HV2-MV4.203", "HV2 Trafo"]

        for code in grid_codes:
            grid_boolean_dict[code] = net.trafo.name.str.contains(code, regex=False)

    return grid_boolean_dict




