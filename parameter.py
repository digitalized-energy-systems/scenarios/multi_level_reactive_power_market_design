"""
Parameter for the reactive power market and the network constraints
"""

voltage_levels = [3, 5]
# List that contains the simbench voltage levels that will be used for the calculation.
# Order: highest to lowest voltage level. Currently a maximum of two voltage levels can
# participate in market

iterations_per_level = 15
# Number of OPF calculations for each voltage level (Minimum number is 5)

switch_position = "no_sw"
# Define switch position of the simbench net: sw = complete,
# no_sw = only open line- and trafo-switches

flexibility_units = ["sgen"]
# List that contains the unit-types of the flexibility units.

possible_flexibility_units = ["sgen", "gen", "load", "storage"]
# List that contains possible flexibility unit-types.

save_epf_algorithm_res_df = False
# Specify whether the results dataframe of the epf_algorithm should be saved
# as a csv file.

plot_epf = False
# Specify whether the EPFs and Graphs of the epf_algorithm should be plotted


# ################### Network(s) Parameter ###################

# Quadratic cost coefficient of the flexibility units
DER_cq2_eur_per_mvar2 = 247

# Constraints for networks that aren't simbench networks
min_vm_pu = 0.9
max_vm_pu = 1.1
max_line_loading = 100
max_trafo_loading = 100

# Constraints for simbench grids (for single and multi/cross grid calculations)
vm_pu = {"EHV": (0.9, 1.1),
         "HV": (0.95, 1.05),
         "MV": (0.95, 1.05),
         "LV": (0.9, 1.1)}

simbench_loadings = {"EHV": 100,
                     "EHV_HV": 100,
                     "HV": 100,
                     "HV_MV": 100,
                     "MV": 100,
                     "MV_LV": 100,
                     "LV": 100}

# cost parameters for elements:
price_active_power = 51.4           # Lin. cost coefficient for active losses


# ################### Constants ####################
# Voltage level coding
volt_Lvls = {1: "EHV",
             2: "EHV_HV",
             3: "HV",
             4: "HV_MV",
             5: "MV",
             6: "MV_LV",
             7: "LV"}

# SimBench grid codes for the loading of their networks
SimBenchCodes = {'EHV1': '1-EHV-mixed--0-',
                 'HV1': '1-HV-mixed--0-',
                 'HV2': '1-HV-urban--0-',
                 'MV1': '1-MV-rural--0-',
                 'MV2': '1-MV-semiurb--0-',
                 'MV3': '1-MV-urban--0-',
                 'MV4': '1-MV-comm--0-',
                 'LV1': '1-LV-rural1--0-',
                 'LV2': '1-LV-rural2--0-',
                 'LV3': '1-LV-rural3--0-',
                 'LV4': '1-LV-semiurb4--0-',
                 'LV5': '1-LV-semiurb5--0-',
                 'LV6': '1-LV-urban6--0-'}
