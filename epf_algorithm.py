"""
Algorithm for the calculation of the flexibility range and the creation of
expected payment functions for a grid.
"""

import pandas as pd
import numpy as np
import logging

import pandapower as pp
import parameter
from functions import cost_fct
from functions import pp_aux
from functions import grid_constraints as constraints

logger = logging.getLogger(__name__)


class EquivalentGrid:
    def __init__(self, name: str, net, price_active_power: float):
        """
        **name** (str) - Name of the used grid

        **net** - The pandapower format network

        **price_active_power** - Price for active power

        **SearchInterval** - Approximated flexibility range

        **results** (list) - Results of the calculated grid

        **res_columns** (list) - List which contains the names of the
        result values

        **res_df** - Dataframe with the results of the calculated grid

        **Q_min_ext** - Minimum reactive power at the primary substation in MVAr

        **Q_max_ext** - Maximum reactive power at the primary substation in MVAr

        **Q_max_eq_sgen** - Minimum reactive power of the equivalent grid/sgen in MVAr

        **Q_min_eq_sgen** - Maximum reactive power of the equivalent grid/sgen in MVAr

        **P_net** - Active power of the equivalent grid in MW

        **base_case_cost** - Costs of the base case scenario in €/h

        **base_case_q_eq_sgen** - Reactive power in MVAr of the equivalent
        sgen in the base case scenario

        **coefficients_EPF1** - Coefficients of the grid-EPF for calculation method 1

        **coefficients_EPF2** - Coefficients of the grid-EPF for calculation method 2

        **coefficients_EPF3** - Coefficients of the grid-EPF for calculation method 3
        """

        self.name = name
        self.net = net
        self.price_active_power = price_active_power

        self.SearchInterval = [0., 0.]
        self.results = []

        self.res_columns = ['q_ext', 'ap_ext', 'q_eq_sgen', 'q_units', 'c_units',
                            'q_losses', 'p_losses', 'c_losses', 'c_opf', 'q_load',
                            'min_voltage', 'max_voltage', 'max_line_loading', 'max_trafo_loading']

        self.res_df = pd.DataFrame(columns=self.res_columns)
        self.Q_min_ext = 0.
        self.Q_max_ext = 0.
        self.Q_max_eq_sgen = -self.Q_min_ext
        self.Q_min_eq_sgen = -self.Q_max_ext
        self.P_net = 0.

        self.base_case_reactive_power = 0.
        self.base_case_cost = 0.
        self.base_case_q_eq_sgen = 0.

        self.coefficients_EPF1 = []
        self.coefficients_EPF2 = []
        self.coefficients_EPF3 = []

    def set_up_grid(self, set_constraints: bool, set_costs: bool, specific_units: dict = None, calculation_mode: str = "single", slack_voltage_vm_pu: float = 1):
        """ Creates constraints and cost functions for the calculation of the
            EPFs and flexibility range of a grid.

            INPUT:
                **net** - The pandapower format network

                **set_constraints** (bool) - True: constraints will be set for
                the network; False: constraints won't be set

                **set_costs** (bool) - True: cost functions will be created for
                flexibility units; False: cost functions won't be created

                **specific_units** (dict) - Optional: Dictionary that contains
                the unit type and a list of indexes of the flexible units:
                {'sgen': [indexes], 'gen': [indexes],..}, if no dictionary is
                given, every element of the specified flexibility unit types is
                considered as flexible

                **calculation_mode** (str) - String that specifies the
                calculation mode. Choose between "single" (default) or
                "time_series". In "single" mode a complete set up of
                constraints and costs can be carried out. At the "time_series"
                mode only constraints and cost of the flexible units will be
                updated.

                **slack_voltage_vm_pu** (float) - Voltage of the external grid
        """

        if set_constraints:
            if calculation_mode == "single":
                constraints.set_opf_constraints(self.net, slack_voltage_vm_pu, specific_units)
            elif calculation_mode == "time_series":
                constraints.time_series_constraints(self.net, slack_voltage_vm_pu, specific_units)
            else:
                assert False, "Calculation mode doesn't exist. Choose " \
                              "between 'single' and 'time_series'"
        if set_costs:
            if calculation_mode == "single":
                cost_fct.create_cost_functions(self.net, self.price_active_power)
            elif calculation_mode == "time_series":
                logger.warning("For time series calculation it is more time "
                               "efficient to set the costs a single time "
                               "before the calculation. Therefore set "
                               "'set_costs' to 'False'")
            else:
                assert False, "Calculation mode doesn't exist. Choose " \
                              "between 'single' and 'time_series'"

    def execute_algorithm(self):
        """
            Starts the algorithm for the determination of the reactive power flexibility range of a grid.
        """

        self.base_case_scenario()

        q_ext_extreme_range = self.determine_extreme_q_ext_values()

        # If Interval is smaller than 20 VAr don't consider grid as flexible
        if round(q_ext_extreme_range[0], 5) == round(q_ext_extreme_range[1], 5) \
                and round(self.base_case_reactive_power, 5) == round(q_ext_extreme_range[0], 5):
            print("\n\n Reactive power flexibility range is smaller than 20 VAr!!! Grid isn't considered as flexibility!")
            self.res_df = pd.DataFrame(self.results, columns=self.res_columns)
            print(self.res_df)
            base_case_idx = self.res_df.loc[self.res_df.q_ext == self.base_case_reactive_power].index[0]

            self.base_case_cost = self.res_df.loc[base_case_idx, "c_units"] \
                                  + self.res_df.loc[base_case_idx, "p_losses"] \
                                  * self.price_active_power

            self.base_case_q_eq_sgen = self.res_df.loc[base_case_idx, "q_eq_sgen"]
            self.P_net = self.res_df.loc[base_case_idx, "ap_ext"]

            self.coefficients_EPF1 = np.array([0, 0, 0])
            self.coefficients_EPF2 = np.array([0, 0, 0])
            self.coefficients_EPF3 = np.array([0, 0, 0])

        else:
            iteration_list = self.create_search_interval(q_ext_extreme_range)

            self.variation_q_ext(iteration_list)
            self.calculation_relevant_values()

    def execute_opf(self):
        """
            Execution of the OPF/Clearing of the market.
        """
        opf_successful = True
        try:
            pp.runopp(self.net, delta=1e-16)
        except pp.optimal_powerflow.OPFNotConverged:
            logger.warning("Optimal Power Flow did not converge. Will start a PF before the "
                           "OPF.")
            pp.runpp(self.net)
            try:
                pp.runopp(self.net, delta=1e-16, init="flat")
            except pp.optimal_powerflow.OPFNotConverged:
                logger.warning("OPF didn't converged again!")
                opf_successful = False

        return opf_successful

    def determine_extreme_q_ext_values(self):
        """
            Determination of the minimum/maximum reactive power that ca be
            provided by a grid to its upstream grid operator without violating
            any grid constraint.
        """

        flexibility_units = self.net.poly_cost.et == "sgen"
        ext_grid = self.net.poly_cost.et == "ext_grid"

        self.net.poly_cost.loc[flexibility_units, "cq2_eur_per_mvar2"] = 0
        self.net.poly_cost.loc[ext_grid, "cp1_eur_per_mw"] = 0
        self.net.poly_cost.loc[ext_grid, "cq1_eur_per_mvar"] = 1

        q_ext_extreme = []
        opf_successful = self.execute_opf()
        if opf_successful:
            q_ext_extreme.append(self.net.res_ext_grid.loc[0, "q_mvar"])

        self.net.poly_cost.loc[ext_grid, "cq1_eur_per_mvar"] = -1

        opf_successful = self.execute_opf()
        if opf_successful:
            q_ext_extreme.append(self.net.res_ext_grid.loc[0, "q_mvar"])

        self.Q_max_ext = q_ext_extreme[1]
        self.Q_min_ext = q_ext_extreme[0]
        self.Q_max_eq_sgen = -self.Q_min_ext
        self.Q_min_eq_sgen = -self.Q_max_ext

        self.net.poly_cost.loc[flexibility_units, "cq2_eur_per_mvar2"] = parameter.DER_cq2_eur_per_mvar2
        self.net.poly_cost.loc[0, "cp1_eur_per_mw"] = parameter.price_active_power
        self.net.poly_cost.loc[0, "cq1_eur_per_mvar"] = 0

        return q_ext_extreme

    def create_search_interval(self, q_ext_extreme_range):
        """
            Creation of equidistant point within the reactive power flexibility range.
        """

        self.SearchInterval = q_ext_extreme_range
        complete_q_interval = np.linspace(self.SearchInterval[0], self.SearchInterval[1], num=parameter.iterations_per_level-1, endpoint=True)

        return complete_q_interval

    def base_case_scenario(self):
        """
            Calculation of the base case scenario (the lowest cost of a grid operator/EPF minimum): Cost minimizing
            OPF without a reactive power limit at the grid coupling point.
        """
        self.net.ext_grid.max_q_mvar = 1000
        self.net.ext_grid.min_q_mvar = -1000
        self.net.poly_cost.loc[self.net.poly_cost.et == "ext_grid", "cq1_eur_per_mvar"] = 0.

        opf_successful = self.execute_opf()
        if opf_successful:
            self.base_case_reactive_power = self.net.res_ext_grid.loc[0, "q_mvar"]
            self.base_case_q_eq_sgen = -self.base_case_reactive_power
            self.P_net = self.net.res_ext_grid.loc[0, "p_mw"]
            self.base_case_cost = self.net.res_cost
            self.add_result(self.base_case_reactive_power)

        else:
            logger.warning("OPF didn't converge at base case scenario!")
            # TODO: At this stage we could directly exclude grids that aren't capable of Q provision to upstream grid

    def variation_q_ext(self, reactive_power_list):
        """ Variation of the reactive power at the grid coupling point and
            market clearing/OPF execution for a specific reactive power exchange.

            INPUT:
                **iteration_list** (list) - List that contains the reactive power values used for the iteration.
        """

        self.net.poly_cost.loc[self.net.poly_cost.et == "ext_grid", "cq1_eur_per_mvar"] = 0.
        for q_ext in reactive_power_list:
            self.net.ext_grid.min_q_mvar = q_ext-1e-7
            self.net.ext_grid.max_q_mvar = q_ext+1e-7

            opf_successful = self.execute_opf()

            if opf_successful:
                self.add_result(q_ext)
            else:
                logger.warning("OPF didn't converge for a reactive power restriciton of %s and %s MVAr!" % (self.net.ext_grid.loc[0, "min_q_mvar"], self.net.ext_grid.loc[0, "max_q_mvar"]))
                print("Flexibility area boundarypoints: " + str(self.Q_min_ext) + " MVAr and " + str(self.Q_max_ext) + " MVAr")
                print("Will lower external grid q restricitons.")
                self.net.ext_grid.min_q_mvar = q_ext - 1e-6
                self.net.ext_grid.max_q_mvar = q_ext + 1e-6

                opf_successful = self.execute_opf()
                if opf_successful:
                    print("OPF was successful now!!!")
                    self.add_result(q_ext)
                else:
                    logger.warning("OPF didn't converge again at for a reactive power of %s and %s MVAr!" % (self.net.ext_grid.loc[0, "min_q_mvar"], self.net.ext_grid.loc[0, "max_q_mvar"]))
                    print("Flexibility area boundarypoints: " + str(self.Q_min_ext) + " MVAr and " + str(self.Q_max_ext) + " MVAr")

    def calculation_relevant_values(self):
        """
            Calculation of relevant results (after iteration of Q is finished). Including the calculation
            of the EPFs and their quadratic approximation.
        """

        self.res_df = pd.DataFrame(self.results, columns=self.res_columns)
        self.res_df.sort_values(by=["q_ext"], inplace=True)
        self.res_df.reset_index(drop=True, inplace=True)

        assert (len(self.res_df.index) != 0), "Error: OPF couldn't converge for a single price! " \
                                              "Relax network constraints."

        self.calculation_of_epfs()

        self.coefficients_EPF1 = self.approximate_epf("EPF1")
        self.coefficients_EPF2 = self.approximate_epf("EPF2")
        self.coefficients_EPF3 = self.approximate_epf("EPF3")

        self.res_df["EPF1_revenue"] = self.res_df["EPF1"] - (self.res_df["c_opf"] - self.base_case_cost)
        self.res_df["EPF2_revenue"] = self.res_df["EPF2"] - (self.res_df["c_opf"] - self.base_case_cost)
        self.res_df["EPF3_revenue"] = self.res_df["EPF3"] - (self.res_df["c_opf"] - self.base_case_cost)

    def calculation_of_epfs(self):
        """ Calculation of EPFs. Currently, the 3 EPFs are calculated exactly
            the same way. If a comparison of different calculation methods is
            wished, the methods can be adjusted here.
        """
        for idx in self.res_df.index:
            self.res_df.loc[idx, "EPF1"] = self.res_df.loc[idx, "c_opf"] - self.base_case_cost
            self.res_df.loc[idx, "EPF1_operator_costs"] = self.res_df.loc[idx, "c_opf"] - self.res_df.loc[idx, "EPF1"]

            self.res_df.loc[idx, "EPF2"] = self.res_df.loc[idx, "EPF1"]
            self.res_df.loc[idx, "EPF2_operator_costs"] = self.res_df.loc[idx, "EPF1_operator_costs"]

            self.res_df.loc[idx, "EPF3"] = self.res_df.loc[idx, "EPF1"]
            self.res_df.loc[idx, "EPF3_operator_costs"] = self.res_df.loc[idx, "EPF1_operator_costs"]

    def approximate_epf(self, epf):
        """ Quadratic approximation of the calculated EPF values.
            Currently, 3 approximation methods are used:
                **EPF1**: Approximation with least squares method
                **EPF2**: Approximation with weighted least squares method (w=100000)
                **EPF3**: Approximation with weighted least squares method (w=1000)

            INPUT:
                **epf** (string) - Sting that specifies which EPF should be
                calculated. ("EPF1", "EPF2"...)

            OUTPUT:
                **coefficients** (numpy.array) - Coefficients of the quadratic
                EPF. Coefficients in the order [deg, deg-1, … 0].
        """
        coefficients = np.empty((1, 3))

        if epf == "EPF1":
            x_values = self.res_df["q_eq_sgen"].to_numpy()
            y_values = self.res_df["EPF1"].to_numpy()
            coefficients = np.polyfit(x_values, y_values, 2)
        elif epf == "EPF2":
            x_values = self.res_df["q_eq_sgen"].to_numpy()
            y_values = self.res_df["EPF2"].to_numpy()
            weights = [1] * len(x_values)
            idx_minimum = np.where(y_values == np.amin(y_values))
            weights[idx_minimum[0][0]] = 100000
            coefficients = np.polyfit(x_values, y_values, 2, w=weights)
        elif epf == "EPF3":
            x_values = self.res_df["q_eq_sgen"].to_numpy()
            y_values = self.res_df["EPF3"].to_numpy()
            weights = [1] * len(x_values)
            idx_minimum = np.where(y_values == np.amin(y_values))
            weights[idx_minimum[0][0]] = 1000
            coefficients = np.polyfit(x_values, y_values, 2, w=weights)

        return coefficients

    def add_result(self, q_ext):
        """ Adds the results of each iteration to the results list.

            INPUT:
                **q_ext** (float) - The reactive power at the primary substation for each iteration.
                                    For the base case this value will be determined at the unrestricted OPF.
        """

        ap_ext = self.net.res_ext_grid.loc[0, "p_mw"]
        q_eq_sgen = - q_ext
        q_units = pp_aux.flex_units_q_production(self.net)
        c_units = cost_fct.costs_q_flex_units(self.net)
        [p_loss, q_loss] = pp_aux.loss_calculation(self.net)
        c_loss = cost_fct.loss_costs(self.net)
        c_opf = self.net.res_cost
        min_voltage = self.net.res_bus.vm_pu.min()
        max_voltage = self.net.res_bus.vm_pu.max()
        max_line_loading = self.net.res_line.loading_percent.max()
        max_trafo_loading = self.net.res_trafo.loading_percent.max()

        self.results.append((q_ext, ap_ext, q_eq_sgen, q_units, c_units, q_loss, p_loss,
                             c_loss, c_opf, self.net.res_load["q_mvar"].sum(),
                             min_voltage, max_voltage, max_line_loading, max_trafo_loading))

