import simbench as sb
import pandas as pd
import os

import random

import parameter
from evaluation_functions import general_evaluation, mapping, bottom_up_specific, top_down_specific

import logging
logger = logging.getLogger(__name__)


def main():
    print("Market bottom up process:")
    loading_directory = market_bottom_up(nbr_of_samples=5000)

    print("Market top down process:")
    market_top_down_evaluation(loading_directory)


def market_bottom_up(nbr_of_samples):
    hv_net = sb.get_simbench_net(parameter.SimBenchCodes["HV2"] + parameter.switch_position)

    downstream_g_names = mapping.downstream_grid_names(hv_net)

    idx_eq_sorted, idx_eq, idx_non_eq, sgen_eq = mapping.idx_dataframes(hv_net, downstream_g_names)

    downstream_nets, downstream_profiles, downstream_flexibility_units = general_evaluation.prepare_downstream_grids(downstream_g_names)

    profiles = bottom_up_specific.set_up_grid(hv_net)

    eq_dict, non_eq_dict = general_evaluation.eq_from_non_eq_elements(hv_net)

    random.seed(1)
    sequence = profiles[('load', 'p_mw')].index.tolist()
    time_steps = random.sample(sequence, nbr_of_samples)

    used_epfs = ["EPF1", "EPF2", "EPF3"]
    res_dir, res_files = bottom_up_specific.result_directories(used_epfs)

    bottom_up_specific.ts_calculation(hv_net, profiles, downstream_nets, downstream_profiles,
                                      downstream_flexibility_units, downstream_g_names, idx_eq_sorted, idx_non_eq,
                                      non_eq_dict, eq_dict, sgen_eq, res_dir, res_files, time_steps, used_epfs)

    for epf in used_epfs:
        print(pd.read_csv(os.path.join(res_dir, res_files[epf])).head().to_string())

    return res_dir


def market_top_down_evaluation(loading_directory):
    complete_grid = sb.get_simbench_net('1-HVMV-urban-all-0-no_sw')

    initial_tap_settings = complete_grid.trafo.tap_pos

    profiles_complete_grid = top_down_specific.initial_set_up_complete_grid(complete_grid)
    idx_controllables_complete_grid = complete_grid.sgen[complete_grid.sgen.controllable == True].index

    downstream_g_names = ('MV1', 'MV2', 'MV3', 'MV4')
    downstream_nets, downstream_profiles, downstream_flexibility_units = general_evaluation.prepare_downstream_grids(downstream_g_names)

    for key, value in downstream_nets.items():
        value.poly_cost.loc[0, "cq1_eur_per_mvar"] = 0
        value.poly_cost.loc[0, "cp1_eur_per_mw"] = parameter.price_active_power

    mapping_dict, unit_idx_to_grid, complete_grid_to_mv_grid = top_down_specific.mapping_dictionaries(downstream_nets)

    used_epfs = ["EPF1", "EPF2", "EPF3"]

    res_dir, res_files, res_files_detailed_mv_grid_info, res_files_voltage, res_files_loading = top_down_specific.create_results_csv_files(used_epfs, "market")

    results_bottom_up = pd.read_csv(os.path.join(loading_directory, "market_res_epf1.csv"))
    iteration_list = []
    for index in results_bottom_up.index:
        iteration_list.append(results_bottom_up.loc[index, "timestep"])
    print(iteration_list)

    top_down_specific.top_down_ts_calculation(complete_grid, profiles_complete_grid, initial_tap_settings,
                                              complete_grid_to_mv_grid, idx_controllables_complete_grid, mapping_dict,
                                              downstream_profiles, unit_idx_to_grid, iteration_list, loading_directory,
                                              res_dir, res_files, res_files_voltage, res_files_loading,
                                              res_files_detailed_mv_grid_info, used_epfs)

    res_reoptimized_df = pd.read_csv(os.path.join(res_dir, res_files["NT_EPF3"]))
    print(res_reoptimized_df.to_string())


if __name__ == '__main__':
    main()

