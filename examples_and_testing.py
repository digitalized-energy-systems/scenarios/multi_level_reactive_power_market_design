"""
Testing and examples for the epf_algorithm, which creates EPFs for a
grid and calculates the reactive power flexibility range.
"""
import epf_algorithm
import pandapower.networks as nw
import simbench as sb

import parameter

from functions.plotting_saving import plot_epf_results
from functions.plotting_saving import saving
from functions import pp_aux


def main():
    # simbench_example(net_code='MV1')
    cigre_example()
    # one_bus_example()
    # two_bus_example()
    # three_bus_example()


def simbench_example(net_code):
    """
        Attention: This simbench example includes all downstream grids as flexibility providers.
        It is assumed that these grids are able to provide Q without violating grid constraints.
    """
    net_name = parameter.SimBenchCodes[net_code] + parameter.switch_position
    net = sb.get_simbench_net(net_name)

    assert not sb.profiles_are_missing(net)
    profiles = sb.get_absolute_values(net, profiles_instead_of_study_cases=True)

    pp_aux.apply_absolute_values(net, profiles, 0)
    pp_aux.rated_app_power(net, profiles)

    equivalent_grid = epf_algorithm.EquivalentGrid(net_name, net, parameter.price_active_power)

    equivalent_grid.set_up_grid(True, True)

    equivalent_grid.execute_algorithm()

    print(equivalent_grid.res_df.to_string())
    plot_epf_results(equivalent_grid, parameter.plot_epf)
    saving(equivalent_grid, parameter.save_epf_algorithm_res_df)


def cigre_example():
    net_name = "pv_wind"

    net = nw.create_cigre_network_mv(with_der=net_name)

    net.sgen.loc[:, "p_mw"] = 0.8 * net.sgen.loc[:, "p_mw"]

    res_grid = epf_algorithm.EquivalentGrid(net_name, net, parameter.price_active_power)

    res_grid.set_up_grid(True, True)

    res_grid.execute_algorithm()

    print(res_grid.res_df.to_string())
    plot_epf_results(res_grid, parameter.plot_epf)
    saving(res_grid, parameter.save_epf_algorithm_res_df)


def one_bus_example():
    net_name = "One Bus Net"
    net = pp_aux.create_1_bus_test_grid()

    res_grid = epf_algorithm.EquivalentGrid(net_name, net, parameter.price_active_power)

    res_grid.set_up_grid(False, False)

    res_grid.execute_algorithm()

    print(res_grid.res_df.to_string())
    plot_epf_results(res_grid, parameter.plot_epf)
    saving(res_grid, parameter.save_epf_algorithm_res_df)


def two_bus_example():
    net_name = "Two Bus Net"
    net = pp_aux.create_simple_2_bus_test_grid()

    res_grid = epf_algorithm.EquivalentGrid(net_name, net, parameter.price_active_power)

    res_grid.set_up_grid(False, False)

    res_grid.execute_algorithm()

    print(res_grid.res_df.to_string())
    plot_epf_results(res_grid, parameter.plot_epf)
    saving(res_grid, parameter.save_epf_algorithm_res_df)


def three_bus_example():
    net_name = "Three Bus Net"
    net = pp_aux.create_simple_3_bus_test_grid()

    res_grid = epf_algorithm.EquivalentGrid(net_name, net, parameter.price_active_power)

    res_grid.set_up_grid(True, True)

    res_grid.execute_algorithm()

    print(res_grid.res_df.to_string())
    plot_epf_results(res_grid, parameter.plot_epf)
    saving(res_grid, parameter.save_epf_algorithm_res_df)


if __name__ == '__main__':
    main()
